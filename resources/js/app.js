
require('./bootstrap');

window.Vue = require('vue');
import _ from 'lodash'

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
// Moment Js
import moment from 'moment'
Vue.filter('timeformat',(arg)=>{
    return moment(arg).format("DD-MM-YYYY h:mm a")
})
// Sweet alert 2
import Swal from 'sweetalert2'
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
})

const app = new Vue({
    el: '#app',
    data:{
        user: User,
        favorites:[],
        current_bids:[],
        form:{
            amount:'',
        }
    },
    methods:{
        addToFavorite(id){
            axios.post(`/user/favorite`,{property_id:id,user_id:this.user.id})
                .then(res=>{
                    this.favoriteList();
                })
                .then(res=>{
                    Toast.fire({
                        type: 'success',
                        title: 'Property successfully added in your favorite list'
                    })
                })
                .catch(err=>{

                })
        },
        favoriteList(){
            axios.get(`/user/user-favorite`)
                .then(res=>{
                    this.favorites = res.data
                })
                .catch(err=>{

                })
        },
        checkCartItem(productId){
            return _.find(this.favorites,{'property_id':productId});
        },
        exitInFavorite(){
            Toast.fire({
                type: 'warning',
                title: 'Property already added in your favorite list'
            })
        },
        viewCurrentBid(id){
           axios.get(`/user/view-current-bid/${id}`)
               .then(res=>{
                   this.current_bids = res.data
               })
               .catch(err=>{
               })
        },
        submitBidForm(id){
            axios.post(`/user/bid`,{'bid_amount':this.form.amount,'property_id':id,'user_id':this.user.id})
                .then(res=>{
                    $('#place_bid_modal').modal('toggle')
                    this.form.amount =''
                    if(res.data=='error'){
                        Toast.fire({
                            type: 'warning',
                            title: 'Bid Already added..!'
                        })
                    }else if(res.data=='error_amount'){
                        Toast.fire({
                            type: 'warning',
                            title: 'Insufficient amount..!'
                        })
                    }else{
                        Toast.fire({
                            type: 'success',
                            title: 'Bid Successfully added.'
                        })
                    }
                })
                .catch(err=>{

                })

        }

    },
    mounted(){
        this.favoriteList();
    }
});
