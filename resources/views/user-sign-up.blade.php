@extends('layouts.frontend.app')

@section('title','User Sign Up')

@push('css')
    <style>
        .contact-section {
            background: #ecebea;
        }
        .reg.form-content-box{
            max-width: 800px;
        }
    </style>
@endpush

@section('content')
    <!-- Contact section start -->
    <div class="contact-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form content box start -->
                    <div class="reg form-content-box">
                        <!-- details -->
                        <div class="details">
                            <!-- Logo-->
                            <a href="{{route('index')}}">
                                <img src="{{asset('assets/newfrontend/img/logos/black-logo.png') }}" class="cm-logo" alt="black-logo">
                            </a>
                            <!-- Name -->
                            <h3>User Registration</h3>
                            <!-- Form start-->
                            <form id="form_validation" method="post" action="{{route('user-sign-up.store')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                   <div class="col-md-6">
                                        <input type="text" name="f_name" class="input-text form-control" placeholder=" First Name" value="" />
                                        @error('f_name')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                   </div>
                                   <div class="col-md-6">
                                       <input type="text" name="l_name" class="input-text form-control" placeholder="Last Name" value="" />
                                        @error('l_name')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                   </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="text" name="email" class="input-text form-control" placeholder=" Email *" value="" />
                                        @error('email')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                         <select class="input-text form-control" id="sel1" name="role_id">
                                            <option value="">User Type</option>
                                            <option value="1">Buyer</option>
                                            <option value="2">Tenant</option>
                                        </select>
                                        @error('role_id')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="password" name="password" class="input-text form-control" placeholder=" Password *" value="" />
                                        @error('password')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <input type="password" name="confirm_password" class="input-text form-control" placeholder="Confirm  Password *" value="" />
                                        @error('confirm_password')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="text" name="address_1" class="input-text form-control" placeholder="Address 1" value="" />
                                        @error('address_1')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="address_2" class="input-text form-control" placeholder="Address 2" value="" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="text" name="city" class="input-text form-control" placeholder="City" value="" />
                                        @error('city')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <select class="input-text form-control" id="sel1" name="country">
                                            <option value="">Country</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                            @endforeach
                                            @error('country')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="text" name="postcode" class="input-text form-control" placeholder="Post Code" value="" />
                                        @error('postcode')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="phone" class="input-text form-control" placeholder="Phone No." value="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <select class="input-text form-control" id="sel1" name="employment_status">
                                            <option value="">Employement Status</option>
                                            <option value="1">Employed</option>
                                            <option value="2">Unemployed</option>
                                        </select>
                                    </div>
                                     <div class="col-md-6">
                                        <input class="input-text" type="file" name="image" value="Upload Company logo" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="5" cols="10" name="about" placeholder="About Your Self..."></textarea>
                                    @error('about')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group mb-0">
                                    <button type="submit" class="btn-md button-theme btn-block">Signup</button>
                                </div>
                            </form>
                        </div>
                        <!-- Footer -->
                        <div class="footer">
                            <span>Already a member? <a href="{{route('login')}}">Login here</a></span>
                        </div>
                    </div>
                    <!-- Form content box end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Contact section end -->
@endsection

@push('js')

@endpush
