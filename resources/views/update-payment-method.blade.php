@extends('layout.backends.app')

@section('title','dashboard')
@section('content')
    <input id="card-holder-name" type="text" value="{{$agent->f_name}}">

    <!-- Stripe Elements Placeholder -->
    <div id="card-element"></div>

    <button id="card-button" data-secret="{{ $intent->client_secret }}">
        Update Payment Method
    </button>
    @stop
@push('js')
    <script src="https://js.stripe.com/v3/"></script>

    <script>
        const stripe = Stripe('{{env('STRIPE_KEY')}}');

        const elements = stripe.elements();
        const cardElement = elements.create('card');

        cardElement.mount('#card-element');
    </script>
    <script>
        const cardHolderName = document.getElementById('card-holder-name');
        const cardButton = document.getElementById('card-button');
        const clientSecret = cardButton.dataset.secret;

        cardButton.addEventListener('click', async (e) => {
            const { setupIntent, error } = await stripe.handleCardSetup(
                clientSecret, cardElement, {
                    payment_method_data: {
                        billing_details: { name: cardHolderName.value }
                    }
                }
            );

            if (error) {
                // Display "error.message" to the user...
                console.log(error)
            } else {
                // The card has been verified successfully...
                console.log('OK')
            }
        });
    </script>
    @endpush
