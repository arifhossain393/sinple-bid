@extends('layouts.frontend.app')

@section('title','Bid')

@push('css')
<style>
    .row {
        margin-top: 17px !important;
    }
</style>
@endpush

@section('content')
    <section class="bid-property">
        <h2>Bid Property</h2>
        <div class="row">
            <form action="{{route('search-property')}}" method="GET">

            <div class="container src-bg ">
                <div class="col-md-2 col-sm-12">
                    <select class="sw" name="type">
                        <option value="1">Buy</option>
                        <option value="0">Rent</option>
                    </select>
                </div><!-- /btn-group -->
                <div class="col-md-8 col-sm-12">

                    <input class="form-control sr-pb" id="searchTextField" type="text" size="50" placeholder="Enter a location" autocomplete="on" runat="server" />
                    <input type="hidden" id="address" name="address" />
                    <input type="hidden" id="latitude" name="latitude" />
                    <input type="hidden" id="longitude" name="longitude" />

                </div>
                <div class="col-md-2 col-sm-12">
                    <button type="submit" class="sr-btn btn-md btn-default">Search</button>
                </div>
            </div><!-- /.col-lg-6 -->
            </form>
        </div>
    </section>
    {{--<section class="bid-property">--}}
        {{--<h2>Bid Property</h2>--}}
        {{--<div class="row">--}}
            {{--<form action="{{route('search-property')}}" method="GET">--}}
            {{--<div class="container src-bg ">--}}
                {{--<div class="col-md-10 col-sm-12">--}}
                    {{--<select class="sw">--}}
                        {{--<option value="volvo">Buy</option>--}}
                        {{--<option value="saab">Rent</option>--}}
                    {{--</select>--}}
                    {{--<input class="form-control sr-pb" type="text" placeholder="Post code" aria-label="Search">--}}
                    {{--<input class="form-control sr-pb" id="searchTextField" type="text" size="50" placeholder="Enter a location" autocomplete="on" runat="server" />--}}
                    {{--<input type="hidden" id="address" name="address" />--}}
                    {{--<input type="hidden" id="latitude" name="latitude" />--}}
                    {{--<input type="hidden" id="longitude" name="longitude" />--}}
                {{--</div>--}}
                {{--<div class="col-md-2 col-sm-12">--}}
                    {{--<button type="submit" class="sr-btn btn-md btn-default">Search</button>--}}
                {{--</div>--}}
                {{--</div>--}}
            {{--</form>      <!-- /.col-lg-6 -->--}}
        {{--</div>--}}
    {{--</section>--}}
    <!--======= BANNER =========-->





    <!--======= WHAT WE DO =========-->




    <!--======= CALL US =========-->

    <!-- <section class="call-us">
      <div class="overlay">
        <div class="container">
          <ul class="row">
            <li class="col-sm-6">
              <h4></h4>
              <h6></h6>
            </li>
            <li class="col-sm-4">
              <h1></h1>
            </li>
            <li class="col-sm-2 no-padding"> <a href="11-Register.html" class=""></a> </li>
          </ul>
        </div>
      </div>
    </section> -->

    <br><br><br>


    <section id="team">
        <div class="container ">
            <!--======= TITTLE =========-->
            <div class="tittle">
                <h3>Find a Buyer or Renter who can Perform on click 2 Bid</h3>
                <br>

            </div>
            <div class="row pl-2">
                <div class="col-md-6">

                    <!--======= TEAM ROW =========-->
                    <ul class="row">

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-1.jpg')}}" alt="">
                                <div class="">
                                    <!--======= SOCIAL ICON =========-->
                                    <!-- <ul class="social_icons animated-6s fadeInUp">
                                      <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                      <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                      <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                      <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul> -->
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <!--  <div class="team-detail">
                                   <h6>David Martin</h6>
                                   <p>Founder</p>
                                 </div> -->
                            </div>
                        </li>

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-2.jpg')}}" alt="">
                                <div class="">
                                    <!--======= SOCIAL ICON =========-->
                                    <!-- <ul class="social_icons animated-6s fadeInUp">
                                      <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                      <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                      <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                      <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul> -->
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <!--  <div class="team-detail">
                                   <h6>Hendrick jack </h6>
                                   <p>co-Founder</p>
                                 </div> -->
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">

                    <!--======= TEAM ROW =========-->
                    <ul class="row">

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-3.jpg')}}" alt="">
                                <div class="">
                                    <!--======= SOCIAL ICON =========-->
                                    <!-- <ul class="social_icons animated-6s fadeInUp">
                                      <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                      <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                      <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                      <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul> -->
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <!-- <div class="team-detail">
                                  <h6>charles edward </h6>
                                  <p>team leader </p>
                                </div> -->
                            </div>
                        </li>

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-4.jpg')}}" alt="">
                                <div class="">
                                    <!--======= SOCIAL ICON =========-->
                                    <!-- <ul class="social_icons animated-6s fadeInUp">
                                      <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                      <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                      <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                      <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul> -->
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <!--  <div class="team-detail">
                                   <h6>jessica wevins </h6>
                                   <p>team leader</p>
                                 </div> -->
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('js')

    <script type="text/javascript">

        var options = {
            componentRestrictions: {country: "uk"}
        };
        function initialize() {
            var input = document.getElementById('searchTextField');
            var autocomplete = new google.maps.places.Autocomplete(input,options);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                document.getElementById('address').value = place.name;
                document.getElementById('latitude').value = place.geometry.location.lat();
                document.getElementById('longitude').value = place.geometry.location.lng();
                //alert("This function is working!");
                //alert(place.name);
                // alert(place.address_components[0].long_name);
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize)
    </script>
@endpush
