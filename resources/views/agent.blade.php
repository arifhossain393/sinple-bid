@extends('layout.backends.app')

@section('title','dashboard')

@push('css')
    <style>
        .block-header h2 {

            color: #000 !important;

        }

        .card {
            margin-left: -16px !important;
        }
        h5, .h5 {
            font-size: 15px !important;
        }
        h5 {
            margin-left: -11px !important;
            margin-top: 0px !important;
        }

        .btn-info, .btn-info:hover, .btn-info:active, .btn-info:focus {
            background-color: #8bc34a !important;
        }
        .btn.btn-info {
            margin-left: 90px !important;
        }
    </style>
@endpush
@section('content')

    <div class="block-header">
        <h2><b>AGENT DASHBOARD</b></h2>

    </div>

    <!-- Widgets -->
    <div class="row clearfix">
        {{--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box bg-pink hover-expand-effect">--}}
                {{--<div class="icon">--}}
                    {{--<i class="material-icons">playlist_add_check</i>--}}
                {{--</div>--}}
                {{--<div class="content">--}}
                    {{--<div class="text">Customers</div>--}}
                    {{--<div class="number count-to" data-from="0" data-to="0" data-speed="15" data-fresh-interval="20"></div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <a href="{{ route('property.index') }}"><i class="material-icons">help</i></a>
                </div>
                <div class="content">
                    <div class="text">Properties</div>
                    <div class="number count-to" data-from="0" data-to="
                        {{\App\Property::where('agent_id',auth()->guard('agent')->user()->id)->count()}}
                    " data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        {{--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">--}}
            {{--<div class="info-box bg-light-green hover-expand-effect">--}}
                {{--<div class="icon">--}}
                    {{--<i class="material-icons">forum</i>--}}
                {{--</div>--}}
                {{--<div class="content">--}}
                    {{--<div class="text">Favourite</div>--}}
                    {{--<div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"></div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--@php--}}
            {{--$properties = auth()->guard('agent')->user()->properties;--}}
            {{--$count = 0;--}}
        {{--@endphp--}}
        {{--@foreach($properties as $property)--}}
            {{--@foreach(\App\Bid::where('property_id',$property->id)->get() as $bid)--}}
                {{--@if($bid->property_id == $property->id)--}}
                    {{--@php--}}
                        {{--$count = $count+1;--}}
                    {{--@endphp--}}
                {{--@endif--}}
            {{--@endforeach--}}
        {{--@endforeach--}}
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-orange hover-expand-effect">
                <div class="icon">
                    <a href="{{ route('agent-bid.index') }}"><i class="material-icons">person_add</i></a>
                </div>
                <div class="content">
                    <div class="text">Bid</div>
                    @php
                        $properties = auth()->guard('agent')->user()->properties;
                        $count = 0;
                    @endphp
                    @foreach($properties as $property)
                        @foreach(\App\Bid::where('property_id',$property->id)->get() as $bid)
                            @if($bid->property_id == $property->id)
                                @php
                                    $count = $count+1;
                                @endphp
                            @endif
                        @endforeach
                    @endforeach
                    <div class="number count-to" data-from="0" data-to="{{$count}}" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Widgets -->
    <!-- CPU Usage -->
    {{--@foreach($subscription as $subscription)--}}
        {{--<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">--}}
            {{--<div class="card">--}}
                {{--<div class="header bg-cyan">--}}

                       {{--<h5>{{$subscription->name}}</h5>--}}


                    {{--<ul class="header-dropdown m-r--5">--}}
                        {{--<li>--}}

                            {{--<h5>£{{$subscription->price}}({{$subscription->days}}days)</h5>--}}

                        {{--</li>--}}

                        {{--<li class="dropdown">--}}

                            {{--<ul class="dropdown-menu pull-right">--}}
                                {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                                {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="body">--}}
                    {{--{!! Str::limit($subscription->description,'250','...') !!}--}}
                {{--</div>--}}
                {{--<div class="header bg-cyan">--}}
                    {{--<a href="{{route('order.paypal',$subscription->id)}}" class="btn btn-info">Purchase</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--@endforeach--}}

    <div class="body">
        <div id="donut_chart" class="dashboard-donut-chart"></div>
    </div>



@endsection

@push('js')
    <script src="{{asset('assets/backend/js/pages/cards/colored.js')}}"></script>
@endpush
