@extends('layouts.backend.app')
@section('title','Bid')

@push('css')
    <style>
        .margin{margin-bottom: 5px!important;}
    </style>
@endpush

@section('content')

    <!-- #END# Basic Examples -->
    <!-- Exportable Table -->
    <div class="row clearfix">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                       Bid History
                    </h2>

                </div>
                <div class="body">

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-md-6 margin">
                                <h5>Property Name</h5>
                            </div>
                            <div class="col-md-6 margin">
                                <h5><a target="_blank" href="{{route('property.details',$bid->property_id)}}" >{{$bid->property->name}}</a></h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 margin">
                                <h5>Actual Price</h5>
                            </div>
                            <div class="col-md-6 margin">
                                <h5>{{env('APP_CURRENCY')}}{{$bid->property->price}}</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 margin">
                                <h5>Customer</h5>
                            </div>
                            <div class="col-md-6 margin">
                                <h5>{{$bid->user->f_name." ".$bid->user->l_name}}</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 margin">
                                <h5>Estate Agent</h5>
                            </div>
                            <div class="col-md-6 margin">
                                <h5><a target="_blank" href="{{route('agent.details',$bid->property->agent->id)}}">{{$bid->property->agent->f_name." ".$bid->property->agent->l_name}}</a></h5>
                            </div>
                        </div>
                        <br>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Submitted Date</th>
                                    <th>Bid Expires</th>
                                    <th>Bid Amount	</th>
                                    <th>Status </th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>{{\Carbon\Carbon::parse($bid->created_at)->format('l d M, h:i a')}}</td>
                                <td>{{\Carbon\Carbon::parse($bid->property->end_date)->format('l d M, h:i a')}}</td>
                                <td>{{env('APP_CURRENCY')}}{{$bid->bid_amount}}</td>
                                <td>
                                    @php
                                        //$minute = \Carbon\Carbon::parse($bid->property->start_date)->diffInMinutes(\Carbon\Carbon::parse($bid->property->end_date),$absolute = false);
                                        $minute = \Carbon\Carbon::parse(\Carbon\Carbon::now())->diffInMinutes(\Carbon\Carbon::parse($bid->property->end_date),$absolute = false);
                                    @endphp
                                    @if($minute<0)
                                        {{--@if($property->bids->max('bid_amount')==$bid->bid_amount)--}}
                                        @if($bid->where('status','!=','rejected')->max('bid_amount')==$bid->bid_amount)
                                            Win
                                        @else
                                            @if( $bid->status=='rejected' || $bid->status=='pending')
                                                Pending
                                            @elseif($bid->status=='rejected' || $bid->status=='pending' )
                                                Rejected
                                            @endif
                                        @endif
                                    @else
                                        @if($bid->status=='pending')
                                            Pending
                                        @elseif($bid->status=='rejected')
                                            Rejected
                                        @endif

                                        {{--
                                        {{--                                                       <option>{{$minute}} {{$property->bids->max('bid_amount')}}</option>--}}
                                    @endif
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('js')

@endpush
