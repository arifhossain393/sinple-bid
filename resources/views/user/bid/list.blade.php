@extends('layouts.backend.app')
@section('title','Property')

@push('css')

    <style>
        .employee-pic{
            width: 60px !important;
            height: 60px !important;
            margin-left: 0px !important;
            /* text-align: center; */
            /* vertical-align: middle; */
            margin: 0 auto !important;
            display: block;
            border-radius: 50%;
        }
        td {
            color: #000 !important;
        }
        th {

            color: #000 !important;
        }
    </style>
@endpush

@section('content')

    <!-- #END# Basic Examples -->
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                @if(session('successMsg'))
                    <div class="alert alert-success">


                        <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none';">×</button>
                        <span>
                                    <b>Success - </b>{{session('successMsg')}}</span>
                    </div>

                @endif
                <div class="header">
                    <h2 class="text-left">
                  Bids
                    </h2>
{{--                    <h2 class="text-right">--}}
{{--                        <a  href="{{route('property.create')}}" class="btn btn-primary" href="">Add New   <i class="material-icons">add</i></a>--}}
{{--                    </h2>--}}

                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th width="5%">SL</th>
                                <th width="15%">Property Name </th>
                                <th width="15%">Agent Name</th>
                                <th width="10%">Guide Price </th>
                                <th width="10%">Bid Amount </th>
                                <th width="10%">Bid Submitted  </th>
                                <th width="10%">Bid Expires</th>
                                <th width="10%">Type </th>
                                <th width="10%">Status </th>
                                <th width="5%">Action</th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach($bids as $bid)
                                @php
                                    //$minute = \Carbon\Carbon::parse($bid->property->start_date)->diffInMinutes(\Carbon\Carbon::parse($bid->property->end_date),$absolute = false);
                                    $minute = \Carbon\Carbon::parse(\Carbon\Carbon::now())->diffInMinutes(\Carbon\Carbon::parse($bid->property->end_date),$absolute = false);
                                @endphp
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td><a target="_blank" href="{{route('property.details',$bid->property_id)}}">{{$bid->property->name}}</a></td>
                                    <td><a target="_blank" href="{{route('agent.details',$bid->property->agent->id)}}">{{$bid->property->agent->f_name." ".$bid->property->agent->l_name}}</a></td>
                                    <td>{{env('APP_CURRENCY')}}{{$bid->property->price}}</td>
                                    <td>{{env('APP_CURRENCY')}}{{$bid->bid_amount}}</td>
                                    <td>{{\Carbon\Carbon::parse($bid->created_at)->format('l d M, h:i a')}}</td>
                                    <td>{{\Carbon\Carbon::parse($bid->property->end_date)->format('l d M, h:i a')}}</td>
                                    <td>
                                        @if($bid->property->type=='1')
                                            Sale
                                        @endif
                                        @if($bid->property->type=='0')
                                            Rent
                                        @endif
                                    </td>
                                    <td>

                                            @if($minute<0)
                                                {{--@if($property->bids->max('bid_amount')==$bid->bid_amount)--}}
                                                @if($bids->where('status','!=','rejected')->max('bid_amount')==$bid->bid_amount)
                                                Win
                                                @else
                                                @if( $bid->status=='rejected' || $bid->status=='pending')
                                                    Pending
                                                    @elseif($bid->status=='rejected' || $bid->status=='pending' )
                                                    Rejected
                                                    @endif
                                                @endif
                                            @else
                                                @if($bid->status=='pending')
                                                    Pending
                                                    @elseif($bid->status=='rejected')
                                                    Rejected
                                                    @endif

                                                {{--
                                                {{--                                                       <option>{{$minute}} {{$property->bids->max('bid_amount')}}</option>--}}
                                            @endif

                                    </td>
                                    <td>

                                        <form action="#" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <a href="{{route('bid.show',$bid->id)}}" class="btn btn-info">View</a>
                                            {{--                                            <a href="{{route('property.edit',$bid->id)}}" class="btn btn-warning">Edit</a>--}}
                                            {{--                                            <button type="submit" class="btn btn-danger">Delete</button>--}}
                                        </form>

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>


@endpush
