@extends('layouts.backend.app')

@section('title','Add Review')

@push('css')
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
<style>
    .form-line {
        margin-left: 14px !important;
    }

    .btn.btn-danger {
        margin-left: -35% !important;
    }

</style>

@endpush

@section('content')
    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" >
            <div class="card">

                <div class="header">
                    <h2 class="card-inside-title">Your Review </h2>

                </div>
                <div class="body">
                    <form class="form-horizontal"method="POST" action="{{route('review.store')}}"  enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" value="{{ Auth::user()->id}}" name="user_id">
                        <input type="hidden" value="{{ Auth::user()->f_name}}" name="name">
                        <input type="hidden" value="{{ Auth::user()->image}}" name="image">
                        <h2 class="card-inside-title">Your Review:</h2>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea  rows="4" name="review" class="form-control no-resize" placeholder="Please write your review..."></textarea>
                                        @error('review')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-danger">SUBMIT</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Validation -->
    <!-- Advanced Validation -->

    <!-- #END# Advanced Validation -->
    <!-- Validation Stats -->

    <!-- #END# Validation Stats -->
@endsection

@push('js')
    <!-- Bootstrap Material Datetime Picker Plugin Js -->

    <script src="{{asset('assets/backend/plugins/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        });
    </script>


@endpush
