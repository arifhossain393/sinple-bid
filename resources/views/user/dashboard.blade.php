@extends('layouts.backend.app')

@section('title','dashboard')

@push('css')
    <style>
        .block-header h2 {

            color: #000 !important;

        }
    </style>
@endpush
@section('content')

            <div class="block-header">
                <h2><b> DASHBOARD</b></h2>
            </div>

            <!-- Widgets -->
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <a href="{{ route('bid.index') }}"><i class="material-icons">playlist_add_check</i></a>
                        </div>
                        <div class="content">
                            <div class="text">Total Bid</div>
                            <div class="number count-to" data-from="0" data-to="{{\App\Bid::where('user_id',auth()->user()->id)->count()}}" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <a href="{{ route('favorite.index') }}"> <i class="material-icons">help</i></a>
                        </div>
                        <div class="content">
                            <div class="text">Favourite</div>
                            <div class="number count-to" data-from="0" data-to="{{\App\Favorite::where('user_id',auth()->user()->id)->count()}}" data-speed="1000" data-fresh-interval="20"></div>
                        </div>
                    </div>
                </div>
            </div>

@endsection

@push('js')

@endpush
