@extends('layouts.backend.app')
@section('title','Property')

@push('css')

    <style>

        td {
            color: #000 !important;
        }
        th {

            color: #000 !important;
        }
    </style>
@endpush

@section('content')

    <!-- #END# Basic Examples -->
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                @if(session('successMsg'))
                    <div class="alert alert-success">


                        <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none';">×</button>
                        <span>
                                    <b>Success - </b>{{session('successMsg')}}</span>
                    </div>

                @endif
                <div class="header">
                    <h2 class="text-left">
                        Favorites
                    </h2>
                    {{--                    <h2 class="text-right">--}}
                    {{--                        <a  href="{{route('property.create')}}" class="btn btn-primary" href="">Add New   <i class="material-icons">add</i></a>--}}
                    {{--                    </h2>--}}

                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th width="5%">SL</th>
                                <th width="20%">Property Name </th>
                                <th width="15%">Agent Name</th>
                                <th width="10%">Amount</th>
                                <th width="25%">Bid Expires</th>
                                <th width="10%">Type </th>
                                <th width="15%">Action</th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach($favorites as $favorite)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td><a href="">{{$favorite->property->name}}</a></td>
                                    <td><a href="">{{$favorite->property->agent->f_name." ".$favorite->property->agent->l_name}}</a></td>
                                    <td>{{env('APP_CURRENCY')}}{{$favorite->property->price}}</td>
                                    <td>{{\Carbon\Carbon::parse($favorite->property->end_date)->format('l d M, h:i a')}}</td>
                                    <td>
                                        @if($favorite->property->type=='1')
                                            Sale
                                        @endif
                                        @if($favorite->property->type=='0')
                                            Rent
                                        @endif
                                    </td>

                                    <td>

                                        <form action="{{route('favorite.destroy',$favorite->id)}}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            <a target="_blank" href="{{route('property.details',$favorite->property_id)}}" class="btn btn-info">View</a>
                                            {{--                                            <a href="{{route('property.edit',$bid->id)}}" class="btn btn-warning">Edit</a>--}}
                                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger">Delete</button>
                                        </form>

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>


@endpush
