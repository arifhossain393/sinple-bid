@extends('layouts.frontend.app')

@section('title','Property-list')

@push('css')
    <style>
        .properties .home-in li span{
            font-size:14px
        }
        .pagination {
            display: inline-block;
            width: 100%;
            text-align: center;
            border-radius: 0px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .pagination .page-item {
            display: inline-block;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .pagination>.page-item:last-child>.page-link, .pagination>.page-item:last-child>.page-link {
            border-radius: 0px;
        }
        .pagination>.page-item:first-child>.page-link, .pagination>.page-item:first-child>.page-link {
            border-radius: 0px;
        }
        .pagination>.page-item .active>.page-link, .pagination>.page-item .active>.page-link:focus, .pagination>.page-item .active>.page-link:hover, .pagination>.page-item.active>.page-link, .pagination>.page-item.active>.page-link:focus, .pagination>.page-item.active>.page-link:hover {
            background: #4caf50;
        }
        .pagination>.page-item>.page-link:focus, .pagination>.page-item>.page-link:hover, .pagination>.page-item>.page-link:focus, .pagination>.page-item>.page-link:hover {
            background: #4caf50;
            color: #fff;
        }
        .pagination .page-item .page-link {
            display: inline-block;
            border: 1px solid #4caf50 !important;
            color: #4caf50;
            font-size: 19px;
            height: 43px;
            width: 43px;
            line-height: 41px;
            padding: 0px;
            margin-right: 5px;
            font-family: 'Montserrat', sans-serif;
        }
        .pagination .page-item .page-link:hover {
            background: #4caf50;
            color: #fff;
        }
        .properti-detsil .find-sec .btn {
	        margin-top: 8px !important;
	        float: left !important;
        }

        .pagination>.active>span{color:#fff!important;}
        .submit_btn{padding: 0% 40%;background: #1E2D41}
        .properti-detsil .finder{top:28px!important}
    </style>
@endpush

@section('content')
    <div class="sub-banner">
        <div class="overlay">
            <div class="container">
                <h1>Properties for Bid</h1>
                <ol class="breadcrumb">
                    <li class="pull-left">Properties for Bid</li>
                    <li><a href="#">Home</a></li>
                    <li class="active">Properties for Bid</li>
                </ol>
            </div>
        </div>
    </div>

    <!--======= PROPERTY =========-->
    <section class="properties white-bg" id="app">
        <div class="container">
            <!--======= TITTLE =========-->

            <!--======= PROPERTIES DETAIL PAGE =========-->
            <section class="properti-detsil">
                <div class="container">
                    <div class="row">

                        <!--======= LEFT BAR =========-->
                        <div class="col-sm-9">

                            <!--======= PROPERTY FEATURES =========-->
                            <section class="info-property location">
                                <h5 class="tittle-head">Property location</h5>
                                <div class="inner"> </div>
                                <div class="mapsection">
{{--                                    <iframe--}}
{{--                                        style="border:0; "--}}
{{--                                        width="100%"--}}

{{--                                        frameborder="0"--}}
{{--                                        scrolling="no"--}}
{{--                                        marginheight="0"--}}
{{--                                        marginwidth="0"--}}
{{--                                        src="https://maps.google.com/maps?q={{$address}}&hl=es;z=14&amp;output=embed"--}}
{{--                                    >--}}
{{--                                    </iframe>--}}
                                    <div id="map_canvas" style="width: 100%;height: 400px">

                                    </div>

                                    <br />

                                </div>

                            </section>
                        </div>

                        <!--======= RIGT SIDEBAR =========-->
                        <div class="col-sm-3 side-bar">

                            <!--======= FIND PROPERTY =========-->
                            <div class="finder">

                                <!--======= FORM SECTION =========-->
                                <div class="find-sec">
                                    <h5>Search for properties</h5>
                                    <ul class="row">
                                        <form action="{{route('search.all.property')}}" method="GET">
                                        <!--======= FORM =========-->
                                        <li class="col-sm-12">
                                            <input class="selectpicker" id="searchTextField" type="text" size="50" placeholder="Enter Post Code/Area" autocomplete="on" runat="server" />
                                            <input type="hidden" id="address" name="address" />
                                            <input type="hidden" id="latitude" name="latitude" />
                                            <input type="hidden" id="longitude" name="longitude" />
                                        </li>

                                        <!--======= FORM =========-->
                                        <li class="col-sm-12">
                                            <select class="selectpicker" name="distance">
                                                <option value="1">1 Miles</option>
                                                @for($i=5;$i<=30;$i=$i+5)
                                                    <option value="{{$i}}">{{$i}} Miles</option>
                                                @endfor
                                                <option value="40" selected>40 Miles</option>
                                            </select>
                                        </li>

                                        <li class="col-sm-12">
                                            <select class="selectpicker" name="min_bed">
                                                <option value="">Minimum Bed</option>
                                                <option value="1">1 Bed</option>
                                                @for($i=2;$i<=5;$i=$i+1)
                                                    <option value="{{$i}}">{{$i}} Beds</option>
                                                @endfor
                                                <option value="6">5+</option>
                                            </select>
                                        </li>

                                        <!--======= FORM =========-->
                                        <li class="col-sm-12">
                                            <select class="selectpicker" name="max_bed">
                                                <option value="">Maximum Bed</option>
                                                <option value="1">1 Bed</option>
                                                @for($i=2;$i<=4;$i=$i+1)
                                                    <option value="{{$i}}">{{$i}} Beds</option>
                                                @endfor
                                                <option value="6">5+</option>
                                            </select>
                                        </li>
                                        <li class="col-sm-12">
                                            <select class="selectpicker" name="min_price">
                                                <option value="">Minimum price</option>
                                                @for($i=50000;$i<=1000000;$i=$i+10000)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                                @for($i=1100000;$i<=10000000;$i=$i+100000)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </li>
                                        <li class="col-sm-12">
                                            <select class="selectpicker" name="max_price">
                                                <option value="">Maximum Price</option>
                                                @for($i=50000;$i<=1000000;$i=$i+10000)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                                @for($i=1100000;$i<=10000000;$i=$i+100000)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </li>
                                        <li class="col-sm-12">
{{--                                            <a href="" class="btn search_btn">SEARCH</a> --}}
                                            <button type="submit" class="btn submit_btn">SEARCH</button>
                                        </li>
                                        </form>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <!--======= PROPERTIES ROW =========-->
            <ul class="row" style="margin-top: 15px;">

                @foreach($properties as $property)
                <!--======= PROPERTY =========-->
                <li class="col-sm-4" style="margin-top:10px">
                    <!--======= TAGS =========-->

                    <span class="tag font-montserrat @if($property->type=='1')sale @elseif($property->type=='0') rent @endif">
                        FOR @if($property->type=='1')SALE @elseif($property->type=='0') RENT @endif
                    </span>

                    <section>
                        <!--======= IMAGE =========-->
                        <div class="img"> <img width="400" height="300" src="{{asset('uploads/property/'.$property->image['0'])}}" alt="" >
                            <!--======= IMAGE HOVER =========-->

                            <div class="over-proper"> <a href="{{route('property.details',$property->id)}}" class="btn font-montserrat">more details</a> </div>
                        </div>
                        <!--======= HOME INNER DETAILS =========-->
                        <ul class="home-in">
                            <li><span><i class="fa fa-home"></i>{{$property->living}} Livingrooms</span></li>
                            <li><span><i class="fa fa-bed"></i> {{$property->bed}} Bedrooms</span></li>
                            <li><span><i class="fa fa-tty"></i> {{$property->bath}} Bathrooms</span></li>
                        </ul>
                        <!--======= HOME DETAILS =========-->
                        <div class="detail-sec">
                            <a href="{{route('property.details',$property->id)}}" class="font-montserrat">
                                {{Str::limit($property->name,'30','...')}}
                            </a> <span class="locate"><i class="fa fa-map-marker"></i> {{Str::limit($property->address,'40','...')}},{{$property->city}}</span>
{{--                            <p>{!! Str::limit($property->description,'25','...') !!}</p>--}}
                            <div class="share-p"> <span class="price font-montserrat">{{env('APP_CURRENCY')}}{{number_format($property->price, 2, '.', ',')}}</span>

                                <a style="cursor: pointer;" v-if="user!='0'" >
                                    <i v-if="checkCartItem({{$property->id}})" @click.prevent="exitInFavorite" class="fa fa-heart"></i>
                                    <i v-else class="fa fa-heart-o" @click.prevent="addToFavorite({{$property->id}})"></i>
                                </a>
                                <a style="cursor: pointer;" v-else href="{{route('login')}}"  > <i  class="fa fa-heart-o"></i></a>
                            </div>
                        </div>
                    </section>
                </li>
                @endforeach

            </ul>
            <nav>

                @if(method_exists($properties,'links'))
                <ul class="pagination">
                    {{ $properties->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}
                </ul>
                @endif
            </nav>
        </div>
    </section>
@endsection

@push('js')

    <script>
        // jQuery(function($) {
        //     <!-- Asynchronously Load the map API  -->
        //     var script = document.createElement('script');
        //     document.body.appendChild(script);
        // });
        var options = {
            componentRestrictions: {country: "uk"}
        };
        function initialize() {
            var input = document.getElementById('searchTextField');
            var autocomplete = new google.maps.places.Autocomplete(input,options);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                document.getElementById('address').value = place.name;
                document.getElementById('latitude').value = place.geometry.location.lat();
                document.getElementById('longitude').value = place.geometry.location.lng();
                //alert("This function is working!");
                //alert(place.name);
                // alert(place.address_components[0].long_name);
            });
            google.maps.event.addDomListener(window, 'load', initialize)
            var map;
            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                mapTypeId: 'roadmap'
            };
            <!-- Display a map on the page -->
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
            map.setTilt(45);
            <!-- Multiple Markers -->
            // var markers = [
            //     ['Bondi Beach', -33.890542, 151.274856, 4],
            //     ['Coogee Beach', -33.923036, 151.259052, 5],
            //     ['Cronulla Beach', -34.028249, 151.157507, 3],
            //     ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
            //     ['Maroubra Beach', -33.950198, 151.259302, 1]
            // ];
            var markers = [
                    @foreach($properties as $property)
                [{{$property->city}},{{$property->latitude}},{{$property->longitude}},{{$loop->index+1}}],
                @endforeach
            ];
            <!-- Info Window Content -->
            var infoWindowContent = [
                    @foreach($properties as $property)
                ['<div class="info_content">' +
                '<h6>{{$property->address}}</h6>' +
                '</div>'],
                @endforeach
            ];
            <!-- Display multiple markers on a map -->
            var infoWindow = new google.maps.InfoWindow(), marker, i;
            <!-- Loop through our array of markers & place each one on the map   -->
            for( i = 0; i < markers.length; i++ ) {
                var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: markers[i][0]
                });
                <!-- Allow each marker to have an info window     -->
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infoWindow.setContent(infoWindowContent[i][0]);
                        infoWindow.open(map, marker);
                    }
                })(marker, i));
                <!-- Automatically center the map fitting all markers on the screen -->
                map.fitBounds(bounds);
            }
            <!-- Override our map zoom level once our fitBounds function runs (Make sure it only runs once) -->
            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
                this.setZoom(10);
                google.maps.event.removeListener(boundsListener);
            });
        }
    </script>
@endpush
