@extends('layouts.frontend.app')

@section('title','Contact-Us')

@push('css')
<style>
    .contact-form label {

        color: #000 !important;

    }
</style>
@endpush

@section('content')
    <div class="sub-banner">
        <div class="overlay">
            <div class="container">
                <h1>CONTACT</h1>
                <ol class="breadcrumb">
                    <li class="pull-left">CONTACT</li>
                    <li><a href="#">Home</a></li>
                    <li class="active">CONTACT</li>
                </ol>
            </div>
        </div>
    </div>

    <!--======= MAP =========-->
    <!--  <div id="map"></div> -->
    <!--======= CONTACT =========-->
    <section class="contact">

        <!--======= CONTACT INFORMATION =========-->
        <div class="contact-info">
            <div class="container">
                <!--======= CONTACT =========-->
                <ul class="row con-det">

                    <!--======= ADDRESS =========-->
                    <li class="col-md-4"> <i class="fa fa-map-marker"></i>
                        <p style="color: #000!important;">18 Griffin Court, Luton England UK,LU2 DSX</p>

                        <!--======= EMAIL =========-->
                    <li class="col-md-4"> <i class="fa fa-phone"></i>
                        <p style="color: #000!important;">Tel  :  020 3805 58578</p>
                    </li>

                    <!--======= ADDRESS =========-->
                    <li class="col-md-4"> <i class="fa fa-envelope-o"></i>
                        <p style="color: #000!important;margin-top: -6px !important;">Email  : info@clicks2bid.co.uk</p>
                    </li>
                </ul>

                <!--======= CONTACT FORM =========-->

            </div>
        </div>
        <div class="contact-form">
            <div class="container">

                <!--======= TITTLE =========-->
                <div class="tittle"> <img src="{{asset('assets/frontend/images/head-top.png')}}" alt="">
                    <h3>feel free to communicate with us</h3>
                    <br>

                </div>
                <div id="contact_message" class="success-msg"> <i class="fa fa-paper-plane-o"></i>Thank You. Your Message has been Submitted</div>
                <form role="form" id="contact_form" method="post" action="{{route('contact.store')}}" >
                    @csrf
                    <ul class="row">
                        <li class="col-sm-6">
                            <label class="font-montserrat">your name *
                                <input type="text" class="form-control" name="name" id="name" placeholder="">
                                @error('name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </label>
                        </li>
                        <li class="col-sm-6">
                            <label class="font-montserrat">your e-mail *
                                <input type="text" class="form-control" name="email" id="email" placeholder="">
                                @error('email')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </label>
                        </li>
                        <li class="col-sm-6">
                            <label class="font-montserrat">Phone *
                                <input type="text" class="form-control" name="phone" id="phone" placeholder="">
                                @error('phone')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </label>
                        </li>
                        <li class="col-sm-6">
                            <label class="font-montserrat">Subject
                                <input type="text" class="form-control" name="subject" id="subject" placeholder="">
                                @error('subject')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </label>
                        </li>
                        <li class="col-sm-12">
                            <label class="font-montserrat">message
                                <textarea class="form-control" name="message" id="message" rows="5" placeholder=""></textarea>
                                @error('message')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </label>
                        </li>
                        <li class="col-sm-12">
                            <button type="submit" value="submit" class="btn font-montserrat" id="btn_submit" onClick="proceed();">Send message</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </section>

    <section>
        <div class="mapsection">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2463.204519109898!2d-0.40043838478363625!3d51.87547919154534!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876483daf59747d%3A0x9344ccdf09edb7a5!2sRedrow%20-%20Saxon%20Square%2C%20Luton!5e0!3m2!1sen!2sbd!4v1568809184216!5m2!1sen!2sbd" width="900" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </section>

@endsection

@push('js')

@endpush
