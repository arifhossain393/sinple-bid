@extends('layouts.frontend.app')

@section('title','Agent Sign Up')

@push('css')
<style>
    textarea {
        border: 2px solid aliceblue !important;
        padding: 20px !important;
        width: 570px !important;
        resize: both !important;
        overflow: auto !important;
        height: 200px !important;
        background-color: white !important;
        color: #000 !important;
    }
    #app {
        background-color: #eaeaea !important;
    }

</style>
@endpush

@section('content')
    <div class="row clearfix">
        <div class="sub-banner">
            <div class="overlay">
                <div class="container">
                    <h1>Agent Register form</h1>
                    <ol class="breadcrumb">
                        <li class="pull-left">register form</li>
                        <li><a href="#">Home</a></li>
                        <li class="active">register form</li>
                    </ol>
                </div>
            </div>
        </div>

        <!--======= PROPERTIES DETAIL PAGE =========-->
        <div class="container login-container">
            <div class="row">

                <div class="col-md-8 login-form-1 col-md-offset-2">
                    <!--   <div class="login-logo">
                          <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                      </div> -->
                    <h3>Agent Registration</h3>
                    <form id="form_validation" method="post" action="{{route('agent-sign-up.store')}}" enctype="multipart/form-data">
                        @csrf
                    <div class="form-group">
                        <input type="text" name="c_name" class="form-control" placeholder="Full Company Name" value="{{old('c_name')}}" />
                        @error('c_name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="text" name="f_name" class="form-control" placeholder="Director First Name" value="{{old('f_name')}}" />
                        @error('f_name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="text" name="l_name" class="form-control" placeholder="Last Name" value="{{old('l_name')}}" />
                        @error('l_name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="text" name="address_1" class="form-control" placeholder="Address 1" value="{{old('address_1')}}" />
                        @error('address_1')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="text" name="address_2" class="form-control" placeholder="Address 2" value="{{old('address_2')}}" />
                        @error('address_2')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="text" name="city" class="form-control" placeholder="City" value="{{old('city')}}" />
                        @error('city')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <select name="county" class="form-control" id="sel1">
                            <option value="">County</option>
                            @foreach($counties as $county)
                                <option value="{{$county->id}}">{{$county->county}}</option>
                            @endforeach
                        </select>

                        @error('county')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input type="text" name="postcode" class="form-control" placeholder="Post Code" value="{{old('postcode')}}" />
                        @error('postcode')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <select name="country" class="form-control" id="sel1">
                            <option value="">Country</option>
                            @foreach($countries as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                        @error('country')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input type="text" name="branch" class="form-control" placeholder="No of Branches" value="{{old('branch')}}" />
                        @error('branch')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="text" name="phone" class="form-control" placeholder="Phone number" value="{{old('phone')}}" />
                        @error('phone')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Your Email *" value="{{old('email')}}" />
                        @error('email')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Your Password *" value="" />
                        @error('password')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password *" value="" />
                        @error('confirm_password')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <textarea rows="6" cols="50" name="description" placeholder="About Your Self...">{{old('description')}}</textarea>
                        @error('description')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="file" name="image" value="Upload your Company logo" />
                        @error('image')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btnSubmit" value="Submit" />
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush
