<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">


    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') - {{ config('app.name', 'clicks2bid') }}</title>
    <meta name="keywords" content="HTML5,CSS3,HTML,Template,Multi-Purpose,M_Adnan,Corporate Theme,Realtor | Real Estate HTML5 Templates" >
    <meta name="description" content="Realtor | Real Estate HTML5 Template">
    <meta name="author" content="M_Adnan">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- FONTS ONLINE -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Play|Roboto&display=swap" rel="stylesheet" type="text/css">


    <!-- old form external link start -->
    <!-- <link href="css/css/bootstrap.min.css" rel="stylesheet" type="text/css"> -->

    <!-- old form external link end -->

    <!--MAIN STYLE-->
    <link href="{{asset('assets/frontend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/css/main.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/css/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/css/responsive.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <link href="{{asset('assets/frontend/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <style>
        .element {
            font-size: 10px !important;
        }
        footer .loc-info p i {

            margin-top: 1px !important;

        }

    </style>
@stack('css')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    {{--<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>--}}
    {{--<script>tinymce.init({selector:'textarea'});</script>--}}
</head>
<body >
<!-- Page Wrap ===========================================-->
<div id="wrap" class="home-1">



    <!--======= HEADER =========-->
@include('layouts.frontend.partial.header')




@include('sweetalert::alert')
    <div id="app">
        @yield('content')
    </div>


<!--======= FOOTER =========-->
@include('layouts.frontend.partial.footer')


<!--======= RIGHTS =========-->
@include('layouts.frontend.partial.rights')
</div>



<script src="{{asset('assets/frontend/js/jquery-1.11.0.min.js')}}"  ></script>
<script src="{{asset('js/app.js')}}" defer> </script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&libraries=places&callback=initialize"
        async defer></script>
<script src="{{asset('assets/frontend/js/wow.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/bootstrap-select.js')}}"></script>

<script src="{{asset('assets/frontend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.stellar.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.flexslider-min.js')}}"></script>
<script src="{{asset('assets/frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.sticky.js')}}"></script>
<script src="{{asset('assets/frontend/js/own-menu.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.nouislider.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>

<script src="{{asset('assets/frontend/js/main.js')}}"></script>

@if(auth()->check())
    <script>
        window.User = {!! auth()->user()  !!}
    </script>
@else
    @php
        $user = '0';
    @endphp
    <script>
        window.User = {!! $user !!}
    </script>
@endif
@stack('js')
<!-- <script type="text/javascript">
  /*-----------------------------------------------------------------------------------*/
/*    PRICE RANGE
/*-----------------------------------------------------------------------------------*/
£("#price-range").noUiSlider({
  range: {
      'min': [ 0 ],
      'max': [ 10000000]},
  start: [0, 10000000],
       connect:true,
       serialization:{
           lower: [
         £.Link({
          target: £("#price-min")
        })],
   upper: [
          £.Link({
          target: £("#price-max")
        })],
   format: {
      // Set formatting
          decimals: 0,
          prefix: '£'
  }}
});
</script> -->


</body>

</html>


