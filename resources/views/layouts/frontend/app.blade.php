<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>@yield('title') - {{ config('app.name', 'GMS-Estate') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/newfrontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/newfrontend/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/newfrontend/css/bootstrap-submenu.css') }}">

    <link rel="stylesheet" type="text/css" href="{{asset('assets/newfrontend/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/newfrontend/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{asset('assets/newfrontend/css/leaflet.css') }}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/newfrontend/css/map.css') }}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/newfrontend/fonts/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/newfrontend/fonts/flaticon/font/flaticon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/newfrontend/fonts/linearicons/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/newfrontend/css/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/newfrontend/css/dropzone.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/newfrontend/css/slick.css') }}">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/newfrontend/css/style.css') }}">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('assets/newfrontend/css/skins/default.css') }}">

    <!-- Favicon icon -->
    <!-- <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" > -->

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,300,700">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/newfrontend/css/ie10-viewport-bug-workaround.css') }}">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script  src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="{{asset('assets/newfrontend/js/ie-emulation-modes-warning.js') }}"></script>
    @stack('css')

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script  src="js/html5shiv.min.js"></script>
    <script  src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div class="page_loader"></div>

    <!--======= HEADER =========-->
    @include('layouts.frontend.partial.header')

    @include('sweetalert::alert')
    @yield('content')

    <!--======= FOOTER =========-->
    @include('layouts.frontend.partial.footer')

    <!-- Full Page Search -->
    <div id="full-page-search">
        <button type="button" class="close">×</button>
        <form action="http://storage.googleapis.com/themevessel-products/fort/index.html#">
            <input type="search" value="" placeholder="type keyword(s) here" />
            <button type="submit" class="btn btn-sm button-theme">Search</button>
        </form>
    </div>

    <script src="{{asset('assets/newfrontend/js/jquery-2.2.0.min.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/popper.min.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/bootstrap.min.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/bootstrap-submenu.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/rangeslider.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/jquery.mb.YTPlayer.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/bootstrap-select.min.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/jquery.scrollUp.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/leaflet.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/leaflet-providers.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/leaflet.markercluster.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/dropzone.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/slick.min.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/jquery.filterizr.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{asset('assets/newfrontend/js/jquery.countdown.js') }}"></script>
    {{-- <script src="{{asset('assets/newfrontend/js/maps.js') }}"></script> --}}
    <script src="{{asset('assets/newfrontend/js/app.js') }}"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{asset('assets/newfrontend/js/ie10-viewport-bug-workaround.js') }}"></script>
    <!-- Custom javascript -->
    <script src="{{asset('assets/newfrontend/js/ie10-viewport-bug-workaround.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&libraries=places&callback=initialize"
        async defer></script>
        @if(auth()->check())
            <script>
                window.User = {!! auth()->user()  !!}
            </script>
        @else
            @php
                $user = '0';
            @endphp
            <script>
                window.User = {!! $user !!}
            </script>
        @endif
    @stack('js')
</body>

</html>
