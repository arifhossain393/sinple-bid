    <!-- Intro section start -->
    <div class="intro-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-8 col-sm-12">
                    <div class="intro-text">
                        <h3>Do You Have Questions ?</h3>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12">
                    <a href="{{route('contact.index')}}" class="btn btn-md">Get in Touch</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Intro section end -->

    <!-- Footer start -->
    <footer class="footer">
        <div class="container footer-inner">
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                    <div class="footer-item clearfix">
                        <h4>
                            Contact
                        </h4>
                        <ul class="contact-info">
                            <li>
                                <i class="flaticon-pin"></i>LU1 1HS, England, UK
                            </li>
                            <li>
                                <i class="flaticon-mail"></i><a
                                    href="mailto:info@gmswebdesign.co.uk">info@gmswebdesign.co.uk</a>
                            </li>
                            <li>
                                <i class="flaticon-phone"></i><a href="tel:020 3805 5857">020 3805 5857</a>
                            </li>
                            <li>
                                <i class="flaticon-fax"></i>+0477 85X6 552
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                        <div class="social-list-2">
                            <ul>
                                <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                    <div class="footer-item">
                        <h4>
                            Useful Links
                        </h4>
                        <ul class="links">
                            <li>
                                <a href="{{route('index')}}">Home</a>
                            </li>
                            <li>
                                <a href="#">About Us</a>
                            </li>
                            <li>
                                <a href="{{route('faq')}}">FAQ</a>
                            </li>
                            <li>
                                <a href="{{route('resource')}}">Resources</a>
                            </li>
                            <li>
                                <a href="{{route('privacy')}}">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="{{route('term')}}">Terms & Condition</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-3 col-md-6 col-sm-6">
                    <div class="recent-properties footer-item">
                        <h4>Recent Properties</h4>
                        <div class="media mb-4">
                            <a class="pr-3" href="properties-details.html">
                                <img class="media-object" src="{{asset('assets/newfrontend/img/properties/small-properties-1.jpg') }}"
                                    alt="small-properties">
                            </a>
                            <div class="media-body align-self-center">
                                <h5>
                                    <a href="properties-grid-rightside.html">Relaxing Apartment</a>
                                </h5>
                                <div class="listing-post-meta">
                                    <a href="#"><i class="fa fa-calendar"></i> Oct 27, 2018 </a> | £345,00
                                </div>
                            </div>
                        </div>
                        <div class="media mb-4">
                            <a class="pr-3" href="properties-details.html">
                                <img class="media-object" src="{{asset('assets/newfrontend/img/properties/small-properties-2.jpg') }}"
                                    alt="small-properties">
                            </a>
                            <div class="media-body align-self-center">
                                <h5>
                                    <a href="properties-grid-rightside.html">Office Apartment</a>
                                </h5>
                                <div class="listing-post-meta">
                                    <a href="#"><i class="fa fa-calendar"></i> Feb 19, 2019 </a> | £415,00
                                </div>
                            </div>
                        </div>
                        <div class="media">
                            <a class="pr-3" href="properties-details.html">
                                <img class="media-object" src="{{asset('assets/newfrontend/img/properties/small-properties-3.jpg') }}"
                                    alt="small-properties">
                            </a>
                            <div class="media-body align-self-center">
                                <h5>
                                    <a href="properties-grid-rightside.html">Real Luxury Villa</a>
                                </h5>
                                <div class="listing-post-meta">
                                    <a href="#"><i class="fa fa-calendar"></i> Oct 12, 2018 </a> | £345,00
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-item clearfix">
                        <h4>Subscribe</h4>
                        <div class="Subscribe-box">
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                mollit.</p>
                            <form class="form-inline" action="#" method="GET">
                                <input type="text" class="form-control mb-sm-0" id="inlineFormInputName3"
                                    placeholder="Email Address">
                                <button type="submit" class="btn"><i class="fa fa-paper-plane"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12">
                    <p class="copy sub-footer">© 2019 <a href="#">GMS.</a> Design & Developed by GMS Webdesign.</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer end -->
