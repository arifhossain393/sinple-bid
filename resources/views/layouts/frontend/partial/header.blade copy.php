<header class="sticky">
    <div class="container">

        <!--======= LOGO =========-->
        <div class="logo"> <a href="{{route('index')}}"><img src="{{asset('assets/frontend/images/image/logo.png')}}" alt="" ></a> </div>
        <!--======= NAV =========-->
        <nav>

            <!--======= MENU START =========-->
            <div class="nav-bg">
                <li class="regi" style="float:right;"><a class="regi" href="#">REGISTER</a>
                    <ul id="dropdown_sign_in">
                        <li><a href="{{route('user-sign-up.index')}}">AS A USER</a></li>
                        <li><a href="{{route('agent-sign-up.index')}}">AS AN AGENT</a></li>
                    </ul>
                </li>
                <li class="regi" style="float:right;"><a class="regi" href="#">SIGN IN</a>
                    <ul id="dropdown_sign_in">
                        <li><a href="{{route('login')}}">AS A USER</a></li>
                        <li><a href="{{route('agent.auth.login')}}">AS AN AGENT</a></li>
                    </ul>
                </li>
            </div>

            <br>
            <ul class="ownmenu">
                <li><a href="{{route('buy')}}">BUY</a>
                </li>
                {{--<li><a href="{{route('sale')}}">SALE</a></li>--}}
                <li><a href="{{route('rent')}}">RENT</a></li>
                <li><a href="{{route('bid')}}">Bid</a></li>
                <li><a href="{{route('find-agent')}}">FIND AGENT</a></li>
                <li><a href="{{route('how-it-works')}}">HOW IT WORKS</a></li>
                <li><a href="{{route('contact.index')}}">CONTACT</a></li>
            </ul>
            <br>




            <!--======= SUBMIT COUPON =========-->
            <div class="sub-nav-co"> <a href="#."><i class="fa fa-search"></i></a> </div>
        </nav>
    </div>
</header>
