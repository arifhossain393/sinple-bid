<!-- Top header start -->
    <header class="top-header" id="top-header-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-9 col-sm-7">
                    <div class="list-inline">
                        <a href="tel:1-8X0-666-8X88"><i class="fa fa-phone"></i>020 3805 5857</a>
                        <a href="tel:info@themevessel.com"><i class="fa fa-envelope"></i>info@gmswebdesign.co.uk</a>
                        <a href="tel:info@gmswebdesign.co.uk"><i class="flaticon-pin"></i>Mon - Sun: 8:00am - 6:00pm</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-3 col-sm-5">
                    <ul class="top-social-media pull-right">
                        <li>
                            <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
                        </li>
                        <li>
                            <a href="#" class="linkedin"><i class="fa fa-linkedin"></i> </a>
                        </li>
                        <li>
                            <a href="#" class="rss"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- Top header end -->

    <!-- Main header start -->
    <header class="main-header fixed-header-2">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand company-logo" href="{{route('index')}}">
                    <img src="{{asset('assets/newfrontend/img/logos/black-logo.png') }}" alt="logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav header-ml">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('index')}}">
                                Home
                            </a>
                        </li>

                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Properties
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li><a class="dropdown-item" href="{{route('buy')}}">Buy</a></li>
                                <li><a class="dropdown-item" href="{{route('rent')}}">Rent</a></li>
                                <li><a class="dropdown-item" href="{{route('bid')}}">Bid</a></li>
                            </ul>
                        </li>

                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('find-agent')}}">
                                Find Agent
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">
                                How It Works
                            </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('contact.index')}}">
                                Contact
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon-logout"></i>Sign In
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li><a class="dropdown-item" href="{{route('login')}}">As A User</a></li>
                                <li><a class="dropdown-item" href="{{route('agent.auth.login')}}">As An Agent</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown active">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flaticon-user"></i>Register
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <li><a class="dropdown-item" href="{{route('user-sign-up.index')}}">As A User</a></li>
                                <li><a class="dropdown-item" href="{{route('agent-sign-up.index')}}">As An Agent</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!-- Main header end -->
