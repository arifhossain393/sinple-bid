<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            @if (Auth::user()->image)
                <img src="{{asset('uploads/user/'.Auth::user()->image)}}"  width="80" height="80" alt="User" />
            @else
                <img src="{{asset('assets/backend/images/user.png')}}"  width="80" height="80" alt="User" />
            @endif

        </div>
        {{--<div class="image">--}}
            {{--<img src="{{asset('uploads/user/'.Auth::user()->image)}}" width="48" height="48" alt="User" />--}}
        {{--</div>--}}
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->f_name }}</div>
            <div class="email">{{ Auth::user()->email }}</div>
            <div class="btn-group user-helper-dropdown">
                {{--<i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>--}}
                {{--<ul class="dropdown-menu pull-right">--}}
                    {{--<li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>--}}
                    {{--<li role="separator" class="divider"></li>--}}
                    {{--<li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>--}}

                    {{--<li role="separator" class="divider"></li>--}}
                    {{--<li>--}}
                        {{--<a class="dropdown-item" href="{{ route('logout') }}"--}}
                           {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();">--}}
                            {{--<i class="material-icons">input</i>Sign Out--}}
                        {{--</a>--}}
                        {{--<a class="dropdown-item" href="{{ route('logout') }}"--}}
                          {{-->--}}
                            {{--<i class="material-icons">input</i>Sign Out--}}
                        {{--</a>--}}

                        {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                            {{--@csrf--}}
                        {{--</form>--}}

                {{--</ul>--}}
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>


                <li class="{{ Request::is('user') ? 'active' : '' }}">
                    <a href="{{route('user.dashboard')}}">
                        <i class="material-icons">dashboard</i>
                        <span>Home</span>
                    </a>
                </li>
                <li class="{{ Route::is('bid.index') ? 'active' : '' }}">
                    <a href="{{route('bid.index')}}" class="">
                        <i class="material-icons">gavel</i>
                        <span>My Bids</span>
                    </a>
                </li>
                <li class="{{ Route::is('favorite.index') ? 'active' : '' }}">
                    <a href="{{route('favorite.index')}}">
                        <i class="material-icons">favorite</i>
                        <span>Favourites</span>
                    </a>
                </li>
                <li class="{{ Route::is('review.index') ? 'active' : '' }}">
                    <a href="{{route('review.index')}}">
                        <i class="material-icons">face</i>
                        <span>Review</span>
                    </a>
                </li>
                <li class="{{ Request::is('user/settings*') ? 'active' : '' }}">
                    <a href="{{route('user.settings',Auth::user()->id)}}">
                        <i class="material-icons">group</i>
                        <span>My Profile</span>
                    </a>
                </li>

                <li class="header">system</li>
                <li>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="material-icons">input</i><span>Logout</span>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                </li>




        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2019<a href="javascript:void(0);">Clicks2Bid, All Rights Reserved.</a>.
        </div>

    </div>
    <!-- #Footer -->
</aside>
