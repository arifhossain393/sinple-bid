
@extends('layouts.frontend.app')
@section('title','Login')

@push('css')
    <style>
        body {

            color: #8a0d0d !important;
        }
        .form-control {

            color: #000 !important;

        }
        #app {
            background-color: #eaeaea !important;
        }
    </style>
@endpush

@section('content')
    <div class="row clearfix">
        <div class="sub-banner">
            <div class="overlay">
                <div class="container">
                    <h1>User Login form</h1>
                    <ol class="breadcrumb">
                        <li class="pull-left">Login form</li>
                        <li><a href="#">Home</a></li>
                        <li class="active">Login form</li>
                    </ol>
                </div>
            </div>
        </div>

        <!--======= PROPERTIES DETAIL PAGE =========-->
        <div class="container login-container">
            <div class="row">

                <div class="col-md-8 login-form-2 col-md-offset-2">
                    @if(session()->get('success'))
                        <div class="text-center">
                            <span class="alert alert-success">{{session()->get('success')}}</span>
                        </div>
                    @endif

                    <!-- <div class="login-logo">
                        <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                    </div> -->
                    <h3>User Login</h3>

                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Your Email *"  autocomplete="email" autofocus />
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Your Password *" autocomplete="current-password"/>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btnSubmit" value="Login" />
                        </div>
                        {{--<div class="form-group">--}}

                            {{--<a href="{{route('user.password.reset.get') }}" class="btnForgetPwd" >Forgot Password?</a>--}}
                        {{--</div>--}}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-center">
                                    <a href="{{route('user.password.reset.get') }}" value="Login" class="btnForgetPwd">Forgot Password?</a>
                                </div>
                                <div class="col-md-4 text-center">

                                </div>
                                <div class="col-md-4 text-center">
                                    <a href="{{route('user-sign-up.index')}}" class="btnForgetPwd" value="Login">Register</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush
