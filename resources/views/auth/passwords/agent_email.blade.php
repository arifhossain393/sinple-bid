
@extends('layouts.frontend.app')
@section('title','Login')

@push('css')
    <style>
        .contact-section {
            background: #ecebea;
        }
    </style>
@endpush

@section('content')
    <!-- Contact section start -->
    <div class="contact-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form content box start -->
                    <div class="form-content-box">
                        <!-- details -->
                        <div class="details">
                            @if(session()->get('success'))
                                <div class="text-center">
                                    <span class="alert alert-success">{{session()->get('success')}}</span>
                                </div>
                                @endif
                                @if(session()->get('error'))
                                <div class="text-center">
                                    <span class="alert alert-warning">{{session()->get('error')}}</span>
                                </div>
                            @endif
                            <!-- Logo -->
                            <a href="{{route('index')}}">
                                <img src="{{asset('assets/newfrontend/img/logos/black-logo.png') }}" class="cm-logo" alt="black-logo">
                            </a>
                            <!-- Name -->
                            <h3>Agent Password Reset</h3>
                            <!-- Form start -->
                            <form method="POST" action="{{ route('agent.password.reset.post') }}">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="input-text form-control @error('email') is-invalid @enderror" name="email" placeholder="Your Email *"  autocomplete="email" autofocus />
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>

                                <div class="form-group mb-0">
                                    <button type="submit" class="btn-md button-theme btn-block">Send</button>
                                </div>
                            </form>
                        </div>
                        <!-- Footer -->
                        <div class="footer">
                            <span>Don't have an account? <a href="{{route('agent-sign-up.index')}}">Register here</a></span>
                        </div>
                    </div>
                    <!-- Form content box end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Contact section end -->
@endsection

@push('js')

@endpush
