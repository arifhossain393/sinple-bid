
@extends('layouts.frontend.app')
@section('title','Login')

@push('css')
    <style>
        body {

            color: #8a0d0d !important;
        }
    </style>
@endpush

@section('content')
    <div class="sub-banner">
        <div class="overlay">
            <div class="container">
                <h1>Admin Password Reset form</h1>
                <ol class="breadcrumb">
                    <li class="pull-left">Login form</li>
                    <li><a href="#">Home</a></li>
                    <li class="active">Login form</li>
                </ol>
            </div>
        </div>
    </div>

    <!--======= PROPERTIES DETAIL PAGE =========-->
    <div class="container login-container">
        <div class="row">

            <div class="col-md-8 login-form-2 col-md-offset-2">
                <!-- <div class="login-logo">
                    <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                </div> -->
                @if(session()->get('success'))
                    <div class="text-center">
                        <span class="alert alert-success">{{session()->get('success')}}</span>
                    </div>
                @endif
                @if(session()->get('error'))
                    <div class="text-center">
                        <span class="alert alert-warning">{{session()->get('error')}}</span>
                    </div>
                @endif
                <h3>Admin Password Reset</h3>
                <form method="POST" action="{{ route('admin.password.mail.post') }}">
                    <input type="hidden" value="{{$token}}" name="token">
                    @csrf
                    <div class="form-group">
                        <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Your Email *"  autocomplete="email" autofocus />
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Your Password *" autocomplete="current-password"/>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control @error('confirm_password') is-invalid @enderror" name="confirm_password" placeholder="Your Confirm Password *" autocomplete="current-password"/>
                        @error('confirm_password')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btnSubmit" value="Submit" />
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush
