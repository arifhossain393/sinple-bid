@extends('layouts.frontend.app')

@section('title','Home')

@push('css')
<style>
    #testimonials .testi p {
        color: #000 !important;
    }
    #testimonials .testi .item span {
        color: #000 !important;

    }
    #testimonials .testi .item span {
        color: #000 !important;

    }

</style>
@endpush

@section('content')
    <div id="banner">
        <div class="flex-banner">
            <ul class="slides">
                <!--======= SLIDER =========-->
                <li> <img class="img-responsive" src="{{asset('assets/frontend/images/slider-img-1.jpg')}}" alt="" > </li>

                <!--======= SLIDER =========-->
                <li> <img class="img-responsive" src="{{asset('assets/frontend/images/slider-img-3.jpg')}}" alt="" > </li>
                <li> <img class="img-responsive" src="{{asset('assets/frontend/images/slider-img-4.jpg')}}" alt="" > </li>
                <!--   <li> <img class="img-responsive" src="images/slider-img-2.jpg" alt=""></li> -->
            </ul>
        </div>

        <section id="property-search">

            <div class="row">
                <div class="container">
                    <div class="col-md-12 property-search">
                        <form action="{{route('search-property')}}" method="GET">
                            <div class="form-row align-items-center">
                                <!-- main content -->
                                <div class="section-main">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group form-input-fields form-group-lg has-feedback">
                                                <div class="input-group" id="input-group">
                            <span class="select-search" id="selectoption">
                              <select class="custom-select mb-2 mb-sm-0" id="inlineFormCustomSelect" name="type">
                                  <option value="1" selected>FOR SALE</option>
                                  <option value="0">FOR RENT</option>
                              </select>
                            </span>
                                                    <input class="form-control input-search" id="searchTextField" type="text" size="50" placeholder="Enter Post code" autocomplete="on" runat="server" />
                                                    <input type="hidden" id="address" name="address" />
                                                    <input type="hidden" id="latitude" name="latitude" />
                                                    <input type="hidden" id="longitude" name="longitude" />

                                                    <!--   <span class="input-group-addon group-icon"> <span class="fa fa-map-marker"></span>
                                                      </span> -->
                                                    <button  type="submit" class="search_btn btn-home btn-lg btn-submit">
                                                        <span class="" aria-hidden="true"></span> Search
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>


    </div>
    <br/>  <br/>  <br/>




    <section class="slider_bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <h2 class="test">Latest Properties For Sale</h2>
                    <section class="property-slide">
                        <!--======= PROPERTY SLIDER =========-->
                        <div class="testi-slides">
                            <!--======= PROPERTY SLIDE =========-->

                           @foreach($sale_properties as $property)
                            <div class="plots">
                                <div class="row">
                                    <div class="col-xs-4"> <a href="{{route('property.details',$property->id)}}"> <img class="img-responsive" src="{{asset('uploads/property/'.$property->image['0'])}}" alt="" > </a> </div>
                                    <div class="col-xs-8">
                                        <div class="pri-info"> <span class="sale">{{Str::limit($property->name,'30','..')}}</span> <a class="f-mont" href="{{route('property.details',$property->id)}}">{{env('APP_CURRENCY')}}{{number_format($property->price, 2, '.', ',')}}</a>
                                            <p><i class="fa fa-map-marker"></i> {{$property->address}}</p>
                                            <div class="auther"> <img src="{{asset('uploads/agent/'.$property->agent->image)}}" alt="">
                                                <h6>{{$property->agent->name}}</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           @endforeach

                        </div>
                    </section>
                </div>
                <div class="col-md-6 col-sm-12">
                    <h2 class="test">Latest Properties For Rent</h2>
                    <section class="property-slide">
                        <!--======= PROPERTY SLIDER =========-->
                        <div class="testi-slidess">

                            @foreach($rent_properties as $property)
                                <div class="plots">
                                    <div class="row">
                                        <div class="col-xs-4"> <a href="{{route('property.details',$property->id)}}"> <img class="img-responsive" src="{{asset('uploads/property/'.$property->image['0'])}}" alt="" > </a> </div>
                                        <div class="col-xs-8">
                                            <div class="pri-info"> <span class="sale">{{Str::limit($property->name,'30','..')}}</span> <a class="f-mont" href="{{route('property.details',$property->id)}}">{{env('APP_CURRENCY')}}{{number_format($property->price, 2, '.', ',')}}</a>
                                                <p><i class="fa fa-map-marker"></i> {{$property->address}}</p>
                                                <div class="auther"> <img src="{{asset('uploads/agent/'.$property->agent->image)}}" alt="">
                                                    <h6>{{$property->agent->name}}</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>



    <!--======= SERVICES =========-->
    <section class="services  pt-50 pb-20">
        <div class="container">

            <!--======= TITTLE =========-->
            <div class="tittle"> <img src="{{asset('assets/frontend/images/head-top.png')}}" alt="">
                <h3>services we provide</h3>

            </div>
            <ul class="row">

                <!--======= SERVICE SECTION =========-->
                <li class="col-sm-3">
                    <section>
                        <img class="img-responsive" src="{{asset('assets/frontend/images/service-img-1.jpg')}}" alt="" >
                        <div class="icon"> <img src="{{asset('assets/frontend/images/icon-services-3.png')}}" alt=""> </div>
                        <div class="ser-hover">
                            <p>And when the odds are against him and their dangers work to do. You bet your life Speed Racer <a href="#." class="read-more">Read more <i class="fa fa-angle-double-right"></i></a> </p>
                        </div>
                        <a href="#." class="heading">Buy Property</a> </section>
                </li>


                <!--======= SERVICE SECTION =========-->
                <li class="col-sm-3">
                    <section>
                        <img class="img-responsive" src="{{asset('assets/frontend/images/service-img-2.jpg')}}" alt="" >
                        <div class="icon"> <img src="{{asset('assets/frontend/images/icon-services-3.png')}}" alt=""> </div>
                        <div class="ser-hover">
                            <p>And when the odds are against him and their dangers work to do. You bet your life Speed Racer <a href="#." class="read-more">Read more <i class="fa fa-angle-double-right"></i></a> </p>
                        </div>
                        <a href="#." class="heading">Sell Property</a> </section>
                </li>

                <!--======= SERVICE SECTION =========-->
                <li class="col-sm-3">
                    <section>
                        <img class="img-responsive" src="{{asset('assets/frontend/images/service-img-3.jpg')}}" alt="" >
                        <div class="icon"> <img src="{{asset('assets/frontend/images/icon-services-3.png')}}" alt=""> </div>
                        <div class="ser-hover">
                            <p>And when the odds are against him and their dangers work to do. You bet your life Speed Racer <a href="#." class="read-more">Read more <i class="fa fa-angle-double-right"></i></a> </p>
                        </div>
                        <a href="#." class="heading">Rent Property</a> </section>
                </li>

                <!--======= SERVICE SECTION =========-->
                <li class="col-sm-3">
                    <section>
                        <img class="img-responsive" src="{{asset('assets/frontend/images/service-img-4.jpg')}}" alt="" >
                        <div class="icon"> <img src="{{asset('assets/frontend/images/icon-services-4.png')}}" alt=""> </div>
                        <div class="ser-hover">
                            <p>And when the odds are against him and their dangers work to do. You bet your life Speed Racer <a href="#." class="read-more">Read more <i class="fa fa-angle-double-right"></i></a> </p>
                        </div>
                        <a href="#." class="heading">Agent Bidding System</a> </section>
                </li>
            </ul>
        </div>
    </section>





    <!--======= PROPERTY =========-->
    <section class="properties pt-50 pb-20">
        <div class="container">

            <!--======= TITTLE =========-->
            <div class="tittle"> <img src="{{asset('assets/frontend/images/head-top.png')}}" alt="">
                <h3>new properties list</h3>

                <br>

            </div>

            <!--======= PROPERTIES ROW =========-->
            <ul class="row" style="margin-top: 15px;" id="app">

            @foreach($properties as $property)
                <!--======= PROPERTY =========-->
                    <li class="col-sm-4" style="margin-top:10px">
                        <!--======= TAGS =========-->

                        <span class="tag font-montserrat @if($property->type=='1')sale @elseif($property->type=='0') rent @endif">
                        FOR @if($property->type=='1')SALE @elseif($property->type=='0') RENT @endif
                    </span>

                        <section>
                            <!--======= IMAGE =========-->
                            <div class="img"> <img width="400" height="300" src="{{asset('uploads/property/'.$property->image['0'])}}" alt="" >
                                <!--======= IMAGE HOVER =========-->

                                <div class="over-proper"> <a href="{{route('property.details',$property->id)}}" class="btn font-montserrat">more details</a> </div>
                            </div>
                            <!--======= HOME INNER DETAILS =========-->
                            <ul class="home-in">
                                <li><span><i class="fa fa-home"></i>{{$property->living}} Livingroom</span></li>
                                <li><span><i class="fa fa-bed"></i> {{$property->bed}} Bedroom</span></li>
                                <li><span><i class="fa fa-tty"></i> {{$property->bath}} Bathroom</span></li>
                            </ul>
                            <!--======= HOME DETAILS =========-->
                            <div class="detail-sec">
                                <a href="{{route('property.details',$property->id)}}" class="font-montserrat">
                                    {{Str::limit($property->name,'30','...')}}
                                </a> <span class="locate"><i class="fa fa-map-marker"></i> {{Str::limit($property->address,'40','...')}}</span>
                                {{--                            <p>{!! Str::limit($property->description,'25','...') !!}</p>--}}
                                <div class="share-p"> <span class="price font-montserrat">{{env('APP_CURRENCY')}}{{number_format($property->price, 2, '.', ',')}}</span>

                                    <a style="cursor: pointer;" v-if="user!='0'" >
                                        <i v-if="checkCartItem({{$property->id}})" @click.prevent="exitInFavorite" class="fa fa-heart"></i>
                                        <i v-else class="fa fa-heart-o" @click.prevent="addToFavorite({{$property->id}})"></i>
                                    </a>
                                    <a style="cursor: pointer;" v-else href="{{route('login')}}"  > <i  class="fa fa-heart-o"></i></a>
                                </div>
                            </div>
                        </section>
                    </li>
                @endforeach

            </ul>
        </div>
    </section>

    <!--======= TEAM =========-->
    <section id="team">
        <div class="container">
            <!--======= TITTLE =========-->
            <div class="tittle"> <img src="{{asset('assets/frontend/images/head-top.png')}}" alt="">
                <h3>our great agents</h3>
                <br>

            </div>
            <div class="row">
                <div class="col-md-6">

                    <!--======= TEAM ROW =========-->
                    <ul class="row">

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-1.jpg')}}" alt="">
                                <div class="team-over">
                                    <!--======= SOCIAL ICON =========-->
                                    <ul class="social_icons animated-6s fadeInUp">
                                        <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                        <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                        <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                        <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <div class="team-detail">
                                    <h6>David Martin</h6>
                                    <p>Founder</p>
                                </div>
                            </div>
                        </li>

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-2.jpg')}}" alt="">
                                <div class="team-over">
                                    <!--======= SOCIAL ICON =========-->
                                    <ul class="social_icons animated-6s fadeInUp">
                                        <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                        <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                        <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                        <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <div class="team-detail">
                                    <h6>Hendrick jack </h6>
                                    <p>co-Founder</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">

                    <!--======= TEAM ROW =========-->
                    <ul class="row">

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-3.jpg')}}" alt="">
                                <div class="team-over">
                                    <!--======= SOCIAL ICON =========-->
                                    <ul class="social_icons animated-6s fadeInUp">
                                        <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                        <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                        <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                        <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <div class="team-detail">
                                    <h6>charles edward </h6>
                                    <p>team leader </p>
                                </div>
                            </div>
                        </li>

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-4.jpg')}}" alt="">
                                <div class="team-over">
                                    <!--======= SOCIAL ICON =========-->
                                    <ul class="social_icons animated-6s fadeInUp">
                                        <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                        <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                        <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                        <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <div class="team-detail">
                                    <h6>jessica wevins </h6>
                                    <p>team leader</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!--======= TESTIMONILAS =========-->
    <section id="testimonials">
        <div class="container">

            <!--======= TITTLE =========-->
            <div class="tittle">
                <h3>some words from our customer</h3>
            </div>
            <div class="testi">

                <!--======= TESTIMONIALS SLIDERS CAROUSEL =========-->
                <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">
                    <div class="row">
                        <div class="col-md-12"> <img src="{{asset('assets/frontend/images/comment-icon.png')}}" alt="">
                            <div class="carousel-inner" role="listbox">

                                <!--======= SLIDER 1 =========-->
                                @foreach($review as $rev)
                                    @if($loop->index==0)
                                        <div class="item active">
                                        @else
                                        <div class="item">
                                        @endif
                                    <p>{!! $rev['review'] !!}</p>
                                    <h5>{{$rev->name}}</h5>
                                    <span>UK</span> </div>
                                @endforeach

                            </div>
                        </div>

                        <!--======= SLIDER AVATARS =========-->
                        <div class="col-md-12">
                            <ol class="carousel-indicators">
                                @foreach($review as $rev)
                                <li data-target="#carousel-example-generic" data-slide-to="{{$loop->index}}" class="@if($loop->index==0) active @endif"> <img src="{{asset('/uploads/user/'.$rev->image)}}" alt="" > </li>
                               @endforeach
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!--======= PARTHNER =========-->
    <section class="parthner pt-50 pb-20" >
        <div class="container">
            <h2 class="part">Our  Partners</h2>
            <br>
            <section class="customer-logos slider">
                <div class="slide"><img src="{{asset('assets/frontend/images/white.png')}}"/></div>
                <div class="slide"><img src="{{asset('assets/frontend/images/zoopla.png')}}"/></div>
                <div class="slide"><img src="{{asset('assets/frontend/images/prime-location-logo-1.png')}}"/></div>
                <div class="slide"><img src="{{asset('assets/frontend/images/bitmap.png')}}"/></div>
                <div class="slide"><img src="{{asset('assets/frontend/images/gmshost.png')}}"/></div>
                <div class="slide"><img src="{{asset('assets/frontend/images/white.png')}}"/></div>
            </section>
        </div>
    </section>
@endsection

@push('js')
    {{--<script type="text/javascript">--}}

        {{--function initialize() {--}}
            {{--var input = document.getElementById('searchTextField');--}}
            {{--var autocomplete = new google.maps.places.Autocomplete(input);--}}
            {{--google.maps.event.addListener(autocomplete, 'place_changed', function () {--}}
                {{--var place = autocomplete.getPlace();--}}
                {{--document.getElementById('address').value = place.name;--}}
                {{--document.getElementById('latitude').value = place.geometry.location.lat();--}}
                {{--document.getElementById('longitude').value = place.geometry.location.lng();--}}
                {{--//alert("This function is working!");--}}
                {{--//alert(place.name);--}}
                {{--// alert(place.address_components[0].long_name);--}}
            {{--});--}}
        {{--}--}}
        {{--google.maps.event.addDomListener(window, 'load', initialize)--}}
    {{--</script>--}}
    <script type="text/javascript">

        var options = {
            componentRestrictions: {country: "uk"}
        };
        function initialize() {
            var input = document.getElementById('searchTextField');
            var autocomplete = new google.maps.places.Autocomplete(input,options);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                document.getElementById('address').value = place.name;
                document.getElementById('latitude').value = place.geometry.location.lat();
                document.getElementById('longitude').value = place.geometry.location.lng();
                //alert("This function is working!");
                //alert(place.name);
                // alert(place.address_components[0].long_name);
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize)
    </script>
    <script>
        $('.carousel').carousel({
            interval: 4000
        })
    </script>
@endpush
