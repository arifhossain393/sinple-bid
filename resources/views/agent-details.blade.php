@extends('layouts.frontend.app')
@section('title','Agent-Details')
@push('css')
<style>
    img {
        margin-left: 10px !important;
    }
    .center-img {
        display: block!important;
        margin-left: auto;
        margin-right: auto;
        width: 50%!important;
        height: 155px;
    }
    b, strong {

        color: #000 !important;
    }
    .blog-text,.blog-text>* {
        color:#000;
    }
</style>
@endpush
@section('content')

    <div class="sub-banner">
        <div class="overlay">
            <div class="container">
                <h1>Agent details</h1>
                <ol class="breadcrumb">
                    <li class="pull-left">Agent details</li>
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li class="active">Agent details</li>
                </ol>
            </div>
        </div>
    </div>

    <!--======= PROPERTIES DETAIL PAGE =========-->
    <section class="properti-detsil">
            <div class="row">
                <div class="container">
                    <div class="panel panel-default">
                        <div class="panel-heading">Agent details</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <img style=" padding-top: 15px;" class="center-img img-rounded" src="{{asset('uploads/agent/'.$agent->image)}}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h4>{{$agent->c_name}}</h4>
                                    <hr>
                                    <p> <b>Tel: {{$agent->phone}} </b></p>
                                    <div class="blog-text">
                                       {!! $agent->description !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <br>
    <!--======= PROPERTY =========-->
    <section class="properties white-bg">
        <div class="container">

            <!--======= TITTLE =========-->
            <div class="tittle"> <img src="{{asset('assets/frontend/images/head-top.png')}}" alt="">
                <h3>Properties from this agent</h3>
                <br>

            </div>

            <!--======= PROPERTIES ROW =========-->
            <ul class="row">
            @foreach($agent->specific_property as $property)
                <!--======= PROPERTY =========-->
                    <li class="col-sm-4">
                        <!--======= TAGS =========-->

                        <span class="tag font-montserrat @if($property->type=='1')sale @elseif($property->type=='0') rent @endif">
                        FOR @if($property->type=='1')SALE @elseif($property->type=='0') RENT @endif
                    </span>

                        <section>
                            <!--======= IMAGE =========-->
                            <div class="img"> <img class="" style="width: 300px;!important;height: 250px;!important;" src="{{asset('uploads/property/'.$property->image['0'])}}" alt="" >
                                <!--======= IMAGE HOVER =========-->

                                <div class="over-proper"> <a href="{{route('property.details',$property->id)}}" class="btn font-montserrat">more details</a> </div>
                            </div>
                            <!--======= HOME INNER DETAILS =========-->
                            <ul class="home-in">
                                <li><span><i class="fa fa-home"></i>{{$property->living}} Livingrooms</span></li>
                                <li><span><i class="fa fa-bed"></i> {{$property->bed}} Bedrooms</span></li>
                                <li><span><i class="fa fa-tty"></i> {{$property->bath}} Bathrooms</span></li>
                            </ul>
                            <!--======= HOME DETAILS =========-->
                            <div class="detail-sec">
                                <a href="{{route('property.details',$property->id)}}" class="font-montserrat">
                                    {{$property->name}}
                                </a> <span class="locate"><i class="fa fa-map-marker"></i> {{$property->address}}</span>
                                {{--                            <p>{!! Str::limit($property->description,'25','...') !!}</p>--}}
                                <div class="share-p"> <span class="price font-montserrat">{{env('APP_CURRENCY')}}{{number_format($property->price, 2, '.', ',')}}</span>

                                    <a style="cursor: pointer;" v-if="user!='0'" >
                                        <i v-if="checkCartItem({{$property->id}})" @click.prevent="exitInFavorite" class="fa fa-heart"></i>
                                        <i v-else class="fa fa-heart-o" @click.prevent="addToFavorite({{$property->id}})"></i>
                                    </a>
                                    <a style="cursor: pointer;" v-else href="{{route('login')}}"  > <i  class="fa fa-heart-o"></i></a>
                                </div>
                            </div>
                        </section>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>

@endsection

@push('js')

@endpush





