@component('mail::message')

<strong>Name</strong> {{$contact['name']}}
<strong>Email</strong> {{$contact['email']}}
<strong>Subject</strong> {{$contact['subject']}}

<strong>Message</strong>

{{$contact['message']}}

@endcomponent
