@extends('layouts.frontend.app')

@section('title','How it Works')

@push('css')
<style>
    .text-justify {

        color: #000 !important;
    }
    /*.img-wrap img {*/
        /*width: 100%;*/
        /*!* border-radius: 100%; *!*/
        /*border: 1px solid gray !important;*/
    /*}*/
</style>
@endpush

@section('content')
    <section class="how-it-work">
        <div class="container">
            <h1 class="text-center">How it works</h1>

            <div class="row hiwp">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2"><p class="nm text-center"><span class="goal align-text-bottom">1</span></p></div>
                        <div class="col-md-10">
                            <h4 class="text-left">Sign In/Up</h4>
                            <br>
                            <p class="text-justify">
                                If you want to make a bid on any property - you need to sign up your details with clicks2bid. Once you registered, you are then permitted to make bids on properties .
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="img-wrap" style="margin-left: 10%!important; margin-top: -5% !important;">
                        <img class="img-responsive" src="{{asset('assets/frontend/images/signin-1.png')}}">
                    </div>
                </div>
            </div>
            <div class="row hiwp">
                <div class="col-md-6">
                    <div class="img-wrap" style="margin-top: -20%!important; margin-left: -10%!important;">
                        <img style="margin-top: 20px;" class="img-responsive" src="{{asset('assets/frontend/images/search-3.png')}}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2"><p class="nm text-center"><span class="goal align-text-bottom">2</span></p></div>
                        <div class="col-md-10">
                            <h4 class="text-left">Search</h4>
                            <br>
                            <p class="text-justify">
                                We are simply offering a safe and secure platform that you can search properties as you like for buy or rent.

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hiwp">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2"><p class="nm text-center"><span class="goal align-text-bottom">3</span></p></div>
                        <div class="col-md-10">
                            <h4 class="text-left">Bid</h4>
                            <br>
                            <p class="text-justify">
                              If you want buy or rent a property then you have to bid on this property and for bid you must go to the property details page and you can see a bid button after click that button your bidding process will be complete.

                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="img-wrap" style="margin-top: -15% !important; margin-left: 5%!important;">
                        <img class="img-responsive" src="{{asset('assets/frontend/images/gravel-2.png')}}">
                    </div>
                </div>
            </div>
            <div class="row hiwp">
                <div class="col-md-6">
                    <div class="img-wrap" style="margin-top: -10%!important;margin-left: -10%!important;">
                        <img class="img-responsive" src="{{asset('assets/frontend/images/review-1.png')}}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2"><p class="nm text-center"><span class="goal align-text-bottom">4</span></p></div>
                        <div class="col-md-10">
                            <h4 class="text-left">Review</h4>
                            <br>
                            <p class="text-justify">
                               When you chose a property you can see the documents of this property as well.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row hiwp">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2"><p class="nm text-center"><span class="goal align-text-bottom">5</span></p></div>
                        <div class="col-md-10">
                            <h4 class="text-left">For Agent</h4>
                            <br>
                            <p class="text-justify">
                                If you want to publish any property on clicks2bid whether a rental or to sale - you need to sign up your details with clicks2bid. Once you approved, you are then permitted to publish properties on this platform .
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="img-wrap" style="margin-top: -8%!important; margin-left: 10%!important;">
                        <img class="img-responsive" src="{{asset('assets/frontend/images/agent_3.png')}}">
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

@push('js')

@endpush
