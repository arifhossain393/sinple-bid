@extends('layout.backendss.app')

@section('title','subscription')

@push('css')
    <style>
        .block-header h2 {

            color: #000 !important;

        }

        .card {
            margin-left: -16px !important;
        }
        h5, .h5 {
            font-size: 15px !important;
        }
        h5 {
            margin-left: -11px !important;
            margin-top: 0px !important;
        }

        .btn-info, .btn-info:hover, .btn-info:active, .btn-info:focus {
            background-color: #8bc34a !important;
        }
        .btn.btn-info {
            margin-left: 102px !important;
        }
        #agent {
            margin-left: -29% !important;
        }
        .col-lg-4 {
            width: 27.333% !important;
        }

        span{
            color: #000 !important;
            font-family: play!important;
            font-size: 14px !important;
        }
    </style>
@endpush
@section('content')

    <div class="block-header">
        <h2><b>AGENT DASHBOARD</b></h2>

    </div>

    <!-- Widgets -->
    <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-pink hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">playlist_add_check</i>
                </div>
                <div class="content">
                    <div class="text">Customers</div>
                    <div class="number count-to" data-from="0" data-to="0" data-speed="15" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-cyan hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">help</i>
                </div>
                <div class="content">
                    <div class="text">Properties</div>
                    <div class="number count-to" data-from="0" data-to="
                        {{\App\Property::where('agent_id',auth()->guard('agent')->user()->id)->count()}}
                        " data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-light-green hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">forum</i>
                </div>
                <div class="content">
                    <div class="text">Favourite</div>
                    <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="info-box bg-orange hover-expand-effect">
                <div class="icon">
                    <i class="material-icons">person_add</i>
                </div>
                <div class="content">
                    <div class="text">Bid</div>
                    <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="block-header">
        <h2><b>SUBSCRIPTION PACKAGE</b></h2>

    </div>
    <!-- #END# Widgets -->
    <!-- CPU Usage -->
    @foreach(\App\Subscription::where('status',1)->get() as $subscription)
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="card">
                <div class="header bg-cyan">

                    <h5>{{$subscription->name}}</h5>


                    <ul class="header-dropdown m-r--5">
                        <li>

                            <h5>£{{$subscription->price}}({{$subscription->days}}days)</h5>

                        </li>

                        <li class="dropdown">

                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);">Action</a></li>
                                <li><a href="javascript:void(0);">Another action</a></li>
                                <li><a href="javascript:void(0);">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    {!! Str::limit($subscription->description,'250','...') !!}
                </div>
                <div class="header bg-cyan">
                    <a href="{{route('order.paypal',$subscription->id)}}" class="btn btn-info">Purchase</a>
                </div>
            </div>
        </div>
    @endforeach

    <!--<div class="body">-->
    <!--    <div id="donut_chart" class="dashboard-donut-chart"></div>-->
    <!--</div>-->

    <div class="col-lg-12">
        <h5><b>NB:You will have purchase a package to continue.</b></h5>
    </div>



@endsection

@push('js')
    <script src="{{asset('assets/backend/js/pages/cards/colored.js')}}"></script>
@endpush
