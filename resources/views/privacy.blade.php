@extends('layouts.frontend.app')

@section('title','Privacy')

@push('css')

@endpush

@section('content')
    <!-- Sub banner start -->
    <div class="sub-banner">
        <div class="container breadcrumb-area">
            <div class="breadcrumb-areas">
                <h1>Privacy Policy</h1>
                <ul class="breadcrumbs">
                    <li><a href="{{route('index')}}">Home</a></li>
                    <li class="active">privacy-policy</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Sub Banner end -->
    <!-- Faq start -->
    <div class="faq content-area-9">
        <div class="container">
            <!-- Main title -->
            <div class="main-title text-center">
                <h1>Privacy Policy</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
            </div>
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div id="faq" class="faq-accordion">
                        <div class="card m-b-0">
                            <div class="card-header">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse1">
                                     How long does the GMS-Estate auction last ?
                                </a>
                            </div>
                            <div id="collapse1" class="card-block collapse">
                                <div class="p-text">
                                    Our estate agents control the duration of each listing. While most properties are live for 7-14 days, the agent may accept a bid on day 1 and remove the property from the site. Make sure you don’t miss out on any properties by downloading our mobile app to receive notifications and updates.
                                </div>
                            </div>
                            <div class="card-header">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse2">
                                    How long does the GMS-Estate auction last ?
                                </a>
                            </div>
                            <div id="collapse2" class="card-block collapse">
                                <div class="p-text">
                                     Our estate agents control the duration of each listing. While most properties are live for 7-14 days, the agent may accept a bid on day 1 and remove the property from the site. Make sure you don’t miss out on any properties by downloading our mobile app to receive notifications and updates.
                                </div>
                            </div>
                            <div class="card-header">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse3">
                                    How long does the GMS-Estate auction last ?
                                </a>
                            </div>
                            <div id="collapse3" class="card-block collapse">
                                <div class="p-text">
                                    Our estate agents control the duration of each listing. While most properties are live for 7-14 days, the agent may accept a bid on day 1 and remove the property from the site. Make sure you don’t miss out on any properties by downloading our mobile app to receive notifications and updates.
                                </div>
                            </div>
                            <div class="card-header bd-none">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse4">
                                    How long does the GMS-Estate auction last ?
                                </a>
                            </div>
                            <div id="collapse4" class="card-block collapse">
                                <div class="p-text">
                                    Our estate agents control the duration of each listing. While most properties are live for 7-14 days, the agent may accept a bid on day 1 and remove the property from the site. Make sure you don’t miss out on any properties by downloading our mobile app to receive notifications and updates.
                                </div>
                            </div>
                            <div class="card-header">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse5">
                                    How long does the GMS-Estate auction last ?
                                </a>
                            </div>
                            <div id="collapse5" class="card-block collapse">
                                <div class="p-text">
                                   Our estate agents control the duration of each listing. While most properties are live for 7-14 days, the agent may accept a bid on day 1 and remove the property from the site. Make sure you don’t miss out on any properties by downloading our mobile app to receive notifications and updates.
                                </div>
                            </div>
                            <div class="card-header mb-0">
                                <a class="card-title collapsed" data-toggle="collapse" data-parent="#faq" href="#collapse6">
                                    How long does the GMS-Estate auction last ?
                                </a>
                            </div>
                            <div id="collapse6" class="card-block collapse">
                                <div class="p-text">
                                    Our estate agents control the duration of each listing. While most properties are live for 7-14 days, the agent may accept a bid on day 1 and remove the property from the site. Make sure you don’t miss out on any properties by downloading our mobile app to receive notifications and updates.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Faq end -->
@endsection

@push('js')

@endpush
