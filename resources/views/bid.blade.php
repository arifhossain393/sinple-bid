@extends('layouts.frontend.app')

@section('title','Bid')

@push('css')
<style>
    .search-area-5 {
        transition: all 0.4s;
        position: relative;
        bottom: 0;
        width: 100%;
        z-index: 1;
        padding: 30px 0 0;
        background: #ecebea;
    }
</style>
@endpush

@section('content')
    <!-- Sub banner start -->
    <div class="sub-banner">
        <div class="container breadcrumb-area">
            <div class="breadcrumb-areas">
                <h1>Bid Property</h1>
                <ul class="breadcrumbs">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li class="active">Bid Property</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Sub Banner end -->

    <!-- Search area 3 start -->
    <div class="search-area-5 none-992">
        <div class="container">
            <div class="inline-search-area">
                <form action="{{route('search-property')}}" method="GET">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6 form-group">
                            <select class="selectpicker search-fields" id="inlineFormCustomSelect" name="type">
                                <option value="1" selected>FOR SALE</option>
                                <option value="0">FOR RENT</option>
                            </select>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                            <input id="searchTextField" type="text" placeholder="Enter Post code" autocomplete="on"
                        runat="server" class="form-control input-search">
                            <input type="hidden" id="address" name="address" />
                            <input type="hidden" id="latitude" name="latitude" />
                            <input type="hidden" id="longitude" name="longitude" />
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6 form-group">
                            <button type="submit" class="btn button-theme btn-search btn-block search_btn">
                                <i class="fa fa-search"></i><strong>Find</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Search area 3 end -->

    <!-- Our team start -->
    <div class="our-team content-area-3">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1>Our Agent</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="team-1">
                        <div class="team-photo">
                            <a href="#">
                                <img src="{{asset('assets/newfrontend/img/avatar/avatar-7.jpg') }}" alt="agent" class="img-fluid">
                            </a>
                            <ul class="social-list clearfix">
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-details">
                            <h5><a href="agent-detail.html">Martin Smith</a></h5>
                            <h6>Web Developer</h6>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="team-1">
                        <div class="team-photo">
                            <a href="#">
                                <img src="{{asset('assets/newfrontend/img/avatar/avatar-6.jpg') }}" alt="agent" class="img-fluid">
                            </a>
                            <ul class="social-list clearfix">
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-details">
                            <h5><a href="agent-detail.html">Carolyn Stone</a></h5>
                            <h6>Creative Director</h6>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="team-1">
                        <div class="team-photo">
                            <a href="#">
                                <img src="{{asset('assets/newfrontend/img/avatar/avatar-8.jpg') }}" alt="agent" class="img-fluid">
                            </a>
                            <ul class="social-list clearfix">
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-details">
                            <h5><a href="agent-detail.html">Brandon Miller</a></h5>
                            <h6>Manager</h6>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="team-1">
                        <div class="team-photo">
                            <a href="#">
                                <img src="{{asset('assets/newfrontend/img/avatar/avatar-5.jpg') }}" alt="agent" class="img-fluid">
                            </a>
                            <ul class="social-list clearfix">
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-details">
                            <h5><a href="agent-detail.html">Michelle Nelson</a></h5>
                            <h6>Support Manager</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Our team end -->
@endsection

@push('js')

    <script type="text/javascript">

        var options = {
            componentRestrictions: {country: "uk"}
        };
        function initialize() {
            var input = document.getElementById('searchTextField');
            var autocomplete = new google.maps.places.Autocomplete(input,options);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                document.getElementById('address').value = place.name;
                document.getElementById('latitude').value = place.geometry.location.lat();
                document.getElementById('longitude').value = place.geometry.location.lng();
                //alert("This function is working!");
                //alert(place.name);
                // alert(place.address_components[0].long_name);
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize)
    </script>
@endpush
