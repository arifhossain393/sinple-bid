@extends('layouts.frontend.app')

@section('title','Home')

@push('css')
<style>

</style>
@endpush

@section('content')
    <!-- Banner start -->
    <div class="banner" id="banner">
        <div id="bannerCarousole" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner banner-slider-inner text-left">
                <div class="carousel-item banner-max-height active">
                    <img class="d-block w-100 h-100" src="{{asset('assets/newfrontend/img/banner/banner-1.jpg') }}" alt="banner">
                </div>
                <div class="carousel-item banner-max-height">
                    <img class="d-block w-100 h-100" src="{{asset('assets/newfrontend/img/banner/banner-3.jpg') }}" alt="banner">
                </div>
                <div class="carousel-item banner-max-height">
                    <img class="d-block w-100 h-100" src="{{asset('assets/newfrontend/img/banner/banner-2.jpg') }}" alt="banner">
                </div>
                <div class="carousel-content container banner-info-2 bi-3 text-center">
                    <h3>Find Your Apartments</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
                    <a href="index.html" class="btn btn-white btn-read-more">Read More</a>
                </div>
            </div>
            <!-- Search area 3 start -->
            <div class="search-area-5 none-992">
                <div class="container">
                    <div class="inline-search-area">
                        <form action="{{route('search-property')}}" method="GET">
                            <div class="row">
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6 form-group">
                                    <select class="selectpicker search-fields" id="inlineFormCustomSelect" name="type">
                                        <option value="1" selected>FOR SALE</option>
                                        <option value="0">FOR RENT</option>
                                    </select>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                    <input id="searchTextField" type="text" placeholder="Enter Post code" autocomplete="on"
                                runat="server" class="form-control input-search">
                                    <input type="hidden" id="address" name="address" />
                                    <input type="hidden" id="latitude" name="latitude" />
                                    <input type="hidden" id="longitude" name="longitude" />
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6 form-group">
                                    <button type="submit" class="btn button-theme btn-search btn-block search_btn">
                                        <i class="fa fa-search"></i><strong>Find</strong>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Search area 3 end -->
        </div>
    </div>
    <!-- Banner end -->

    <!-- Search area 3 start -->
    <div class="search-area-3 clearfix d-lg-none d-xl-none">
        <div class="container">
            <div class="inline-search-area">
                <form action="{{route('search-property')}}" method="GET">
                    <div class="row">
                        <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6 search-col">
                            <select class="selectpicker search-fields" id="inlineFormCustomSelect" name="type">
                                <option value="1" selected>FOR SALE</option>
                                <option value="0">FOR RENT</option>
                            </select>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-6 search-col">
                            <input id="searchTextField" type="text" placeholder="Enter Post code" autocomplete="on"
                                runat="server" class="form-control input-search">
                            <input type="hidden" id="address" name="address" />
                            <input type="hidden" id="latitude" name="latitude" />
                            <input type="hidden" id="longitude" name="longitude" />
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-12 search-col">
                            <button type="submit" class="btn button-theme btn-search bs-2 btn-block search_btn">
                                <i class="fa fa-search"></i><strong>Find</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Search area 3 end -->

    <!-- Featured Properties start -->
    <div class="featured-properties content-area-13">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1>Featured Properties</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
            </div>
            <div class="row">
                 @foreach($properties as $property)
                    <div class="col-lg-4 col-md-6">
                        <div class="property-box">
                            <div class="property-thumbnail">
                                <a href="{{route('property.details',$property->id)}}" class="property-img">
                                    <div class="tag @if($property->type=='1')sale @elseif($property->type=='0') rent @endif">
                                        FOR @if($property->type=='1')SALE @elseif($property->type=='0') RENT @endif
                                    </div>
                                    <div class="price-box"><span>{{env('APP_CURRENCY')}}{{number_format($property->price, 2, '.', ',')}}</span> month</div>
                                    <img class="d-block w-100" src="{{asset('uploads/property/'.$property->image['0'])}}" alt="properties">
                                </a>
                            </div>
                            <div class="detail">
                                <h1 class="title">
                                    <a href="{{route('property.details',$property->id)}}">{{Str::limit($property->name,'30','...')}}</a>
                                </h1>
                                <div class="location">
                                    <a href="{{route('property.details',$property->id)}}">
                                        <i class="flaticon-pin"></i>{{Str::limit($property->address,'40','...')}}
                                    </a>
                                </div>
                                <ul class="facilities-list clearfix">
                                    <li>
                                        <i class="flaticon-bed"></i> {{$property->bed}} Beds
                                    </li>
                                    <li>
                                        <i class="flaticon-bathroom"></i> {{$property->bath}} Baths
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Featured Properties end -->

    <!-- advantages start -->
    <div class="advantages content-area">
        <div class="container">
            <!-- Main title -->
            <div class="main-title-2 text-center">
                <h1>Our Advantages</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="advantages-box">
                        <div class="icon">
                            <i class="flaticon-pin"></i>
                        </div>
                        <div class="detail">
                            <h5>Various Locations</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="advantages-box">
                        <div class="icon">
                            <i class="flaticon-camera"></i>
                        </div>
                        <div class="detail">
                            <h5>View Apartments</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="advantages-box">
                        <div class="icon">
                            <i class="flaticon-password"></i>
                        </div>
                        <div class="detail">
                            <h5>Privacy and Security</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="advantages-box">
                        <div class="icon">
                            <i class="flaticon-sale"></i>
                        </div>
                        <div class="detail">
                            <h5>No Commission</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus tincidunt.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- advantages end -->

    <!-- Categories strat -->
    <div class="categories content-area-13">
        <div class="container">
            <!-- Main title -->
            <div class="main-title text-center">
                <h1>Our Services</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
            </div>
            <div class="row row-2">
                <div class="col-lg-4 col-md-12">
                    <div class="row">
                        <div class="col-md-6 col-lg-12 col-pad">
                            <div class="category">
                                <div class="category_bg_box cat-1-bg">
                                    <div class="category-overlay">
                                        <div class="category-content">
                                            <h3 class="category-title">
                                                <a href="#">Buy Property</a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-12 col-pad">
                            <div class="category">
                                <div class="category_bg_box cat-3-bg">
                                    <div class="category-overlay">
                                        <div class="category-content">
                                            <h3 class="category-title">
                                                <a href="#">Sell Property</a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-pad">
                    <div class="category">
                        <div class="category_bg_box category_long_bg cat-4-bg">
                            <div class="category-overlay">
                                <div class="category-content">
                                    <h3 class="category-title">
                                        <a href="#">Other Services</a>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="row">
                        <div class="col-md-6 col-lg-12 col-pad">
                            <div class="category">
                                <div class="category_bg_box cat-2-bg">
                                    <div class="category-overlay">
                                        <div class="category-content">
                                            <h3 class="category-title">
                                                <a href="#">Rent Property</a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-12 col-pad">
                            <div class="category">
                                <div class="category_bg_box cat-5-bg">
                                    <div class="category-overlay">
                                        <div class="category-content">
                                            <h3 class="category-title">
                                                <a href="properties-details.html">Agent Bidding System</a>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Categories end -->

    <!-- Our team start -->
    <div class="our-team content-area-3">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1>Our Agent</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="team-1">
                        <div class="team-photo">
                            <a href="#">
                                <img src="{{asset('assets/newfrontend/img/avatar/avatar-7.jpg') }}" alt="agent" class="img-fluid">
                            </a>
                            <ul class="social-list clearfix">
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-details">
                            <h5><a href="agent-detail.html">Martin Smith</a></h5>
                            <h6>Web Developer</h6>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="team-1">
                        <div class="team-photo">
                            <a href="#">
                                <img src="{{asset('assets/newfrontend/img/avatar/avatar-6.jpg') }}" alt="agent" class="img-fluid">
                            </a>
                            <ul class="social-list clearfix">
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-details">
                            <h5><a href="agent-detail.html">Carolyn Stone</a></h5>
                            <h6>Creative Director</h6>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="team-1">
                        <div class="team-photo">
                            <a href="#">
                                <img src="{{asset('assets/newfrontend/img/avatar/avatar-8.jpg') }}" alt="agent" class="img-fluid">
                            </a>
                            <ul class="social-list clearfix">
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-details">
                            <h5><a href="agent-detail.html">Brandon Miller</a></h5>
                            <h6>Manager</h6>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="team-1">
                        <div class="team-photo">
                            <a href="#">
                                <img src="{{asset('assets/newfrontend/img/avatar/avatar-5.jpg') }}" alt="agent" class="img-fluid">
                            </a>
                            <ul class="social-list clearfix">
                                <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                        <div class="team-details">
                            <h5><a href="agent-detail.html">Michelle Nelson</a></h5>
                            <h6>Support Manager</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Our team end -->

    <!-- Testimonial start -->
    <div class="testimonial bg-grea-3 content-area-5">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1>Customer Reviews</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
            </div>
            <!-- Slick slider area start -->
            <div class="slick-slider-area">
                <div class="row slick-carousel"
                    data-slick='{"slidesToShow": 2, "responsive":[{"breakpoint": 1024,"settings":{"slidesToShow": 2}}, {"breakpoint": 768,"settings":{"slidesToShow": 1}}]}'>
                    @foreach($review as $rev)
                        <div class="slick-slide-item">
                            <div class="testimonial-inner">
                                <div class="text-box">
                                    <p>{!! $rev['review'] !!}</p>
                                    {{-- <div class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                        <span>( 4 Reviews )</span>
                                    </div> --}}
                                </div>
                                <div class="user">
                                    <div class="photo">
                                        <img class="media-object" src="{{asset('assets/newfrontend/img/avatar/avatar-1.jpg') }}" alt="user">
                                    </div>
                                    <div class="detail">
                                        <h5>{{$rev->name}}</h5>
                                        <p>UK</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="slick-prev slick-arrow-buton">
                    <i class="fa fa-angle-left"></i>
                </div>
                <div class="slick-next slick-arrow-buton">
                    <i class="fa fa-angle-right"></i>
                </div>
            </div>
        </div>
    </div>
    <!-- Testimonial end -->
@endsection

@push('js')
    {{--<script type="text/javascript">--}}

        {{--function initialize() {--}}
            {{--var input = document.getElementById('searchTextField');--}}
            {{--var autocomplete = new google.maps.places.Autocomplete(input);--}}
            {{--google.maps.event.addListener(autocomplete, 'place_changed', function () {--}}
                {{--var place = autocomplete.getPlace();--}}
                {{--document.getElementById('address').value = place.name;--}}
                {{--document.getElementById('latitude').value = place.geometry.location.lat();--}}
                {{--document.getElementById('longitude').value = place.geometry.location.lng();--}}
                {{--//alert("This function is working!");--}}
                {{--//alert(place.name);--}}
                {{--// alert(place.address_components[0].long_name);--}}
            {{--});--}}
        {{--}--}}
        {{--google.maps.event.addDomListener(window, 'load', initialize)--}}
    {{--</script>--}}
    <script type="text/javascript">

        var options = {
            componentRestrictions: {country: "uk"}
        };
        function initialize() {
            var input = document.getElementById('searchTextField');
            var autocomplete = new google.maps.places.Autocomplete(input,options);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                document.getElementById('address').value = place.name;
                document.getElementById('latitude').value = place.geometry.location.lat();
                document.getElementById('longitude').value = place.geometry.location.lng();
                //alert("This function is working!");
                //alert(place.name);
                // alert(place.address_components[0].long_name);
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize)
    </script>
    <script>
        $('.carousel').carousel({
            interval: 4000
        })
    </script>
@endpush
