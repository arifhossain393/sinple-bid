@extends('layouts.frontend.app')

@section('title','Rent-property-list')

@push('css')

@endpush

@section('content')
    <div class="sub-banner">
        <div class="overlay">
            <div class="container">
                <h1>properties for rent</h1>
                <ol class="breadcrumb">
                    <li class="pull-left">properties for rent</li>
                    <li><a href="#">Home</a></li>
                    <li class="active">properties for rent</li>
                </ol>
            </div>
        </div>
    </div>

    <!--======= PROPERTY =========-->
    <section class="properties white-bg">
        <div class="container">

            <!--======= TITTLE =========-->

            <!--======= PROPERTIES DETAIL PAGE =========-->
            <section class="properti-detsil">
                <div class="container">
                    <div class="row">

                        <!--======= LEFT BAR =========-->
                        <div class="col-sm-9">




                            <!--======= PROPERTY FEATURES =========-->
                            <section class="info-property location">
                                <h5 class="tittle-head">property location</h5>
                                <div class="inner"> </div>
                                <div class="mapsection">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2463.204519109898!2d-0.40043838478363625!3d51.87547919154534!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876483daf59747d%3A0x9344ccdf09edb7a5!2sRedrow%20-%20Saxon%20Square%2C%20Luton!5e0!3m2!1sen!2sbd!4v1568809184216!5m2!1sen!2sbd" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                                </div>

                            </section>
                        </div>

                        <!--======= RIGT SIDEBAR =========-->
                        <div class="col-sm-3 side-bar">

                            <!--======= FIND PROPERTY =========-->
                            <div class="finder">

                                <!--======= FORM SECTION =========-->
                                <div class="find-sec">
                                    <h5>Search for properties</h5>
                                    <ul class="row">

                                        <!--======= FORM =========-->
                                        <li class="col-sm-12">
                                            <select class="selectpicker">
                                                <option value="">City</option>
                                                <option value="">Cat 1</option>
                                                <option value="">Cat 2</option>
                                            </select>
                                        </li>

                                        <!--======= FORM =========-->
                                        <li class="col-sm-12">
                                            <select class="selectpicker">
                                                <option value="">Location</option>
                                                <option value="">Cat 1</option>
                                                <option value="">Cat 2</option>
                                            </select>
                                        </li>

                                        <!--======= FORM =========-->
                                        <!--    <li class="col-sm-12">
                                             <select class="selectpicker">
                                               <option value="">Property Status</option>
                                               <option value="">Cat 1</option>
                                               <option value="">Cat 2</option>
                                             </select>
                                           </li> -->

                                        <!--======= FORM =========-->
                                        <!--  <li class="col-sm-12">
                                           <select class="selectpicker">
                                             <option value="">Property Type</option>
                                             <option value="">Cat 1</option>
                                             <option value="">Cat 2</option>
                                           </select>
                                         </li> -->

                                        <!--======= FORM =========-->
                                        <li class="col-sm-12">
                                            <select class="selectpicker">
                                                <option value="">Minimum Bid</option>
                                                <option value="">Cat 1</option>
                                                <option value="">Cat 2</option>
                                            </select>
                                        </li>

                                        <!--======= FORM =========-->
                                        <li class="col-sm-12">
                                            <select class="selectpicker">
                                                <option value="">Maximum Bid</option>
                                                <option value="">Cat 1</option>
                                                <option value="">Cat 2</option>
                                            </select>
                                        </li>
                                        <li class="col-sm-12">
                                            <select class="selectpicker">
                                                <option value="">Minimum price</option>
                                                <option value="">Cat 1</option>
                                                <option value="">Cat 2</option>
                                            </select>
                                        </li>
                                        <li class="col-sm-12">
                                            <select class="selectpicker">
                                                <option value="">Maximum price</option>
                                                <option value="">Cat 1</option>
                                                <option value="">Cat 2</option>
                                            </select>
                                        </li>
                                        <li class="col-sm-12"> <a href="#." class="btn search_btn">SEARCH</a> </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <!--======= PROPERTIES ROW =========-->
            <ul class="row">

                <!--======= PROPERTY =========-->
                <li class="col-sm-4">
                    <!--======= TAGS =========-->
                    <span class="tag font-montserrat rent">FOR RENT</span>
                    <section>
                        <!--======= IMAGE =========-->
                        <div class="img"> <img class="img-responsive" src="{{asset('assets/frontend/images/img-1.jpg')}}" alt="" >
                            <!--======= IMAGE HOVER =========-->

                            <div class="over-proper"> <a href="property-Details.html" class="btn font-montserrat">more details</a> </div>
                        </div>
                        <!--======= HOME INNER DETAILS =========-->
                        <ul class="home-in">
                            <li><span><i class="fa fa-home"></i> 20,000 Acres</span></li>
                            <li><span><i class="fa fa-bed"></i> 3 Bedrooms</span></li>
                            <li><span><i class="fa fa-tty"></i> 3 Bathrooms</span></li>
                        </ul>
                        <!--======= HOME DETAILS =========-->
                        <div class="detail-sec"> <a href="property-Details.html" class="font-montserrat">sweet home for small family</a> <span class="locate"><i class="fa fa-map-marker"></i> Luton,UK</span>
                            <p>Till the one day when the lady met this fellow and they knew it was much more than </p>
                            <div class="share-p"> <span class="price font-montserrat">£ 256,596</span> <i class="fa fa-star-o"></i> <i class="fa fa-heart"></i> </div>
                        </div>
                    </section>
                </li>

                <!--======= PROPERTY =========-->
                <li class="col-sm-4">
                    <!--======= TAGS =========-->
                    <span class="tag font-montserrat rent">FOR RENT</span>
                    <section>
                        <!--======= IMAGE =========-->
                        <div class="img"> <img class="img-responsive" src="{{asset('assets/frontend/images/img-2.jpg')}}" alt="" >
                            <!--======= IMAGE HOVER =========-->

                            <div class="over-proper"> <a href="property-Details.html" class="btn font-montserrat">more details</a> </div>
                        </div>
                        <!--======= HOME INNER DETAILS =========-->
                        <ul class="home-in">
                            <li><span><i class="fa fa-home"></i> 20,000 Acres</span></li>
                            <li><span><i class="fa fa-bed"></i> 3 Bedrooms</span></li>
                            <li><span><i class="fa fa-tty"></i> 3 Bathrooms</span></li>
                        </ul>
                        <!--======= HOME DETAILS =========-->
                        <div class="detail-sec"> <a href="property-Details.html" class="font-montserrat">sweet home for small family</a> <span class="locate"><i class="fa fa-map-marker"></i> Luton,UK</span>
                            <p>Till the one day when the lady met this fellow and they knew it was much more than </p>
                            <div class="share-p"> <span class="price font-montserrat">£ 256,596</span> <i class="fa fa-star-o"></i> <i class="fa fa-heart"></i> </div>
                        </div>
                    </section>
                </li>

                <!--======= PROPERTY =========-->
                <li class="col-sm-4">
                    <!--======= TAGS =========-->
                    <span class="tag font-montserrat sale">FOR RENT</span>
                    <section>
                        <!--======= IMAGE =========-->
                        <div class="img"> <img class="img-responsive" src="{{asset('assets/frontend/images/img-3.jpg')}}" alt="" >
                            <!--======= IMAGE HOVER =========-->

                            <div class="over-proper"> <a href="property-Details.html" class="btn font-montserrat">more details</a> </div>
                        </div>
                        <!--======= HOME INNER DETAILS =========-->
                        <ul class="home-in">
                            <li><span><i class="fa fa-home"></i> 20,000 Acres</span></li>
                            <li><span><i class="fa fa-bed"></i> 3 Bedrooms</span></li>
                            <li><span><i class="fa fa-tty"></i> 3 Bathrooms</span></li>
                        </ul>
                        <!--======= HOME DETAILS =========-->
                        <div class="detail-sec"> <a href="property-Details.html" class="font-montserrat">sweet home for small family</a> <span class="locate"><i class="fa fa-map-marker"></i> Luton,UK</span>
                            <p>Till the one day when the lady met this fellow and they knew it was much more than </p>
                            <div class="share-p"> <span class="price font-montserrat">£ 256,596</span> <i class="fa fa-star-o"></i> <i class="fa fa-heart"></i> </div>
                        </div>
                    </section>
                </li>

                <!--======= PROPERTY =========-->
                <li class="col-sm-4">
                    <!--======= TAGS =========-->
                    <span class="tag font-montserrat rent">FOR RENT</span>
                    <section>
                        <!--======= IMAGE =========-->
                        <div class="img"> <img class="img-responsive" src="{{asset('assets/frontend/images/img-4.jpg')}}" alt="" >
                            <!--======= IMAGE HOVER =========-->

                            <div class="over-proper"> <a href="property-Details.html" class="btn font-montserrat">more details</a> </div>
                        </div>
                        <!--======= HOME INNER DETAILS =========-->
                        <ul class="home-in">
                            <li><span><i class="fa fa-home"></i> 20,000 Acres</span></li>
                            <li><span><i class="fa fa-bed"></i> 3 Bedrooms</span></li>
                            <li><span><i class="fa fa-tty"></i> 3 Bathrooms</span></li>
                        </ul>
                        <!--======= HOME DETAILS =========-->
                        <div class="detail-sec"> <a href="property-Details.html" class="font-montserrat">sweet home for small family</a> <span class="locate"><i class="fa fa-map-marker"></i> Luton,UK</span>
                            <p>Till the one day when the lady met this fellow and they knew it was much more than </p>
                            <div class="share-p"> <span class="price font-montserrat">£ 2,956,596</span> <i class="fa fa-star-o"></i> <i class="fa fa-heart"></i> </div>
                        </div>
                    </section>
                </li>

                <!--======= PROPERTY =========-->
                <li class="col-sm-4">
                    <!--======= TAGS =========-->
                    <span class="tag font-montserrat sale">FOR RENT</span>
                    <section>
                        <!--======= IMAGE =========-->
                        <div class="img"> <img class="img-responsive" src="{{asset('assets/frontend/images/img-5.jpg')}}" alt="" >
                            <!--======= IMAGE HOVER =========-->

                            <div class="over-proper"> <a href="property-Details.html" class="btn font-montserrat">more details</a> </div>
                        </div>
                        <!--======= HOME INNER DETAILS =========-->
                        <ul class="home-in">
                            <li><span><i class="fa fa-home"></i> 20,000 Acres</span></li>
                            <li><span><i class="fa fa-bed"></i> 3 Bedrooms</span></li>
                            <li><span><i class="fa fa-tty"></i> 3 Bathrooms</span></li>
                        </ul>
                        <!--======= HOME DETAILS =========-->
                        <div class="detail-sec"> <a href="property-Details.html" class="font-montserrat">sweet home for small family</a> <span class="locate"><i class="fa fa-map-marker"></i> Luton,UK</span>
                            <p>Till the one day when the lady met this fellow and they knew it was much more than </p>
                            <div class="share-p"> <span class="price font-montserrat">£ 256,596</span> <i class="fa fa-star-o"></i> <i class="fa fa-heart"></i> </div>
                        </div>
                    </section>
                </li>

                <!--======= PROPERTY =========-->
                <li class="col-sm-4">
                    <!--======= TAGS =========-->
                    <span class="tag font-montserrat rent">FOR RENT</span>
                    <section>
                        <!--======= IMAGE =========-->
                        <div class="img"> <img class="img-responsive" src="{{asset('assets/frontend/images/img-6.jpg')}}" alt="" >
                            <!--======= IMAGE HOVER =========-->

                            <div class="over-proper"> <a href="property-Details.html" class="btn font-montserrat">more details</a> </div>
                        </div>
                        <!--======= HOME INNER DETAILS =========-->
                        <ul class="home-in">
                            <li><span><i class="fa fa-home"></i> 20,000 Acres</span></li>
                            <li><span><i class="fa fa-bed"></i> 3 Bedrooms</span></li>
                            <li><span><i class="fa fa-tty"></i> 3 Bathrooms</span></li>
                        </ul>
                        <!--======= HOME DETAILS =========-->
                        <div class="detail-sec"> <a href="property-Details.html" class="font-montserrat">sweet home for small family</a> <span class="locate"><i class="fa fa-map-marker"></i> Luton,UK</span>
                            <p>Till the one day when the lady met this fellow and they knew it was much more than </p>
                            <div class="share-p"> <span class="price font-montserrat">£ 2,956,596</span> <i class="fa fa-star-o"></i> <i class="fa fa-heart"></i> </div>
                        </div>
                    </section>
                </li>

                <!--======= PROPERTY =========-->
                <li class="col-sm-4">
                    <!--======= TAGS =========-->
                    <span class="tag font-montserrat rent">FOR RENT</span>
                    <section>
                        <!--======= IMAGE =========-->
                        <div class="img"> <img class="img-responsive" src="{{asset('assets/frontend/images/img-2.jpg')}}" alt="" >
                            <!--======= IMAGE HOVER =========-->

                            <div class="over-proper"> <a href="property-Details.html" class="btn font-montserrat">more details</a> </div>
                        </div>
                        <!--======= HOME INNER DETAILS =========-->
                        <ul class="home-in">
                            <li><span><i class="fa fa-home"></i> 20,000 Acres</span></li>
                            <li><span><i class="fa fa-bed"></i> 3 Bedrooms</span></li>
                            <li><span><i class="fa fa-tty"></i> 3 Bathrooms</span></li>
                        </ul>
                        <!--======= HOME DETAILS =========-->
                        <div class="detail-sec"> <a href="property-Details.html" class="font-montserrat">sweet home for small family</a> <span class="locate"><i class="fa fa-map-marker"></i> Luton,UK</span>
                            <p>Till the one day when the lady met this fellow and they knew it was much more than </p>
                            <div class="share-p"> <span class="price font-montserrat">£ 256,596</span> <i class="fa fa-star-o"></i> <i class="fa fa-heart"></i> </div>
                        </div>
                    </section>
                </li>

                <!--======= PROPERTY =========-->
                <li class="col-sm-4">
                    <!--======= TAGS =========-->
                    <span class="tag font-montserrat sale">FOR RENT</span>
                    <section>
                        <!--======= IMAGE =========-->
                        <div class="img"> <img class="img-responsive" src="{{asset('assets/frontend/images/img-3.jpg')}}" alt="" >
                            <!--======= IMAGE HOVER =========-->

                            <div class="over-proper"> <a href="property-Details.html" class="btn font-montserrat">more details</a> </div>
                        </div>
                        <!--======= HOME INNER DETAILS =========-->
                        <ul class="home-in">
                            <li><span><i class="fa fa-home"></i> 20,000 Acres</span></li>
                            <li><span><i class="fa fa-bed"></i> 3 Bedrooms</span></li>
                            <li><span><i class="fa fa-tty"></i> 3 Bathrooms</span></li>
                        </ul>
                        <!--======= HOME DETAILS =========-->
                        <div class="detail-sec"> <a href="property-Details.html" class="font-montserrat">sweet home for small family</a> <span class="locate"><i class="fa fa-map-marker"></i> Luton,UK</span>
                            <p>Till the one day when the lady met this fellow and they knew it was much more than </p>
                            <div class="share-p"> <span class="price font-montserrat">£ 256,596</span> <i class="fa fa-star-o"></i> <i class="fa fa-heart"></i> </div>
                        </div>
                    </section>
                </li>

                <!--======= PROPERTY =========-->
                <li class="col-sm-4">
                    <!--======= TAGS =========-->
                    <span class="tag font-montserrat rent">FOR RENT</span>
                    <section>
                        <!--======= IMAGE =========-->
                        <div class="img"> <img class="img-responsive" src="{{asset('assets/frontend/images/img-4.jpg')}}" alt="" >
                            <!--======= IMAGE HOVER =========-->

                            <div class="over-proper"> <a href="property-Details.html" class="btn font-montserrat">more details</a> </div>
                        </div>
                        <!--======= HOME INNER DETAILS =========-->
                        <ul class="home-in">
                            <li><span><i class="fa fa-home"></i> 20,000 Acres</span></li>
                            <li><span><i class="fa fa-bed"></i> 3 Bedrooms</span></li>
                            <li><span><i class="fa fa-tty"></i> 3 Bathrooms</span></li>
                        </ul>
                        <!--======= HOME DETAILS =========-->
                        <div class="detail-sec"> <a href="#." class="font-montserrat">sweet home for small family</a> <span class="locate"><i class="fa fa-map-marker"></i> Luton,UK</span>
                            <p>Till the one day when the lady met this fellow and they knew it was much more than </p>
                            <div class="share-p"> <span class="price font-montserrat">£ 2,956,596</span> <i class="fa fa-star-o"></i> <i class="fa fa-heart"></i> </div>
                        </div>
                    </section>
                </li>
            </ul>
            <nav>
                <ul class="pagination">
                    <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                    <li><a href="#">2 </a></li>
                    <li><a href="#">3 </a></li>
                    <li><a href="#">4 </a></li>
                    <li><a href="#"><i class="fa fa-angle-double-right"></i> </a></li>
                </ul>
            </nav>
        </div>
    </section>
@endsection

@push('js')

@endpush

