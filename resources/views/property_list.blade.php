@extends('layouts.frontend.app')

@section('title','Property-list')

@push('css')
    <style>
        .propert-location-section {
            padding: 35px 0 0;
        }
    </style>
@endpush

@section('content')
    <!-- Sub banner start -->
    <div class="sub-banner">
        <div class="container breadcrumb-area">
            <div class="breadcrumb-areas">
                <h1>Properties</h1>
                <ul class="breadcrumbs">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    <li class="active">Properties</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Sub Banner end -->
    <!-- property location section start -->
    <div class="propert-location-section content-area">
        <div class="container">
            <div class="row">
                {{-- geo map --}}
                <div class="col-lg-8 col-md-12 col-xs-12">
                     <!--======= PROPERTY FEATURES =========-->
                    <section class="info-property location">
                        <h3 class="sidebar-title">Property location</h3>
                        <div class="inner"> </div>
                        <div class="mapsection">
                            <div id="map_canvas" style="width: 100%;height: 453px">
                            </div>
                            <br />
                        </div>
                    </section>
                </div>
                {{-- geo search --}}
                 <div class="col-lg-4 col-md-12">
                    <div class="sidebar-right">
                        <!-- Advanced search start -->
                        <div class="widget advanced-search d-none d-xl-block d-lg-block">
                            <h3 class="sidebar-title">Search Properties</h3>
                            <form action="{{route('search.all.property')}}" method="GET">
                                <div class="form-group">
                                    <input type="text" class="selectpicker form-control search-fields" id="searchTextField" placeholder="Enter Your Post Code" autocomplete="off" runat="server">
                                    <input type="hidden" id="address" name="address" />
                                    <input type="hidden" id="latitude" name="latitude" />
                                    <input type="hidden" id="longitude" name="longitude" />
                                </div>
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="distance">
                                        <option value="1">1 Miles</option>
                                        @for($i=5;$i<=30;$i=$i+5)
                                            <option value="{{$i}}">{{$i}} Miles</option>
                                        @endfor
                                        <option value="40" selected>40 Miles</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="min_bed">
                                        <option value="">Minimum Bed</option>
                                        <option value="1">1 Bed</option>
                                        @for($i=2;$i<=5;$i=$i+1)
                                            <option value="{{$i}}">{{$i}} Beds</option>
                                        @endfor
                                        <option value="6">5+ Beds</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="max_bed">
                                        <option value="">Maximum Bed</option>
                                        <option value="1">1 Bed</option>
                                        @for($i=2;$i<=4;$i=$i+1)
                                            <option value="{{$i}}">{{$i}} Beds</option>
                                        @endfor
                                        <option value="6">5+ Beds</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="min_price">
                                        <option value="">Minimum price</option>
                                        @for($i=50000;$i<=1000000;$i=$i+10000)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                        @for($i=1100000;$i<=10000000;$i=$i+100000)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="selectpicker search-fields" name="max_price">
                                        <option value="">Maximum Price</option>
                                        @for($i=50000;$i<=1000000;$i=$i+10000)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                        @for($i=1100000;$i<=10000000;$i=$i+100000)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>

                                <div class="form-group mb-0">
                                    <button type="submit" class="search-button">Search</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- property location section end -->
    <!-- Properties section body start -->
    <div class="properties-section-body content-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <!-- Option bar start -->
                    <div class="row">
                        @foreach($properties as $property)
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="property-box">
                                    <div class="property-thumbnail">
                                        <a href="{{route('property.details',$property->id)}}" class="property-img">
                                            <div class="tag @if($property->type=='1')sale @elseif($property->type=='0') rent @endif">
                                                FOR @if($property->type=='1')SALE @elseif($property->type=='0') RENT @endif
                                            </div>
                                            <div class="price-box"><span>{{env('APP_CURRENCY')}}{{number_format($property->price, 2, '.', ',')}}</span> month</div>
                                            <img class="d-block w-100" src="{{asset('uploads/property/'.$property->image['0'])}}"
                                                alt="properties">
                                        </a>
                                    </div>
                                    <div class="detail">
                                        <h1 class="title">
                                            <a href="{{route('property.details',$property->id)}}">{{Str::limit($property->name,'30','...')}}</a>
                                        </h1>
                                        <div class="location">
                                            <a href="properties-details.html">
                                                <i class="flaticon-pin"></i>{{Str::limit($property->address,'40','...')}},{{$property->city}}
                                            </a>
                                        </div>
                                        <ul class="facilities-list clearfix">
                                            <li>
                                                <i class="flaticon-bed"></i> {{$property->living}} Beds
                                            </li>
                                            <li>
                                                <i class="flaticon-bathroom"></i> {{$property->bath}} Baths
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <!-- Page navigation start -->
                    <div class="pagination-box p-box-2 text-center">
                        {{ $properties->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Properties section body end -->
@endsection

@push('js')

    <script type="text/javascript">
        // jQuery(function($) {
        //     <!-- Asynchronously Load the map API  -->
        //     var script = document.createElement('script');
        //     document.body.appendChild(script);
        // });
        var options = {
            componentRestrictions: {country: "uk"}
        };
        function initialize() {
            var input = document.getElementById('searchTextField');
            var autocomplete = new google.maps.places.Autocomplete(input,options);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                document.getElementById('address').value = place.name;
                document.getElementById('latitude').value = place.geometry.location.lat();
                document.getElementById('longitude').value = place.geometry.location.lng();
                //alert("This function is working!");
                //alert(place.name);
                // alert(place.address_components[0].long_name);
            });
            google.maps.event.addDomListener(window, 'load', initialize)



            var map;
            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                mapTypeId: 'roadmap'
            };
            <!-- Display a map on the page -->
            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
            map.setTilt(45);
            <!-- Multiple Markers -->
            // var markers = [
            //     ['Bondi Beach', -33.890542, 151.274856, 4],
            //     ['Coogee Beach', -33.923036, 151.259052, 5],
            //     ['Cronulla Beach', -34.028249, 151.157507, 3],
            //     ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
            //     ['Maroubra Beach', -33.950198, 151.259302, 1]
            // ];
            var markers = [
                    @foreach($properties as $property)
                [{{$property->city}},{{$property->latitude}},{{$property->longitude}},{{$loop->index+1}}],
                @endforeach
            ];
            <!-- Info Window Content -->
            var infoWindowContent = [
                    @foreach($properties as $property)
                ['<div class="info_content">' +
                '<h6>{{$property->address}}</h6>' +
                '</div>'],
                @endforeach
            ];
            <!-- Display multiple markers on a map -->
            var infoWindow = new google.maps.InfoWindow(), marker, i;
            <!-- Loop through our array of markers & place each one on the map   -->
            for( i = 0; i < markers.length; i++ ) {
                var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: markers[i][0]
                });
                <!-- Allow each marker to have an info window     -->
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infoWindow.setContent(infoWindowContent[i][0]);
                        infoWindow.open(map, marker);
                    }
                })(marker, i));
                <!-- Automatically center the map fitting all markers on the screen -->
                map.fitBounds(bounds);
            }
            <!-- Override our map zoom level once our fitBounds function runs (Make sure it only runs once) -->
            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
                this.setZoom(10);
                google.maps.event.removeListener(boundsListener);
            });
        }
    </script>
@endpush
