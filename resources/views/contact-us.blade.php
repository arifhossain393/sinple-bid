@extends('layouts.frontend.app')

@section('title','Contact-Us')

@push('css')
    <style>
        .contact-1{
            background: #ecebea;
        }
        .contact-1 .form-control {
            border: 1px solid #7b7b7b;
            background: #fff;
        }
        .contact-1 .form-control:focus {
            border: 1px solid #160ff2;
            background: #fff;
        }
    </style>
@endpush

@section('content')
    <!-- Sub banner start -->
    <div class="sub-banner">
        <div class="container breadcrumb-area">
            <div class="breadcrumb-areas">
                <h1>Contact Us</h1>
                <ul class="breadcrumbs">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li class="active">Contact Us</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Sub Banner end -->
    <!-- Contact 1 start -->
    <div class="contact-1" style="padding: 30px 0;">
        <div class="container">
            <!-- Main title -->
            <div class="main-title text-center">
                <h1>Contact Us</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
            </div>
            <!-- Contact info -->
            <div class="contact-info">
                <div class="row">
                    <div class="col-md-3 col-sm-6 mrg-btn-50">
                        <i class="flaticon-pin"></i>
                        <p>Office Address</p>
                        <strong>LU1 1HS, England, UK</strong>
                    </div>
                    <div class="col-md-3 col-sm-6 mrg-btn-50">
                        <i class="flaticon-phone"></i>
                        <p>Phone Number</p>
                        <strong>020 3805 5857</strong>
                    </div>
                    <div class="col-md-3 col-sm-6 mrg-btn-50">
                        <i class="flaticon-mail"></i>
                        <p>Email Address</p>
                        <strong>info@gmswebdesign.co.uk</strong>
                    </div>
                    <div class="col-md-3 col-sm-6 mrg-btn-50">
                        <i class="flaticon-earth"></i>
                        <p>Web</p>
                        <strong>www.gmsestate.co.uk</strong>
                    </div>
                </div>
            </div>
            <form role="form" id="contact_form" method="post" action="{{route('contact.store')}}" >
                @csrf
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group name">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                            @error('name')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group email">
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                            @error('email')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group subject">
                            <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject">
                            @error('subject')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group number">
                            <input type="text" name="phone" id="phone" class="form-control" placeholder="Number">
                            @error('phone')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group message">
                            <textarea class="form-control" name="message" id="message" placeholder="Write message"></textarea>
                            @error('message')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="send-btn text-center">
                            <button type="submit" value="submit" class="btn btn-md button-theme" id="btn_submit" onClick="proceed();">Send Message</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Contact 1 end -->

    <section>
        <div class="mapsection">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2463.204519109898!2d-0.40043838478363625!3d51.87547919154534!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876483daf59747d%3A0x9344ccdf09edb7a5!2sRedrow%20-%20Saxon%20Square%2C%20Luton!5e0!3m2!1sen!2sbd!4v1568809184216!5m2!1sen!2sbd" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </section>

@endsection

@push('js')

@endpush
