@extends('layout.backend.app')

@section('title','Subscription')

@push('css')

@endpush

@section('content')

    <!-- #END# Basic Examples -->
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                @if(session('successMsg'))
                    <div class="alert alert-success">


                        <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none';">×</button>
                        <span>
                                    <b>Success - </b>{{session('successMsg')}}</span>
                    </div>

                @endif
                <div class="header">
                    <h2 class="text-left">
                    <b>Manage Package Subscription</b>
                    </h2>
                    <h2 class="text-right">
                        <a  href="{{route('subscription.create')}}" class="btn btn-primary" href="">Add New   <i class="material-icons">add</i></a>
                    </h2>

                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th >SL</th>
                                <th >Image</th>
                                <th >Name</th>
                                <th >Price</th>
                                <th >Days</th>
                                <th >Status</th>
                                <th >Add Property</th>
                                <th >Date </th>
                                <th >Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($subscriptions as $subscription)
                            <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>
                            <img src="{{asset('uploads/subscription/'.$subscription->image)}}" alt="{{$subscription->name}}" width="50">
                            </td>
                            <td>{{$subscription->name}}</td>

                            <td>{{env('APP_CURRENCY')}}{{$subscription->price}}</td>
                            <td>{{$subscription->days}}</td>
                            <td>
                            @if($subscription->status===1)
                            Active
                            @else
                            Inactive
                            @endif
                            </td>
                            <td>{{$subscription->property}}</td>
                            <td>{{\Carbon\Carbon::parse($subscription->created_at)->format('d/m/Y')}}</td>
                            <td>

                            <form action="{{route('subscription.destroy',$subscription->id)}}" method="POST">
                            @method('DELETE')
                            @csrf
{{--                            <a href="{{route('subscription.show',$subscription->id)}}" class="btn btn-info">View</a>--}}
                            <a href="{{route('subscription.edit',$subscription->id)}}" class="btn btn-warning">Edit</a>
                           <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger">Delete</button>
                            </form>

                            </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('js')

@endpush
