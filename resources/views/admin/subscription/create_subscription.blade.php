@extends('layout.backend.app')

@section('title','Add Subscription')

@push('css')
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    <style>
        .col-lg-10 {

            margin-left: 96px !important;
        }
        .form-group .form-line .form-label {

            color: #131212 !important;

        }
        .fallback {
            margin-top: 15px !important;
        }
        .form-control {
            color: #000 !important;
        }
        .card .body {

            color: #000 !important;
        }
    </style>
@endpush

@section('content')
    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" >
            <div class="card">

                <div class="header">
                    <h2 class="card-inside-title"><b>Add Subscription</b></h2>

                </div>
                <div class="body">
                    <form id="form_validation" method="post" action="{{route('subscription.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="name" value="{{old('name')}}" >
                                @error('name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                                <label class="form-label">Package Name:</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="price" value="{{old('price')}}">
                                @error('price')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror

                                <label class="form-label">Package Price:</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">

                                <select class="form-line" name="status" >

                                    <option  value="" selected>Days of Subscription</option>
                                    <option @if(old('status')=='30')  @endif value="30">30</option>


                                </select>

                                @error('days')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror


                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="property" value="" >
                                @error('property')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror

                                <label class="form-label">Property Added</label>
                            </div>
                        </div>

                        <div class="fallback">
                            <label class="form-label form-label-text "><b>Image:</b></label>
                            <input name="image" type="file" />
                            @error('image')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <br>
                        <div class="form-group form-float">
                            <select class="form-line" name="status" >
                                <option value="">Select Status</option>
                                <option @if(old('status')=='1') selected @endif value="1">Active</option>
                                <option @if(old('status')=='0') selected @endif value="0">In Active</option>

                            </select>
                            @error('status')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <h2 class="card-inside-title">Description:</h2>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea  rows="4" name="description" class="form-control no-resize" placeholder="Please type what you want...">
                                            {{old('description')}}
                                        </textarea>
                                        @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        <a class="btn btn-danger waves-effect" href="{{route('property.index')}}">
                            <span>Back</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Validation -->
    <!-- Advanced Validation -->

    <!-- #END# Advanced Validation -->
    <!-- Validation Stats -->

    <!-- #END# Validation Stats -->
@endsection

@push('js')
    <!-- Bootstrap Material Datetime Picker Plugin Js -->

    <script src="{{asset('assets/backend/plugins/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        });
    </script>


@endpush
