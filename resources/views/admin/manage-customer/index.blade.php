@extends('layout.backend.app')

@section('title','Customer')

@push('css')

    <style>
        .employee-pic{
            width: 60px !important;
            height: 60px !important;
            margin-left: 0px !important;
            /* text-align: center; */
            /* vertical-align: middle; */
            margin: 0 auto !important;
            display: block;
            border-radius: 50%;
        }
        td {
            color: #000 !important;
        }
        th {

            color: #000 !important;
        }
    </style>
@endpush

@section('content')

    <!-- #END# Basic Examples -->
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                @if(session('successMsg'))
                    <div class="alert alert-success">


                        <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none';">×</button>
                        <span>
                                    <b>Success - </b>{{session('successMsg')}}</span>
                    </div>

                @endif

                <div class="header">
                    <h2 class="text-left">
                        <b>Manage Customer</b>
                    </h2>
                    <h2 class="text-right">
                        @isset(auth('admin')->user()->role->permission->permission['customer']['add'])
                            <a  href="{{route('customer.create')}}" class="btn btn-primary" href="">Add New   <i class="material-icons">add</i></a>
                        @endisset

                    </h2>

                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th width="5%">User Id</th>
                                <th width="10%">Photo</th>
                                <th width="10%">Name</th>
                                {{--<th width="15%">User Type</th>--}}
                                <th width="20%">Address</th>
                                <th width="10%">Email</th>
                                {{--<th width="5%">Status</th>--}}
                                <th width="15%">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($users as $users)
                                <tr>
                                    <td>{{$users->id}}</td>
                                    <td>

                                        <img src="{{asset('uploads/user/'.$users->image)}}" alt="{{$users->name}}" width="50">


                                        {{--<img src="{{asset('uploads/user/'.$user->image)}}" alt="{{$user->name}}" width="50">--}}
                                    </td>

                                    <td>{{$users->f_name}}</td>
                                    {{--<td>--}}
                                        {{--@if($users->role_id=='1')--}}
                                            {{--Buyer--}}
                                        {{--@elseif($users->role_id=='2')--}}
                                            {{--Tenant--}}
                                        {{--@endif--}}
                                    {{--</td>--}}
                                    <td>{{$users->address_1}}</td>
                                    <td>{{$users->email}}</td>

                                    {{--<td> <a href="{{route('user.activate',$users->id)}}" class="btn @if($users->status=='0')btn-info @else btn-success @endif">--}}
                                            {{--@if($users->status=='0')--}}
                                                {{--Active--}}
                                            {{--@else--}}
                                                {{--Activated--}}
                                            {{--@endif--}}
                                        {{--</a>--}}
                                    {{--</td>--}}



                                    <td>

                                        <form action="{{route('customer.destroy',$users->id)}}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            @isset(auth('admin')->user()->role->permission->permission['customer']['edit'])
                                                <a href="{{route('customer.edit',$users->id)}}" class="btn btn-warning">Edit</a>
                                            @endisset
                                            @isset(auth('admin')->user()->role->permission->permission['customer']['delete'])
                                                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger">Delete</button>
                                            @endisset

                                        </form>

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>


@endpush

