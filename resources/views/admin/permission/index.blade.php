@extends('layout.backend.app')

@section('title','Permission')

@push('css')

        <style>

            td {
                color: #000 !important;
            }
            th {

                color: #000 !important;
            }
        </style>

@endpush

@section('content')

    <!-- #END# Basic Examples -->
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                @if(session('successMsg'))
                    <div class="alert alert-success">


                        <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none';">×</button>
                        <span>
                                    <b>Success - </b>{{session('successMsg')}}</span>
                    </div>

                @endif
                <div class="header">
                    <h2 class="text-left">
                        <b>Manage Permission</b>
                    </h2>
                    <h2 class="text-right">
                        <a  href="{{route('adminPermission.create')}}" class="btn btn-primary" href="">Add New   <i class="material-icons">add</i></a>
                    </h2>

                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th >SL</th>
                                <th >Name</th>
                                <th >Action</th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach($permissions as $permission)
                                <tr>
                                    <td>{{$loop->iteration}}</td>

                                    <td>{{$permission->role->name}}</td>

                                    <td>

                                        <form action="{{route('adminPermission.destroy',$permission->id)}}" method="POST">
                                            @method('DELETE')
                                            @csrf

                                            <a href="{{route('adminPermission.edit',$permission->id)}}" class="btn btn-warning">Edit</a>

                                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger">Delete</button>
                                        </form>

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('js')

@endpush
