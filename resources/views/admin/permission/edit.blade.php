@extends('layout.backend.app')
@section('title','Edit-Permission')
@push('css')
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    {{--<link href="{{asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" />--}}

    <style>

        .col-lg-10 {

            margin-left: 96px !important;
        }
        .form-group .form-line .form-label {

            color: #131212 !important;

        }
        .fallback {
            margin-top: 15px !important;
        }
        .form-control {
            color: #000 !important;
        }
        .card .body {

            color: #000 !important;
        }
    </style>
@endpush
@section('content')
    <div class="row clearfix">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" >
            <div class="card">

                <div class="header">
                    <h2 class="card-inside-title"><b>Update Permission</b></h2>

                </div>
                <div class="body">
                    <form id="form_validation" method="post" action="{{route('adminPermission.update',$permission->id)}}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group form-float">

                                <select class="form-line" name="role_id" id="">

                                    @foreach(\App\Role::all() as $role)
                                        <option value="{{$role->id}}" @if($role->id==$permission->role->id)selected @endif>{{$role->name}}</option>
                                    @endforeach
                                </select>
                                @error('role_id')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror


                        </div>
                        {{--<div class="form-group form-float">--}}
                            {{--<select class="form-line" name="country">--}}
                                {{--<option value="">*Country</option>--}}

                                {{--@foreach($countries as $country)--}}
                                    {{--@if($country->id==$user->country)--}}
                                        {{--<option value="{{$country->id}}"selected>{{$country->name}}</option>--}}
                                    {{--@else--}}
                                        {{--<option value="{{$country->id}}">{{$country->name}}</option>--}}
                                    {{--@endif--}}
                                {{--@endforeach--}}
                                {{--@error('country')--}}
                                {{--<span class="text-danger">{{ $message }}</span>--}}
                                {{--@enderror--}}
                            {{--</select>--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <label for="">Bids</label>
                            <input type="checkbox" @isset($permission->permission['bid']['view']) checked @endisset name="permission[bid][view]" id="bid_view" class="filled-in chk-col-pink">
                            <label for="bid_view" ><strong>View</strong></label>
                            <input type="checkbox" @isset($permission->permission['bid']['add']) checked @endisset name="permission[bid][add]" id="bid_add" class="filled-in chk-col-pink">
                            <label for="bid_add"><strong>Add</strong></label>
                            <input type="checkbox" @isset($permission->permission['bid']['edit']) checked @endisset name="permission[bid][edit]" id="bid_edit" class="filled-in chk-col-pink">
                            <label for="bid_edit"><strong>Edit</strong></label>
                            <input type="checkbox" @isset($permission->permission['bid']['delete']) checked @endisset name="permission[bid][delete]" id="bid_delete" class="filled-in chk-col-pink">
                            <label for="bid_delete"><strong>Delete</strong></label>
                        </div>
                        <div class="form-group">
                            <label for="">Agent</label>
                            <input type="checkbox" @isset($permission->permission['agent']['view']) checked @endisset name="permission[agent][view]" id="agent_view" class="filled-in chk-col-pink">
                            <label for="agent_view"><strong>View</strong></label>
                            <input type="checkbox" @isset($permission->permission['agent']['add']) checked @endisset name="permission[agent][add]" id="agent_add" class="filled-in chk-col-pink">
                            <label for="agent_add"><strong>Add</strong></label>
                            <input type="checkbox" @isset($permission->permission['agent']['edit']) checked @endisset name="permission[agent][edit]" id="agent_edit" class="filled-in chk-col-pink">
                            <label for="agent_edit"><strong>Edit</strong></label>
                            <input type="checkbox" @isset($permission->permission['agent']['delete']) checked @endisset name="permission[agent][delete]" id="agent_delete" class="filled-in chk-col-pink">
                            <label for="agent_delete"><strong>Delete</strong></label>
                        </div>

                        <div class="form-group">
                            <label for="">Customer :</label>
                            <input type="checkbox" @isset($permission->permission['customer']['view']) checked @endisset  name="permission[customer][view]" id="customer_view" class="filled-in chk-col-pink">
                            <label for="customer_view"><strong>View</strong></label>
                            <input type="checkbox"  @isset($permission->permission['customer']['add']) checked @endisset  name="permission[customer][add]" id="customer_add" class="filled-in chk-col-pink">
                            <label for="customer_add"><strong>Add</strong></label>
                            <input type="checkbox" @isset($permission->permission['customer']['edit']) checked @endisset  name="permission[customer][edit]" id="customer_edit" class="filled-in chk-col-pink">
                            <label for="customer_edit"><strong>Edit</strong></label>
                            <input type="checkbox" @isset($permission->permission['customer']['delete']) checked @endisset  name="permission[customer][delete]" id="customer_delete" class="filled-in chk-col-pink">
                            <label for="customer_delete"><strong>Delete</strong></label>
                        </div>
                        <div class="form-group">
                            <label for="">Property :</label>
                            <input type="checkbox" @isset($permission->permission['property']['view']) checked @endisset  name="permission[property][view]" id="property_view" class="filled-in chk-col-pink">
                            <label for="property_view"><strong>View</strong></label>
                            <input type="checkbox" @isset($permission->permission['property']['add'])  checked @endisset name="permission[property][add]" id="property_add" class="filled-in chk-col-pink">
                            <label for="property_add"><strong>Add</strong></label>
                            <input type="checkbox" @isset($permission->permission['property']['edit']) checked @endisset name="permission[property][edit]" id="property_edit" class="filled-in chk-col-pink">
                            <label for="property_edit"><strong>Edit</strong></label>
                            <input type="checkbox" @isset($permission->permission['property']['delete']) checked @endisset name="permission[property][delete]" id="property_delete" class="filled-in chk-col-pink">
                            <label for="property_delete"><strong>Delete</strong></label>
                        </div>

                        <div class="form-group">
                            <label for="">Subscription :</label>
                            <input type="checkbox" @isset($permission->permission['subscription']['view']) checked @endisset name="permission[subscription][view]" id="subscription_view" class="filled-in chk-col-pink">
                            <label for="subscription_view"><strong>View</strong></label>
                            <input type="checkbox" @isset($permission->permission['subscription']['add']) checked @endisset name="permission[subscription][add]" id="subscription_add" class="filled-in chk-col-pink">
                            <label for="subscription_add"><strong>Add</strong></label>
                            <input type="checkbox" @isset($permission->permission['subscription']['edit']) checked @endisset name="permission[subscription][edit]" id="subscription_edit" class="filled-in chk-col-pink">
                            <label for="subscription_edit"><strong>Edit</strong></label>
                            <input type="checkbox" @isset($permission->permission['subscription']['delete']) checked @endisset name="permission[subscription][delete]" id="subscription_delete" class="filled-in chk-col-pink">
                            <label for="subscription_delete"><strong>Delete</strong></label>
                        </div>

                        <div class="form-group">
                            <label for="">User :</label>
                            <input type="checkbox" @isset($permission->permission['user']['view']) checked @endisset name="permission[user][view]" id="user_view" class="filled-in chk-col-pink">
                            <label for="user_view"><strong>View</strong></label>
                            <input type="checkbox" @isset($permission->permission['user']['add']) checked @endisset name="permission[user][add]" id="user_add" class="filled-in chk-col-pink">
                            <label for="user_add"><strong>Add</strong></label>
                            <input type="checkbox" @isset($permission->permission['user']['edit']) checked @endisset name="permission[user][edit]" id="user_edit" class="filled-in chk-col-pink">
                            <label for="user_edit"><strong>Edit</strong></label>
                            <input type="checkbox" @isset($permission->permission['user']['delete']) checked @endisset name="permission[user][delete]" id="user_delete" class="filled-in chk-col-pink">
                            <label for="user_delete"><strong>Delete</strong></label>
                        </div>
                        <div class="form-group">
                            <label for="">Customer Review :</label>
                            <input type="checkbox" @isset($permission->permission['review']['view']) checked @endisset name="permission[review][view]" id="review_view" class="filled-in chk-col-pink">
                            <label for="review_view"><strong>View</strong></label>
                            <input type="checkbox" @isset($permission->permission['review']['add']) checked @endisset name="permission[review][add]" id="review_add" class="filled-in chk-col-pink">
                            <label for="review_add"><strong>Add</strong></label>
                            <input type="checkbox" @isset($permission->permission['review']['edit']) checked @endisset name="permission[review][edit]" id="review_edit" class="filled-in chk-col-pink">
                            <label for="review_edit"><strong>Edit</strong></label>
                            <input type="checkbox" @isset($permission->permission['review']['delete']) checked @endisset name="permission[review][delete]" id="review_delete" class="filled-in chk-col-pink">
                            <label for="review_delete"><strong>Delete</strong></label>
                        </div>

                        <div class="form-group">
                            <label for="">Role :</label>
                            <input type="checkbox" @isset($permission->permission['role']['view']) checked @endisset name="permission[role][view]" id="role_view" class="filled-in chk-col-pink">
                            <label for="role_view"><strong>View</strong></label>
                            <input type="checkbox" @isset($permission->permission['role']['add']) checked @endisset name="permission[role][add]" id="role_add" class="filled-in chk-col-pink">
                            <label for="role_add"><strong>Add</strong></label>
                            <input type="checkbox" @isset($permission->permission['role']['edit']) checked @endisset name="permission[role][edit]" id="role_edit" class="filled-in chk-col-pink">
                            <label for="role_edit"><strong>Edit</strong></label>
                            <input type="checkbox" @isset($permission->permission['role']['delete']) checked @endisset name="permission[role][delete]" id="role_delete" class="filled-in chk-col-pink">
                            <label for="role_delete"><strong>Delete</strong></label>
                        </div>




                        <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        <a class="btn btn-danger waves-effect" href="{{url()->previous()}}">
                            <span>Back</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <!-- Bootstrap Material Datetime Picker Plugin Js -->

    <script src="{{asset('assets/backend/js/pages/forms/form-validation.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/forms/basic-form-elements.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/forms/advanced-form-elements.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/dropzone/dropzone.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

    <script src="{{asset('assets/backend/plugins/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        });
    </script>

    <script>
        $('#start_date').bootstrapMaterialDatePicker();
        $('#end_date').bootstrapMaterialDatePicker();

    </script>

@endpush








