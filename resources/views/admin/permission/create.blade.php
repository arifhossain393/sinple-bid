@extends('layout.backend.app')
@section('title','Permission')
@push('css')
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    {{--<link href="{{asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" />--}}

    <style>

        .col-lg-10 {

            margin-left: 96px !important;
        }
        .form-group .form-line .form-label {

            color: #131212 !important;

        }
        .fallback {
            margin-top: 15px !important;
        }
        .form-control {
            color: #000 !important;
        }
        .card .body {

            color: #000 !important;
        }
    </style>
@endpush

@section('content')
    <div class="row clearfix">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" >
            <div class="card">

                <div class="header">
                    <h2 class="card-inside-title"><b>Create Permission</b></h2>

                </div>
                <div class="body">
                    <form id="form_validation" method="post" action="{{route('adminPermission.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group form-float">
                            <div class="form-line">
                                <select name="role_id" id="">
                                    @foreach(\App\Role::all() as $role)
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                        @endforeach
                                </select>
                                @error('role_id')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Bids :</label>
                            <input type="checkbox" name="permission[bid][view]" id="bid_view" class="filled-in chk-col-pink">
                            <label for="bid_view"><strong>View</strong></label>
                            <input type="checkbox" name="permission[bid][add]" id="bid_add" class="filled-in chk-col-pink">
                            <label for="bid_add"><strong>Add</strong></label>
                            <input type="checkbox" name="permission[bid][edit]" id="bid_edit" class="filled-in chk-col-pink">
                            <label for="bid_edit"><strong>Edit</strong></label>
                            <input type="checkbox" name="permission[bid][delete]" id="bid_delete" class="filled-in chk-col-pink">
                            <label for="bid_delete"><strong>Delete</strong></label>
                        </div>
                        <div class="form-group">
                            <label for="">Agent :</label>
                            <input type="checkbox" name="permission[agent][view]" id="agent_view" class="filled-in chk-col-pink">
                            <label for="agent_view"><strong>View</strong></label>
                            <input type="checkbox" name="permission[agent][add]" id="agent_add" class="filled-in chk-col-pink">
                            <label for="agent_add"><strong>Add</strong></label>
                            <input type="checkbox" name="permission[agent][edit]" id="agent_edit" class="filled-in chk-col-pink">
                            <label for="agent_edit"><strong>Edit</strong></label>
                            <input type="checkbox" name="permission[agent][delete]" id="agent_delete" class="filled-in chk-col-pink">
                            <label for="agent_delete"><strong>Delete</strong></label>
                        </div>
                        <div class="form-group">
                            <label for="">Customer :</label>
                            <input type="checkbox" name="permission[customer][view]" id="customer_view" class="filled-in chk-col-pink">
                            <label for="customer_view"><strong>View</strong></label>
                            <input type="checkbox" name="permission[customer][add]" id="customer_add" class="filled-in chk-col-pink">
                            <label for="customer_add"><strong>Add</strong></label>
                            <input type="checkbox" name="permission[customer][edit]" id="customer_edit" class="filled-in chk-col-pink">
                            <label for="customer_edit"><strong>Edit</strong></label>
                            <input type="checkbox" name="permission[customer][delete]" id="customer_delete" class="filled-in chk-col-pink">
                            <label for="customer_delete"><strong>Delete</strong></label>
                        </div>
                        <div class="form-group">
                            <label for="">Property :</label>
                            <input type="checkbox" name="permission[property][view]" id="property_view" class="filled-in chk-col-pink">
                            <label for="property_view"><strong>View</strong></label>
                            <input type="checkbox" name="permission[property][add]" id="property_add" class="filled-in chk-col-pink">
                            <label for="property_add"><strong>Add</strong></label>
                            <input type="checkbox" name="permission[property][edit]" id="property_edit" class="filled-in chk-col-pink">
                            <label for="property_edit"><strong>Edit</strong></label>
                            <input type="checkbox" name="permission[property][delete]" id="property_delete" class="filled-in chk-col-pink">
                            <label for="property_delete"><strong>Delete</strong></label>
                        </div>

                        <div class="form-group">
                            <label for="">Subscription :</label>
                            <input type="checkbox" name="permission[subscription][view]" id="subscription_view" class="filled-in chk-col-pink">
                            <label for="subscription_view"><strong>View</strong></label>
                            <input type="checkbox" name="permission[subscription][add]" id="subscription_add" class="filled-in chk-col-pink">
                            <label for="subscription_add"><strong>Add</strong></label>
                            <input type="checkbox" name="permission[subscription][edit]" id="subscription_edit" class="filled-in chk-col-pink">
                            <label for="subscription_edit"><strong>Edit</strong></label>
                            <input type="checkbox" name="permission[subscription][delete]" id="subscription_delete" class="filled-in chk-col-pink">
                            <label for="subscription_delete"><strong>Delete</strong></label>
                        </div>

                        <div class="form-group">
                            <label for="">User :</label>
                            <input type="checkbox" name="permission[user][view]" id="user_view" class="filled-in chk-col-pink">
                            <label for="user_view"><strong>View</strong></label>
                            <input type="checkbox" name="permission[user][add]" id="user_add" class="filled-in chk-col-pink">
                            <label for="user_add"><strong>Add</strong></label>
                            <input type="checkbox" name="permission[user][edit]" id="user_edit" class="filled-in chk-col-pink">
                            <label for="user_edit"><strong>Edit</strong></label>
                            <input type="checkbox" name="permission[user][delete]" id="user_delete" class="filled-in chk-col-pink">
                            <label for="user_delete"><strong>Delete</strong></label>
                        </div>
                        <div class="form-group">
                            <label for="">Customer Review :</label>
                            <input type="checkbox" name="permission[review][view]" id="review_view" class="filled-in chk-col-pink">
                            <label for="review_view"><strong>View</strong></label>
                            <input type="checkbox" name="permission[review][add]" id="review_add" class="filled-in chk-col-pink">
                            <label for="review_add"><strong>Add</strong></label>
                            <input type="checkbox" name="permission[review][edit]" id="review_edit" class="filled-in chk-col-pink">
                            <label for="review_edit"><strong>Edit</strong></label>
                            <input type="checkbox" name="permission[review][delete]" id="review_delete" class="filled-in chk-col-pink">
                            <label for="review_delete"><strong>Delete</strong></label>
                        </div>
                        <div class="form-group">
                            <label for="">Role :</label>
                            <input type="checkbox" name="permission[role][view]" id="role_view" class="filled-in chk-col-pink">
                            <label for="role_view"><strong>View</strong></label>
                            <input type="checkbox" name="permission[role][add]" id="role_add" class="filled-in chk-col-pink">
                            <label for="role_add"><strong>Add</strong></label>
                            <input type="checkbox" name="permission[role][edit]" id="role_edit" class="filled-in chk-col-pink">
                            <label for="role_edit"><strong>Edit</strong></label>
                            <input type="checkbox" name="permission[role][delete]" id="role_delete" class="filled-in chk-col-pink">
                            <label for="role_delete"><strong>Delete</strong></label>
                        </div>



                        <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        <a class="btn btn-danger waves-effect" href="{{url()->previous()}}">
                            <span>Back</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <!-- Bootstrap Material Datetime Picker Plugin Js -->

    <script src="{{asset('assets/backend/js/pages/forms/form-validation.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/forms/basic-form-elements.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/forms/advanced-form-elements.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/dropzone/dropzone.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

    <script src="{{asset('assets/backend/plugins/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        });
    </script>

    <script>
        $('#start_date').bootstrapMaterialDatePicker();
        $('#end_date').bootstrapMaterialDatePicker();

    </script>

@endpush








