@extends('layout.backend.app')

@section('title','Bid-List')

@push('css')
    <style>
        .panel-group .panel .panel-heading a {
            display: block;
            padding: 20px 15px;
        }
        .btn:not(.btn-link):not(.btn-circle) span{display:none}
        /*.form-inline .form-control {*/
        /*    margin-top: -36%*/
        /*}*/

        h3, .h3 {

            margin-left: 15px !important;
        }
        input {
            margin-top: 26px !important;
            margin-right:10px !important ;
        }

        #accordion_1 {
            margin-top: 13px !important;
        }
        .btn.btn-primary {
            margin-right: 17px !important;
            margin-top: -3px !important;
        }

        input {
            width: 220px !important;
        }
    </style>
@endpush

@section('content')


    <div class="row clearfix card">
      <div>
          <h3 class="pull-left">Bids</h3>
          <div class="pull-right">
              <form action="{{route('search-bid-property')}}" method="GET">
                  <input type="text" name="keyword" placeholder="Please enter name or price">
                  <button class="btn btn-primary" type="submit">Search</button>
              </form>
          </div>
      </div>

        <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">



            <div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
                @foreach($properties as $property)

                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="headingTwo_{{$property->id}}">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_{{$property->id}}" href="#collapseTwo_{{$property->id}}" aria-expanded="false" aria-controls="collapseTwo_{{$property->id}}">
                                    <div class="pull-left">
                                        {{--@php--}}
                                            {{--echo mt_rand(100000, 999999).$property->id;--}}
                                        {{--@endphp.--}}
                                        {{$property->id}}.{{$property->name}} -{{$property->agent->c_name}} -{{env('APP_CURRENCY')}}{{$property->price}}

                                    </div>

                                    <div class="pull-right">
                                        Bid Closing Date : {{\Carbon\Carbon::parse($property->end_date)->format('l d M, h:i a')}}
                                    </div>
                                </a>

                            </h4>
                        </div>
                        <div id="collapseTwo_{{$property->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_{{$property->id}}" aria-expanded="false" style="height: 0px;">
                            <div class="panel-bodyss">
                                <div class="body">
                                    <div class="table-responsive">
                                        <table width="100%" class="table table-bordered table-striped table-hover dataTable js-exportable no-footer">
                                            <thead>
                                            <tr>
                                                <td>Bid no</td>
                                                <td>User Id</td>
                                                <td>Name</td>
                                                <td>Email</td>

                                                <td>Bid Amount</td>
                                                <td>Bid Submitted</td>
                                                <td>Type</td>
                                                <td>Status</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($property->bids as $bid)
                                                <tr>
                                                    <td>
                                                        {{$bid->id}}

                                                    </td>
                                                    <td>
                                                        {{$bid->user->id}}

                                                    </td>
                                                    <td>{{$bid->user->f_name." ".$bid->user->l_name}}</td>
                                                    <td>{{$bid->user->email}}</td>
                                                    <td>{{env('APP_CURRENCY')}}{{number_format($bid->bid_amount, 2, '.', ',')}}</td>
                                                    <td>{{\Carbon\Carbon::parse($bid->created_at)->format('l d M, h:i a')}}</td>
                                                    <td>
                                                        @if($bid->property->type=='1')Sale
                                                        @elseif($bid->property->type=='0')Rent
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @php
                                                            $minute = \Carbon\Carbon::parse(\Carbon\Carbon::now())->diffInMinutes(\Carbon\Carbon::parse($bid->property->end_date),$absolute = false);
                                                               //$minute = \Carbon\Carbon::parse($bid->property->start_date)->diffInMinutes(\Carbon\Carbon::parse($bid->property->end_date),$absolute = false);
                                                        @endphp

                                                        <form action="{{route('bid.status.change')}}" method="POST">
                                                            @csrf
                                                            <input type="hidden" name="id" value="{{$bid->id}}">
                                                            <select name="status" onchange="this.form.submit()">
                                                                @if($minute<0)
                                                                    @if($property->bids->where('status','!=','rejected')->max('bid_amount')==$bid->bid_amount)
                                                                        <option value="win"  selected >Win</option>
                                                                    @else
                                                                        <option value="pending" @if( $bid->status=='rejected' || $bid->status=='pending') disabled @endif @if($bid->status=='pending') selected @endif >Pending</option>
                                                                        <option value="rejected" @if($bid->status=='rejected' || $bid->status=='pending' ) disabled @endif @if($bid->status=='rejected')  selected @endif >Rejected</option>
                                                                    @endif

                                                                @else
                                                                    <option value="pending" @if( $bid->status=='rejected') disabled @endif @if($bid->status=='pending') selected @endif>Pending</option>
                                                                    <option value="rejected"  @if($bid->status=='rejected') disabled @endif @if($bid->status=='rejected')  selected @endif>Rejected</option>

                                                                @endif
                                                            </select>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="pagination pull-right">
                    {{$properties->links()}}
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')

    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
@endpush

