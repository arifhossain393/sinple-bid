@extends('layout.backend.app')

@section('title','Agent')

@push('css')

    <style>
        .employee-pic{
            width: 60px !important;
            height: 60px !important;
            margin-left: 0px !important;
            /* text-align: center; */
            /* vertical-align: middle; */
            margin: 0 auto !important;
            display: block;
            border-radius: 50%;
        }
        td {
            color: #000 !important;
        }
        th {

            color: #000 !important;
        }
    </style>
@endpush

@section('content')

    <!-- #END# Basic Examples -->
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                @if(session('successMsg'))
                    <div class="alert alert-success">


                        <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none';">×</button>
                        <span>
                                    <b>Success - </b>{{session('successMsg')}}</span>
                    </div>

                @endif
                <div class="header">
                    <h2 class="text-left" style="margin-top: 10px !important;color: #000000">
                   <b>Manage Agent</b>
                    </h2>
                    <h2 class="text-right">
                        @isset(auth('admin')->user()->role->permission->permission['agent']['add'])
                        <a  href="{{route('agent.create')}}" class="btn btn-primary" >Add New<i class="material-icons">add</i></a>
                        @endisset
                    </h2>
                    {{--<ul class="header-dropdown m-r--5">--}}
                    {{--<li class="dropdown">--}}
                    {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                    {{--<i class="material-icons">more_vert</i>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu pull-right">--}}
                    {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th width="5%">SL</th>
                                <th width="10%">Photo</th>
                                <th width="20%">Company Name</th>
                                <th width="20%">Address</th>
                                <th width="10%">Email</th>
                                <th width="5%">Status</th>
                                <th width="20%">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($agents as $agent)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>

                                        <img src="{{asset('uploads/agent/'.$agent->image)}}" alt="{{$agent->name}}" width="50">
                                    </td>
                                    <td>{{$agent->c_name}}</td>
                                    <td>{{$agent->address_1}}</td>
                                    <td>{{$agent->email}}</td>
                                    <td> <a href="{{route('agent.activate',$agent->id)}}" class="btn @if($agent->status=='0')btn-info @else btn-success @endif">
                                            @if($agent->status=='0')
                                                Active
                                                @else
                                               Activated
                                            @endif
                                        </a>
                                    </td>



                                    <td>



                                        <form action="{{route('agent.destroy',$agent->id)}}" method="POST">
                                            @method('DELETE')
                                            @csrf

                                            {{--<a href="{{route('agent.show',$agent->id)}}" class="btn btn-info">View</a>--}}
                                            @isset(auth('admin')->user()->role->permission->permission['agent']['edit'])
                                            <a href="{{route('agent.edit',$agent->id)}}" class="btn btn-warning">Edit</a>
                                            @endisset
                                            @isset(auth('admin')->user()->role->permission->permission['agent']['delete'])
                                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger">Delete</button>
                                            @endisset
                                        </form>

                                    </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>


@endpush
