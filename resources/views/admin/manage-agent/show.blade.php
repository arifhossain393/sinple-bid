{{--@extends('layout.backends.app')--}}

{{--@section('title','Property')--}}

{{--@push('css')--}}

    {{--<style>--}}
        {{--.employee-pic{--}}
            {{--width: 60px !important;--}}
            {{--height: 60px !important;--}}
            {{--margin-left: 0px !important;--}}
            {{--/* text-align: center; */--}}
            {{--/* vertical-align: middle; */--}}
            {{--margin: 0 auto !important;--}}
            {{--display: block;--}}
            {{--border-radius: 50%;--}}
        {{--}--}}
        {{--td {--}}
            {{--color: #000 !important;--}}
        {{--}--}}
        {{--th {--}}

            {{--color: #000 !important;--}}
        {{--}--}}
    {{--</style>--}}
{{--@endpush--}}

{{--@section('content')--}}

    {{--<!-- #END# Basic Examples -->--}}
    {{--<!-- Exportable Table -->--}}
    {{--<div class="row clearfix">--}}
        {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}
            {{--<div class="card">--}}
                {{--@if(session('successMsg'))--}}
                    {{--<div class="alert alert-success">--}}


                        {{--<button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none';">×</button>--}}
                        {{--<span>--}}
                                    {{--<b>Success - </b>{{session('successMsg')}}</span>--}}
                    {{--</div>--}}

                {{--@endif--}}
                {{--<div class="header">--}}
                    {{--<h2 class="text-left">--}}
                    {{--Property Info--}}
                    {{--</h2>--}}
                    {{--<h2 class="text-right">--}}
                        {{--<a  href="{{route('property.create')}}" class="btn btn-primary" href="">Add New   <i class="material-icons">add</i></a>--}}
                    {{--</h2>--}}
                    {{--<ul class="header-dropdown m-r--5">--}}
                    {{--<li class="dropdown">--}}
                    {{--<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                    {{--<i class="material-icons">more_vert</i>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu pull-right">--}}
                    {{--<li><a href="javascript:void(0);">Action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Another action</a></li>--}}
                    {{--<li><a href="javascript:void(0);">Something else here</a></li>--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                {{--<div class="body">--}}
                    {{--<div class="table-responsive">--}}
                        {{--<table class="table table-bordered table-striped table-hover dataTable js-exportable">--}}
                            {{--<thead>--}}
                            {{--<tr>--}}
                                {{--<th width="5%">SL</th>--}}
                                {{--<th width="10%">Photo</th>--}}
                                {{--<th width="10%">Property Name</th>--}}
                                {{--<th width="20%">Address</th>--}}
                                {{--<th width="10%">Price</th>--}}
                                {{--<th width="10%">Type </th>--}}
                                {{--<th width="10%">Date</th>--}}
                                {{--<th width="25%">Action</th>--}}
                            {{--</tr>--}}
                            {{--</thead>--}}

                            {{--<tbody>--}}
                            {{--@foreach($properties as $property)--}}
                                {{--<tr>--}}
                                    {{--<td>{{$loop->iteration}}</td>--}}
                                    {{--<td>--}}
                                        {{--<img src="{{asset('uploads/'.$property->image[0])}}" alt="{{$property->name}}" width="50">--}}
                                    {{--</td>--}}
                                    {{--<td>{{$property->name}}</td>--}}
                                    {{--<td>{{$property->address}}</td>--}}
                                    {{--<td>{{env('APP_CURRENCY')}}{{$property->price}}</td>--}}
                                    {{--<td>--}}
                                        {{--@if($property->type===1)--}}
                                            {{--Sale--}}
                                        {{--@else--}}
                                            {{--Rent--}}
                                        {{--@endif--}}
                                    {{--</td>--}}
                                    {{--<td>{{\Carbon\Carbon::parse($property->created_at)->format('d/m/Y')}}</td>--}}
                                    {{--<td>--}}

                                        {{--<form action="{{route('property.destroy',$property->id)}}" method="POST">--}}
                                            {{--@method('DELETE')--}}
                                            {{--@csrf--}}
                                            {{--<a href="{{route('property.show',$property->id)}}" class="btn btn-info">View</a>--}}
                                            {{--<a href="{{route('property.edit',$property->id)}}" class="btn btn-warning">Edit</a>--}}
                                            {{--<button type="submit" class="btn btn-danger">Delete</button>--}}
                                        {{--</form>--}}

                                    {{--</td>--}}
                                {{--</tr>--}}
                            {{--@endforeach--}}

                            {{--</tbody>--}}
                        {{--</table>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!-- #END# Exportable Table -->--}}

{{--@endsection--}}

{{--@push('js')--}}
    {{--<!-- Jquery DataTable Plugin Js -->--}}
    {{--<script src="{{ asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>--}}
    {{--<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>--}}
    {{--<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>--}}
    {{--<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>--}}
    {{--<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>--}}
    {{--<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>--}}
    {{--<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>--}}
    {{--<script src="{{ asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>--}}


{{--@endpush--}}
