@extends('layout.backend.app')

@section('title','Add Agent')

@push('css')
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    {{--<link href="{{asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" />--}}

    <style>

        .col-lg-10 {

            margin-left: 96px !important;
        }
        .form-group .form-line .form-label {

            color: #000000 !important;

        }
        .fallback {
            margin-top: 15px !important;
        }
        .form-control {
            color: #000 !important;
        }
        .card .body {

            color: #000 !important;
        }
    </style>
@endpush

@section('content')
    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" >
            <div class="card">

                <div class="header">
                    <h2 class="card-inside-title"><b>Add Agent</b></h2>
                    {{--                            <ul class="header-dropdown m-r--5">--}}
                    {{--                                <li class="dropdown">--}}
                    {{--                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                    {{--                                        <i class="material-icons">more_vert</i>--}}
                    {{--                                    </a>--}}
                    {{--                                    <ul class="dropdown-menu pull-right">--}}
                    {{--                                        <li><a href="javascript:void(0);">Action</a></li>--}}
                    {{--                                        <li><a href="javascript:void(0);">Another action</a></li>--}}
                    {{--                                        <li><a href="javascript:void(0);">Something else here</a></li>--}}
                    {{--                                    </ul>--}}
                    {{--                                </li>--}}
                    {{--                            </ul>--}}
                </div>
                <div class="body">
                    <form id="form_validation" method="post" action="{{route('agent.store')}}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" value="1" name="confirm_status">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="c_name" >
                                @error('c_name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror

                                <label class="form-label">Company Name:</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="f_name" >
                                @error('f_name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror

                                <label class="form-label">Director First Name:</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="l_name" >
                                @error('l_name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror

                                <label class="form-label">Director Last Name:</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="address_1" >
                                <label class="form-label">Address 1:</label>
                                @error('address_1')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="address_2" >
                                <label class="form-label">Address 2:</label>

                            </div>
                        </div>



                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="city"  >
                                <label class="form-label">City:</label>
                                @error('city')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input  type="text" class="form-control" name="postcode" >
                                <label class="form-label">Post Code:</label>
                                @error('postcode')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <select class="form-line" name="country">
                                <option value="">*Country</option>
                                @foreach($countries as $country)

                                    <option value="{{$country->id}}">{{$country->name}}</option>

                                @endforeach
                                @error('country')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </select>
                        </div>

                        <div class="form-group form-float">
                            <select class="form-line" name="county">
                                <option value="">*County</option>
                                @foreach($county as $coun)

                                    <option value="{{$coun->id}}">{{$coun->county}}</option>

                                @endforeach
                                @error('county')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </select>
                        </div>



                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="branch"  >
                                <label class="form-label ">No. of Branches:</label>
                                @error('branch')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="email" class="form-control" name="email"  >
                                <label class="form-label">E-mail:</label>
                                @error('email')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="phone"  >
                                <label class="form-label">Phone Number:</label>
                                @error('phone')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control" name="password"  >
                                <label class="form-label">Password:</label>
                                @error('password')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control" name="confirm_password"  >
                                <label class="form-label">Confirm Password:</label>
                                @error('confirm_password')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>





                        <div class="fallback">
                            <label class="form-label form-label-text "><b>Image:</b></label>
                            <input name="image" type="file" />
                            @error('image')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>


                        {{--<div class="dropzone"  id="frmFileUpload" >--}}
                        {{--<div class="dz-message">--}}
                        {{--<div class="drag-icon-cph">--}}
                        {{--<i class="material-icons">touch_app</i>--}}
                        {{--</div>--}}
                        {{--<h3>Drop files here or click to upload.</h3>--}}
                        {{--<em>(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</em>--}}
                        {{--</div>--}}
                        {{--<div class="fallback">--}}
                        {{--<input name="image" type="file" multiple />--}}
                        {{--</div>--}}
                        {{--</div>--}}


                        <h2 class="card-inside-title">Description:</h2>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea  rows="4" name="description" class="form-control no-resize" placeholder="Please type what you want..."></textarea>
                                        @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        <a class="btn btn-danger waves-effect" href="{{route('agent.index')}}">
                            <span>Back</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Validation -->
    <!-- Advanced Validation -->

    <!-- #END# Advanced Validation -->
    <!-- Validation Stats -->

    <!-- #END# Validation Stats -->
@endsection

@push('js')
    <!-- Bootstrap Material Datetime Picker Plugin Js -->

    <script src="{{asset('assets/backend/js/pages/forms/form-validation.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/forms/basic-form-elements.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/forms/advanced-form-elements.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/dropzone/dropzone.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

    <script src="{{asset('assets/backend/plugins/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        });
    </script>

    <script>
        $('#start_date').bootstrapMaterialDatePicker();
        $('#end_date').bootstrapMaterialDatePicker();

    </script>

@endpush
