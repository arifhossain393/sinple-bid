@extends('layout.backend.app')
@section('title','edit user')
@push('css')
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />
    <style>

        .col-lg-10 {

            margin-left: 96px !important;
        }
        .form-group .form-line .form-label {

            color: #131212 !important;

        }
        .fallback {
            margin-top: 15px !important;
        }
        .form-control {
            color: #000 !important;
        }
        .card .body {

            color: #000 !important;
        }
    </style>
@endpush

@section('content')
    <div class="row clearfix">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" >
            <div class="card">

                <div class="header">
                    <h2 class="card-inside-title"><b>Edit User</b></h2>

                </div>
                <div class="body">
                    <form id="form_validation" method="post" action="{{route('adminRole.update',$admin->id)}}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="name" value="{{$admin->name}}" >
                                @error('name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                                <label class="form-label"> Name:</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="email" value="{{$admin->email}}" >
                                @error('email')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                                <label class="form-label"> Email:</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" value="{{old('password')}}" >
                                @error('password')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                                <label class="form-label"> Password:</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control" name="confirm_password"  >
                                <label class="form-label">Confirm Password:</label>
                                @error('confirm_password')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <select class="form-line" name="role_id">
                                    <option value="">Role</option>
                                    @foreach($roles as $role)
                                        @if($role->id==$admin->role_id)
                                            <option value="{{$role->id}}"selected>{{$role->name}}</option>
                                        @else
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                        @endif
                                    @endforeach

                                    @error('role_id')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </select>
                            </div>
                        </div>



                        <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        <a class="btn btn-danger waves-effect" href="{{route('adminRole.index')}}">
                            <span>Back</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <!-- Bootstrap Material Datetime Picker Plugin Js -->

    <script src="{{asset('assets/backend/js/pages/forms/form-validation.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/forms/basic-form-elements.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/forms/advanced-form-elements.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/dropzone/dropzone.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

    <script src="{{asset('assets/backend/plugins/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        });
    </script>

    <script>
        $('#start_date').bootstrapMaterialDatePicker();
        $('#end_date').bootstrapMaterialDatePicker();

    </script>

@endpush









