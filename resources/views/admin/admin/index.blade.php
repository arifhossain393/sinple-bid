@extends('layout.backend.app')

@section('title','user')

@push('css')
    <style>

        td {
            color: #000 !important;
        }
        th {

            color: #000 !important;
        }
    </style>
@endpush

@section('content')

    <!-- #END# Basic Examples -->
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                @if(session('successMsg'))
                    <div class="alert alert-success">


                        <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none';">×</button>
                        <span>
                                    <b>Success - </b>{{session('successMsg')}}</span>
                    </div>

                @endif
                <div class="header">
                    <h2 class="text-left">
                        <b>Manage User</b>
                    </h2>
                    <h2 class="text-right">
                        @isset(auth('admin')->user()->role->permission->permission['user']['add'])
                        <a  href="{{route('adminRole.create')}}" class="btn btn-primary" href="">Add New   <i class="material-icons">add</i></a>
                        @endisset
                    </h2>

                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th >SL</th>
                                <th >Name</th>
                                <th >Email</th>
                                <th >Role Type</th>
                                <th >Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($admins as $admin)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$admin->name}}</td>
                                <td>{{$admin->email}}</td>
                                <td>{{$admin->role->name}}</td>
                                <td>
                                    <form action="{{route('adminRole.destroy',$admin->id)}}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        @isset(auth('admin')->user()->role->permission->permission['user']['edit'])
                                        <a href="{{route('adminRole.edit',$admin->id)}}" class="btn btn-warning">Edit</a>
                                        @endisset

                                        @isset(auth('admin')->user()->role->permission->permission['user']['delete'])
                                            @if($admin->role->id!=auth('admin')->user()->role_id)
                                        <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger">Delete</button>
                                                @else
                                                Not Permitted
                                            @endif
                                        @endisset

                                    </form>

                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('js')

@endpush
