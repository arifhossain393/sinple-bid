@extends('layout.backend.app')

@section('title','Acounts Settings')

@push('css')
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    {{--<link href="{{asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" />--}}

    <style>

        .col-lg-10 {

            margin-left: 96px !important;
        }
        .form-group .form-line .form-label {

            color: #131212 !important;

        }
        .fallback {
            margin-top: 15px !important;
        }
        .list-group-item {
            font-size: 16px !important;
            color: #000 !important;
        }
        p {
            margin: 0 0 10px;
            color: #000 !important;
            font-size: 16px !important;
        }
        h4 {
            color: #000 !important;
        }
        .panel-post .panel-heading .media .media-body h4 a {
            color: #000 !important;
        }
        .badge {

            min-width: 129px !important;

        }
        element {

            font-size: 14px !important;

        }
        span {
            font-size: 14px !important;
        }
        .form-group {
            width: 55% !important;

        }
        input {
            margin-top: 5px !important;
        }
        .card .body {

            color: #0b0a0a !important;
        }
        .btn.btn-danger {
            margin-left: -74px !important;
        }
        b, strong {
            margin-left: 19px !important;
        }
        .input-style{margin-top: -1px !important;}
        .form-control {

            color: #000 !important;

        }
    </style>
@endpush

@section('content')

    <div class="row clearfix">

        <div class="col-xs-12 col-sm-12">
            <div class="card">
                <div class="body">
                    <div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="@if(!session()->get('active')) active @endif"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="material-icons">face</i> UPDATE PROFILE</a></li>
                            <li class="@if(session()->get('active')) active @endif" role="presentation"><a href="#profile_settings" aria-controls="settings" role="tab" data-toggle="tab"><i class="material-icons">change_history</i> CHANGE PASSWORD</a></li>

                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in @if(!session()->get('active')) active @endif" id="home">
                                <form class="form-horizontal" method="POST" action="{{ route('admin.profile.update',$admin->id) }}"  enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label for="name" class="col-sm-2 control-label" style="margin-left: 2px !important;">Name:</label>
                                        <div class="col-sm-9">
                                            <div class="form-line">
                                                <input type="text" class="form-control input-style"  name="name" placeholder="Name" value="{{Auth::guard('admin')->user()->name}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">Email:</label>
                                        <div class="col-sm-9">
                                            <div class="form-line">
                                                <input type="email" class="form-control input-style" id="NewPassword" name="email" placeholder="Enter Your Email" value="{{Auth::guard('admin')->user()->email}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-9">
                                            <button type="submit" class="btn btn-danger">SUBMIT</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in @if(session()->get('active')) active @endif " id="profile_settings">
                                <form class="form-horizontal"method="POST" action="{{ route('admin.password.update',$admin->id) }}"  enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label for="OldPassword" class="col-sm-4 control-label">Old Password:</label>
                                        <div class="col-sm-8">
                                            <div class="form-line">
                                                <input type="password" class="form-control input-style"  name="old_password" placeholder="Old Password" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="NewPassword" class="col-sm-4 control-label">New Password:</label>
                                        <div class="col-sm-8">
                                            <div class="form-line">
                                                <input type="password" class="form-control input-style"  name="password" placeholder="New Password" >

                                            </div>
                                            @error('password')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="NewPasswordConfirm" class="col-sm-4 control-label">Confirm Password:</label>
                                        <div class="col-sm-8">
                                            <div class="form-line">
                                                <input type="password" class="form-control input-style"  name="confirm_password" placeholder="New Password (Confirm)" >

                                            </div>
                                            @error('confirm_password')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-danger">SUBMIT</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>



                        {{--<a class="btn btn-danger waves-effect" href="">--}}
                        {{--<span>Back</span>--}}
                        {{--</a>--}}
                    </div>
                </div>
            </div>
        </div>

    @endsection

    @push('js')
        <!-- Bootstrap Material Datetime Picker Plugin Js -->

            <script src="{{asset('assets/backend/js/pages/forms/form-validation.js')}}"></script>
            <script src="{{asset('assets/backend/js/pages/forms/basic-form-elements.js')}}"></script>
            <script src="{{asset('assets/backend/js/pages/forms/advanced-form-elements.js')}}"></script>
            <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
            <script src="{{asset('assets/backend/plugins/dropzone/dropzone.js')}}"></script>
            <script src="{{asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

            <script src="{{asset('assets/backend/plugins/ckeditor/ckeditor.js')}}"></script>
            <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>

            <script>
                tinymce.init({
                    selector: 'textarea'
                });
            </script>

            <script>
                $('#start_date').bootstrapMaterialDatePicker();
                $('#end_date').bootstrapMaterialDatePicker();

            </script>

    @endpush
