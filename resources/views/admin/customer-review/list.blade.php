@extends('layout.backend.app')

@section('title','Review-list')

@push('css')

@endpush

@section('content')

    <!-- #END# Basic Examples -->
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                @if(session('successMsg'))
                    <div class="alert alert-success">


                        <button type="button" aria-hidden="true" class="close" onclick="this.parentElement.style.display='none';">×</button>
                        <span>
                                    <b>Success - </b>{{session('successMsg')}}</span>
                    </div>

                @endif
                <div class="header">
                    <h2 class="text-left">
                    <b>Customer Review</b>
                    </h2>


                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                            <tr>
                                <th >SL</th>
                                <th >Name</th>
                                <th >Review</th>
                                <th >Status</th>
                                <th >Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($review as $rev)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$rev->name}}</td>
                                    <td> {!! $rev['review'] !!}</td>
                                    <td><a href="{{route('review.activation',$rev->id)}}" class="btn @if($rev->status=='0')btn-info @else btn-success @endif">
                                        @if($rev->status=='0')
                                            Approve
                                        @else
                                            Approved
                                        @endif
                                    </td>
                                    <td>

                                        <form action="{{route('review.destroy',$rev->id)}}" method="POST">
                                            @method('DELETE')
                                            @csrf
                                            @isset(auth('admin')->user()->role->permission->permission['review']['delete'])
                                                <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger">Delete</button>
                                            @endisset

                                        </form>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('js')

@endpush
