@extends('layouts.frontend.app')

@section('title','Agent Sign Up')

@push('css')
<style>
    .contact-section {
        background: #ecebea;
    }
    .reg.form-content-box{
        max-width: 800px;
    }
</style>
@endpush

@section('content')
    <!-- Contact section start -->
    <div class="contact-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form content box start -->
                    <div class="reg form-content-box">
                        <!-- details -->
                        <div class="details">
                            <!-- Logo-->
                            <a href="index.html">
                                <img src="{{asset('assets/newfrontend/img/logos/black-logo.png') }}" class="cm-logo" alt="black-logo">
                            </a>
                            <!-- Name -->
                            <h3>Agent Registration</h3>
                            <!-- Form start-->
                            <form id="form_validation" method="post" action="{{route('agent-sign-up.store')}}" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="text" name="f_name" class="input-text form-control" placeholder="First Name" value="{{old('f_name')}}" />
                                        @error('f_name')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="l_name" class="input-text form-control" placeholder="Last Name" value="{{old('l_name')}}" />
                                        @error('l_name')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="text" name="address_1" class="input-text form-control" placeholder="Address 1" value="{{old('address_1')}}" />
                                        @error('address_1')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="address_2" class="input-text form-control" placeholder="Address 2" value="{{old('address_2')}}" />
                                        @error('address_2')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="text" name="city" class="input-text form-control" placeholder="City" value="{{old('city')}}" />
                                        @error('city')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <select name="county" class="input-text form-control" id="sel1">
                                            <option value="">County</option>
                                            @foreach($counties as $county)
                                                <option value="{{$county->id}}">{{$county->county}}</option>
                                            @endforeach
                                        </select>

                                        @error('county')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="text" name="postcode" class="input-text form-control" placeholder="Post Code" value="{{old('postcode')}}" />
                                        @error('postcode')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <select name="country" class="input-text form-control" id="sel1">
                                            <option value="">Country</option>
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('country')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="text" name="c_name" class="input-text form-control" placeholder="Full Company Name" value="{{old('c_name')}}" />
                                        @error('c_name')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="branch" class="input-text form-control" placeholder="No of Branches" value="{{old('branch')}}" />
                                        @error('branch')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="text" name="phone" class="input-text form-control" placeholder="Phone number" value="{{old('phone')}}" />
                                        @error('phone')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <input type="email" name="email" class="input-text form-control" placeholder="Your Email *" value="{{old('email')}}" />
                                        @error('email')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="password" name="password" class="input-text form-control" placeholder="Your Password *" value="" />
                                        @error('password')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <input type="password" name="confirm_password" class="input-text form-control" placeholder="Confirm Password *" value="" />
                                        @error('confirm_password')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input class="input-text" type="file" name="image" value="Upload your Company logo" />
                                    @error('image')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="6" cols="10" name="description" placeholder="About Your Self...">{{old('description')}}</textarea>
                                    @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror

                                </div>

                                <div class="form-group mb-0">
                                    <button type="submit" class="btn-md button-theme btn-block">Signup</button>
                                </div>
                            </form>
                        </div>
                        <!-- Footer -->
                        <div class="footer">
                            <span>Already a member? <a href="#">Login here</a></span>
                        </div>
                    </div>
                    <!-- Form content box end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Contact section end -->
@endsection

@push('js')

@endpush
