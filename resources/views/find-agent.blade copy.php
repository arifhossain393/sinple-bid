@extends('layouts.frontend.app')

@section('title','Find Agent')

@push('css')

@endpush

@section('content')
<h2>Find Agent</h2>
<section id="our-agent">
    <div class="postcode-search-box">
        <div class="container">

            <!--======= FORM SECTION =========-->
            <div class="find-sec">
                <ul class="row">
                    <form action="{{route('find.agent.list')}}" method="GET">
                        <!--======= FORM =========-->
                        <li class="col-sm-3">
                            <label>Agent Name</label>
                            <input type="text" name="agent_name" placeholder="Enter Agent Name">
                        </li>

                        <!--======= FORM =========-->


                        <li class="col-sm-3">
                            <label>Post code</label>

                            <input class="form-control sr-pb" id="searchTextField" type="text" size="50" placeholder="Enter Post Code/Area" autocomplete="on" runat="server" />
                            <input type="hidden" id="address" name="address" />
                            <input type="hidden" id="latitude" name="latitude" />
                            <input type="hidden" id="longitude" name="longitude" />
                        </li>

                        <!--======= FORM =========-->
                        <li class="col-sm-3">
                            <label>This Distance</label>
                            <select class="" name="distance">
                                <option value="1">1 Miles</option>
                                <option value="3">3 Miles</option>
                                @for($i=5;$i<=40;$i=$i+5) <option value="{{$i}}">{{$i}} Miles</option>
                                    @endfor
                            </select>
                        </li>

                        <li class="col-sm-3">
                            <label></label>
                            <button type="submit" class="btn agent_btn">Search</button>
                        </li>
                    </form>
                </ul>
            </div>
        </div>
    </div>
</section>



<br><br><br>


<section id="team">
    <div class="container">
        <!--======= TITTLE =========-->
        <div class="tittle">
            <h3>Find a Buyer or Renter who can Perform on click 2 Bid</h3>
            <br>

        </div>
        <div class="row">
            <div class="col-md-6">

                <!--======= TEAM ROW =========-->
                <ul class="row">

                    <!--======= TEAM =========-->
                    <li class="col-sm-6">
                        <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-1.jpg')}}" alt="">
                            <div class="team-over">
                                <!--======= SOCIAL ICON =========-->
                                <ul class="social_icons animated-6s fadeInUp">
                                    <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                    <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                    <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                    <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>

                            <!--======= TEAM DETAILS =========-->
                            <div class="team-detail">
                                <h6>David Martin</h6>
                                <p>Founder</p>
                            </div>
                        </div>
                    </li>

                    <!--======= TEAM =========-->
                    <li class="col-sm-6">
                        <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-2.jpg')}}" alt="">
                            <div class="team-over">
                                <!--======= SOCIAL ICON =========-->
                                <ul class="social_icons animated-6s fadeInUp">
                                    <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                    <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                    <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                    <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>

                            <!--======= TEAM DETAILS =========-->
                            <div class="team-detail">
                                <h6>Hendrick jack </h6>
                                <p>co-Founder</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">

                <!--======= TEAM ROW =========-->
                <ul class="row">

                    <!--======= TEAM =========-->
                    <li class="col-sm-6">
                        <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-3.jpg')}}" alt="">
                            <div class="team-over">
                                <!--======= SOCIAL ICON =========-->
                                <ul class="social_icons animated-6s fadeInUp">
                                    <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                    <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                    <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                    <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>

                            <!--======= TEAM DETAILS =========-->
                            <div class="team-detail">
                                <h6>charles edward </h6>
                                <p>team leader </p>
                            </div>
                        </div>
                    </li>

                    <!--======= TEAM =========-->
                    <li class="col-sm-6">
                        <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-4.jpg')}}" alt="">
                            <div class="team-over">
                                <!--======= SOCIAL ICON =========-->
                                <ul class="social_icons animated-6s fadeInUp">
                                    <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                    <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                    <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                    <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>

                            <!--======= TEAM DETAILS =========-->
                            <div class="team-detail">
                                <h6>jessica wevins </h6>
                                <p>team leader</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endsection

@push('js')
<script type="text/javascript">
    var options = {
        componentRestrictions: {
            country: "uk"
        }
    };

    function initialize() {
        var input = document.getElementById('searchTextField');
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            document.getElementById('address').value = place.name;
            document.getElementById('latitude').value = place.geometry.location.lat();
            document.getElementById('longitude').value = place.geometry.location.lng();
            //alert("This function is working!");
            //alert(place.name);
            // alert(place.address_components[0].long_name);
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize)
</script>
@endpush
