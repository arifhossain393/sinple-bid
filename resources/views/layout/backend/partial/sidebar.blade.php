<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            <img src="{{asset('assets/backend/images/user.png')}}"  width="80" height="80"alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::guard('admin')->user()->name}}</div>
            <div class="email">{{Auth::guard('admin')->user()->email}}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>

                    <li role="separator" class="divider"></li>
                    <li>
                        <a class="dropdown-item" href="{{ route('admin.auth.logout') }}">

                            <i class="material-icons">input</i><span>Logout</span>
                        </a>

                        {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                            {{--@csrf--}}
                        {{--</form>--}}

                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>


            @if(Request::is('admin*'))

                <li class="{{Request::is('admin') ? 'active': ''}}">
                    <a href="{{route('admin.dashboard')}}">
                        <i class="material-icons">dashboard</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                {{--<li class="{{Request::is('admin/tag') ? 'active': ''}}">--}}
                    {{--<a href="{{route('admin.tag.index')}}">--}}
                        {{--<i class="material-icons">label</i>--}}
                        {{--<span>Tag</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
{{--                <li>--}}
{{--                    <a href="pages/typography.html">--}}
{{--                        <i class="material-icons">text_fields</i>--}}
{{--                        <span>Typography</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="pages/helper-classes.html">--}}
{{--                        <i class="material-icons">layers</i>--}}
{{--                        <span>Helper Classes</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li>--}}

{{--                <li>--}}
{{--                    <a href="javascript:void(0);" class="menu-toggle">--}}
{{--                        <i class="material-icons">swap_calls</i>--}}
{{--                        <span>User Interface (UI)</span>--}}
{{--                    </a>--}}
{{--                    <ul class="ml-menu">--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/alerts.html">Alerts</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/animations.html">Animations</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/badges.html">Badges</a>--}}
{{--                        </li>--}}

{{--                        <li>--}}
{{--                            <a href="pages/ui/breadcrumbs.html">Breadcrumbs</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/buttons.html">Buttons</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/collapse.html">Collapse</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/colors.html">Colors</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/dialogs.html">Dialogs</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/icons.html">Icons</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/labels.html">Labels</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/list-group.html">List Group</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/media-object.html">Media Object</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/modals.html">Modals</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/notifications.html">Notifications</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/pagination.html">Pagination</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/preloaders.html">Preloaders</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/progressbars.html">Progress Bars</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/range-sliders.html">Range Sliders</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/sortable-nestable.html">Sortable & Nestable</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/tabs.html">Tabs</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/thumbnails.html">Thumbnails</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/tooltips-popovers.html">Tooltips & Popovers</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/ui/waves.html">Waves</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="javascript:void(0);" class="menu-toggle">--}}
{{--                        <i class="material-icons">assignment</i>--}}
{{--                        <span>Forms</span>--}}
{{--                    </a>--}}
{{--                    <ul class="ml-menu">--}}
{{--                        <li>--}}
{{--                            <a href="pages/forms/basic-form-elements.html">Basic Form Elements</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/forms/advanced-form-elements.html">Advanced Form Elements</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/forms/form-examples.html">Form Examples</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/forms/form-validation.html">Form Validation</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/forms/form-wizard.html">Form Wizard</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/forms/editors.html">Editors</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="javascript:void(0);" class="menu-toggle">--}}
{{--                        <i class="material-icons">view_list</i>--}}
{{--                        <span>Tables</span>--}}
{{--                    </a>--}}
{{--                    <ul class="ml-menu">--}}
{{--                        <li>--}}
{{--                            <a href="pages/tables/normal-tables.html">Normal Tables</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/tables/jquery-datatable.html">Jquery Datatables</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/tables/editable-table.html">Editable Tables</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="javascript:void(0);" class="menu-toggle">--}}
{{--                        <i class="material-icons">perm_media</i>--}}
{{--                        <span>Medias</span>--}}
{{--                    </a>--}}
{{--                    <ul class="ml-menu">--}}
{{--                        <li>--}}
{{--                            <a href="pages/medias/image-gallery.html">Image Gallery</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/medias/carousel.html">Carousel</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="javascript:void(0);" class="menu-toggle">--}}
{{--                        <i class="material-icons">pie_chart</i>--}}
{{--                        <span>Charts</span>--}}
{{--                    </a>--}}
{{--                    <ul class="ml-menu">--}}
{{--                        <li>--}}
{{--                            <a href="pages/charts/morris.html">Morris</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/charts/flot.html">Flot</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/charts/chartjs.html">ChartJS</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/charts/sparkline.html">Sparkline</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/charts/jquery-knob.html">Jquery Knob</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="javascript:void(0);" class="menu-toggle">--}}
{{--                        <i class="material-icons">content_copy</i>--}}
{{--                        <span>Example Pages</span>--}}
{{--                    </a>--}}
{{--                    <ul class="ml-menu">--}}
{{--                        <li>--}}
{{--                            <a href="pages/examples/profile.html">Profile</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/examples/sign-in.html">Sign In</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/examples/sign-up.html">Sign Up</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/examples/forgot-password.html">Forgot Password</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/examples/blank.html">Blank Page</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/examples/404.html">404 - Not Found</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/examples/500.html">500 - Server Error</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="javascript:void(0);" class="menu-toggle">--}}
{{--                        <i class="material-icons">map</i>--}}
{{--                        <span>Maps</span>--}}
{{--                    </a>--}}
{{--                    <ul class="ml-menu">--}}
{{--                        <li>--}}
{{--                            <a href="pages/maps/google.html">Google Map</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/maps/yandex.html">YandexMap</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="pages/maps/jvectormap.html">jVectorMap</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="javascript:void(0);" class="menu-toggle">--}}
{{--                        <i class="material-icons">trending_down</i>--}}
{{--                        <span>Multi Level Menu</span>--}}
{{--                    </a>--}}
{{--                    <ul class="ml-menu">--}}
{{--                        <li>--}}
{{--                            <a href="javascript:void(0);">--}}
{{--                                <span>Menu Item</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="javascript:void(0);">--}}
{{--                                <span>Menu Item - 2</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="javascript:void(0);" class="menu-toggle">--}}
{{--                                <span>Level - 2</span>--}}
{{--                            </a>--}}
{{--                            <ul class="ml-menu">--}}
{{--                                <li>--}}
{{--                                    <a href="javascript:void(0);">--}}
{{--                                        <span>Menu Item</span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="javascript:void(0);" class="menu-toggle">--}}
{{--                                        <span>Level - 3</span>--}}
{{--                                    </a>--}}
{{--                                    <ul class="ml-menu">--}}
{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0);">--}}
{{--                                                <span>Level - 4</span>--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
                <li class="{{ Request::is('admin/agent*') ? 'active' : '' }}">
                    <a href="{{route('agent.index')}}">
                        <i class="material-icons">people</i>
                        <span>Agents</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/customer') ? 'active' : '' }}">
                    <a href="{{route('customer.index')}}">
                        <i class="material-icons">people_outline</i>
                        <span>Customers</span>
                    </a>
                </li>

                <li class="{{ Route::is('customer-review.index') ? 'active' : '' }}">
                    <a href="{{route('customer-review.index')}}">
                        <i class="material-icons">face</i>
                        <span>Customers Review</span>
                    </a>
                </li>
                <li class="{{ Route::is('admin.bid.list') ? 'active' : '' }}">
                    <a href="{{route('admin.bid.list')}}">
                        <i class="material-icons">gavel</i>
                        <span>Bids</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/manage_property*') ? 'active' : '' }}">
                    <a href="{{route('manage_property.index')}}">
                        <i class="material-icons">home</i>
                        <span>Properties</span>
                    </a>
                </li>
                <li class="{{ Route::is('subscription.index') ? 'active' : '' }}">
                    <a href="{{route('subscription.index')}}">
                        <i class="material-icons">subscriptions</i>
                        <span>Package Subscription</span>
                    </a>
                </li>
                {{--<li class="{{ Request::is('admin/admin*') ? 'active' : '' }}">--}}
                    {{--<a href="{{route('Admin.Role.index',Auth::guard('admin')->user()->id)}}">--}}
                        {{--<i class="material-icons">settings</i>--}}
                        {{--<span>User Management</span>--}}
                    {{--</a>--}}
                {{--</li>--}}

                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">widgets</i>
                    <span>User Management</span>
                </a>
                <ul class="ml-menu">
                    <li class="{{ Request::is('admin/role*') ? 'active' : '' }}">
                        <a href="{{route('role.index')}}">
                            <i class="material-icons">gamepad</i>
                            <span>Manage Roles</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('admin/admin*') ? 'active' : '' }}">
                        <a href="{{route('adminRole.index')}}">
                            <i class="material-icons">account_circle</i>
                            <span>Manage Users</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('') ? 'active' : '' }}">
                        <a href="{{route('adminPermission.index')}}">
                            <i class="material-icons">assignment_turned_in</i>
                            <span>Manage Permission</span>
                        </a>
                    </li>
                </ul>
                </li>
                <li class="{{ Request::is('admin/settings*') ? 'active' : '' }}">
                    <a href="{{route('admin.settings',Auth::guard('admin')->user()->id)}}">
                        <i class="material-icons">group</i>
                        <span>My Profile</span>
                    </a>
                </li>


                <li class="header">system</li>
                <li>
                    <a class="dropdown-item" href="{{ route('admin.auth.logout') }}">

                        <i class="material-icons">input</i><span>Logout</span>
                    </a>

                    {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                        {{--@csrf--}}
                    {{--</form>--}}

                </li>

            @endif


        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; 2019<a href="https://gmswebdesign.co.uk">GMS Webdesign.All Rights reserved</a>.
        </div>

    </div>
    <!-- #Footer -->
</aside>
