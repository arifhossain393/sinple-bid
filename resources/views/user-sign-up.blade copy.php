@extends('layouts.frontend.app')

@section('title','User Sign Up')

@push('css')

<style>
    textarea {
        border: 2px solid aliceblue !important;
        padding: 20px !important;
        width: 570px !important;
        resize: both !important;
        overflow: auto !important;
        height: 200px !important;
        background-color: white !important;
    }

    #app {
        background-color: #eaeaea !important;
    }
</style>

@endpush

@section('content')
    <div class="row clearfix">
    <div class="sub-banner">
        <div class="overlay">
            <div class="container">
                <h1>User Register form</h1>
                <ol class="breadcrumb">
                    <li class="pull-left">register form</li>
                    <li><a href="#">Home</a></li>
                    <li class="active">register form</li>
                </ol>
            </div>
        </div>
    </div>

    <!--======= PROPERTIES DETAIL PAGE =========-->
    <div class="container login-container">
        <div class="row">
            <div class="col-md-8 login-form-1 col-md-offset-2">
                <h3>User Registration</h3>
                <form id="form_validation" method="post" action="{{route('user-sign-up.store')}}" enctype="multipart/form-data">
                    @csrf
                <div class="form-group">
                    <input type="text" name="f_name" class="form-control" placeholder=" First Name" value="" />
                    @error('f_name')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="text" name="l_name" class="form-control" placeholder="Last Name" value="" />
                    @error('l_name')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="text" name="email" class="form-control" placeholder=" Email *" value="" />
                    @error('email')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder=" Password *" value="" />
                    @error('password')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="password" name="confirm_password" class="form-control" placeholder="Confirm  Password *" value="" />
                    @error('confirm_password')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <select class="form-control" id="sel1" name="role_id">
                        <option value="">User Type</option>
                        <option value="1">Buyer</option>
                        <option value="2">Tenant</option>
                    </select>
                    @error('role_id')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="text" name="address_1" class="form-control" placeholder="Address 1" value="" />
                    @error('address_1')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="text" name="address_2" class="form-control" placeholder="Address 2" value="" />
                    <!--@error('address_2')-->
                    <!--<span class="text-danger">{{ $message }}</span>-->
                    <!--@enderror-->
                </div>
                <div class="form-group">
                    <input type="text" name="city" class="form-control" placeholder="City" value="" />
                    @error('city')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <select class="form-control" id="sel1" name="country">
                        <option value="">Country</option>
                        @foreach($countries as $country)

                            <option value="{{$country->id}}">{{$country->name}}</option>

                        @endforeach
                        @error('country')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" name="postcode" class="form-control" placeholder="Post Code" value="" />
                    @error('postcode')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <select class="form-control" id="sel1" name="employment_status">
                        <option value="">Employement Status</option>
                        <option value="1">Employed</option>
                        <option value="2">Unemployed</option>

                    </select>
                    <!--@error('employment_status')-->
                    <!--<span class="text-danger">{{ $message }}</span>-->
                    <!--@enderror-->
                </div>
                <input type="file" name="image" value="Upload Company logo" />
                <div class="form-group">
                    <input type="text" name="phone" class="form-control" placeholder="Phone No." value="" />
                    <!--@error('phone')-->
                    <!--<span class="text-danger">{{ $message }}</span>-->
                    <!--@enderror-->
                </div>

                <div class="form-group">
                    <textarea rows="6" cols="50" name="about" placeholder="About Your Self..."></textarea>
                    @error('about')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>






                <div class="form-group">
                    <input type="submit" class="btnSubmit" value="Submit" />
                </div>
                <div class="form-group">
                    <a href="{{route('login')}}" class="btnForgetPwd">Log In</a>
                </div>

                </form>
            </div>

        </div>
    </div>
    </div>




@endsection

@push('js')

@endpush
