@extends('layouts.frontend.app')

@section('title','Sale')

@push('css')

@endpush

@section('content')
    <h2>Search within the UK for property</h2>
    <div class="row">
        <div class="postcode-search-box">
            <div class="container">

                <!--======= FORM SECTION =========-->
                <div class="find-sec">
                    <ul class="row">



                        <li class="col-md-8 col-sm-6" >
                            <label>Post code/Area</label>
                            <input type="text" class="postt"  name="agent name" placeholder="Enter Post Code/Area">
                        </li>


                        <li class="col-md-4 col-sm-6">
                            <label></label>
                            <button type="submit" class="btn agent_btnnn">Search</button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </section>

    <!--======= BANNER =========-->





    <!--======= WHAT WE DO =========-->




    <!--======= CALL US =========-->

    <!-- <section class="call-us">
      <div class="overlay">
        <div class="container">
          <ul class="row">
            <li class="col-sm-6">
              <h4></h4>
              <h6></h6>
            </li>
            <li class="col-sm-4">
              <h1></h1>
            </li>
            <li class="col-sm-2 no-padding"> <a href="11-Register.html" class=""></a> </li>
          </ul>
        </div>
      </div>
    </section> -->

    <br><br><br>


    <section id="team">
        <div class="container">
            <!--======= TITTLE =========-->
            <div class="tittle">
                <h3>Find a Buyer or Renter who can Perform on click 2 Bid</h3>
                <br>
                <p>This time there's no stopping us. Straightnin' the curves. Flatnin' the hills Someday the mountain might get ‘em but the law never will. The weather started getting rough - the tiny ship was tossed.</p>
            </div>
            <div class="row">
                <div class="col-md-6">

                    <!--======= TEAM ROW =========-->
                    <ul class="row">

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-1.jpg')}}" alt="">
                                <div class="">
                                    <!--======= SOCIAL ICON =========-->
                                    <!-- <ul class="social_icons animated-6s fadeInUp">
                                      <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                      <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                      <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                      <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul> -->
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <!--  <div class="team-detail">
                                   <h6>David Martin</h6>
                                   <p>Founder</p>
                                 </div> -->
                            </div>
                        </li>

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-2.jpg')}}" alt="">
                                <div class="">
                                    <!--======= SOCIAL ICON =========-->
                                    <!-- <ul class="social_icons animated-6s fadeInUp">
                                      <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                      <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                      <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                      <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul> -->
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <!--  <div class="team-detail">
                                   <h6>Hendrick jack </h6>
                                   <p>co-Founder</p>
                                 </div> -->
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">

                    <!--======= TEAM ROW =========-->
                    <ul class="row">

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-3.jpg')}}" alt="">
                                <div class="">
                                    <!--======= SOCIAL ICON =========-->
                                    <!-- <ul class="social_icons animated-6s fadeInUp">
                                      <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                      <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                      <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                      <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul> -->
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <!-- <div class="team-detail">
                                  <h6>charles edward </h6>
                                  <p>team leader </p>
                                </div> -->
                            </div>
                        </li>

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-4.jpg')}}" alt="">
                                <div class="">
                                    <!--======= SOCIAL ICON =========-->
                                    <!-- <ul class="social_icons animated-6s fadeInUp">
                                      <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                      <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                      <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                      <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul> -->
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <!--  <div class="team-detail">
                                   <h6>jessica wevins </h6>
                                   <p>team leader</p>
                                 </div> -->
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('js')

@endpush
