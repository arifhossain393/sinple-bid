@extends('layouts.frontend.app')

@section('title','Agent-List')

@push('css')
    <style>
        .pagination {
            display: inline-block;
            width: 100%;
            text-align: center;
            border-radius: 0px;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .pagination .page-item {
            display: inline-block;
            margin-top: 0px;
            margin-bottom: 0px;
        }
        .pagination>.page-item:last-child>.page-link, .pagination>.page-item:last-child>.page-link {
            border-radius: 0px;
        }
        .pagination>.page-item:first-child>.page-link, .pagination>.page-item:first-child>.page-link {
            border-radius: 0px;
        }
        .pagination>.page-item .active>.page-link, .pagination>.page-item .active>.page-link:focus, .pagination>.page-item .active>.page-link:hover, .pagination>.page-item.active>.page-link, .pagination>.page-item.active>.page-link:focus, .pagination>.page-item.active>.page-link:hover {
            background: #4caf50;
        }
        .pagination>.page-item>.page-link:focus, .pagination>.page-item>.page-link:hover, .pagination>.page-item>.page-link:focus, .pagination>.page-item>.page-link:hover {
            background: #4caf50;
            color: #fff;
        }
        .pagination .page-item .page-link {
            display: inline-block;
            border: 1px solid #4caf50 !important;
            color: #4caf50;
            font-size: 19px;
            height: 43px;
            width: 43px;
            line-height: 41px;
            padding: 0px;
            margin-right: 5px;
            font-family: 'Montserrat', sans-serif;
        }
        .pagination .page-item .page-link:hover {
            background: #4caf50;
            color: #fff;
        }
        .pagination>.active>span{color:#fff!important;}
        .center-img {
            display: block !important;
            margin-left: auto;
            margin-right: auto;
            width: 100% !important;
            height: 180px !important;
        }
        .center {
            display: block!important;
            margin-left: auto;
            margin-right: auto;
            width: 50%!important;
        }
        .single-blog{height: 420px;}
        .blog-text,.blog-text>* {
            color:#000;
        }
        .read-more-btn {
            margin-left: 85px !important;
        }

        .read-more-btn {
            background-image: linear-gradient(to right, #1e2d41, #1e2d41) !important;

        }
        .read-more-btn:hover {
            background-image: linear-gradient(to right, #f3c217, #f3c217) !important;
            text-decoration: none;
            color: #fff;
        }
    </style>
@endpush
@section('content')
    <section class="bgg">

        <div class="container">
            <h1 class="text-center">Agents List</h1>
            <p class="text-center"></p>
            <div class="row">

                @foreach($agents as $agent)
                <div class="col-md-4">
                    <div class="single-blog">

                        <p class="blog-meta" style="color: #000"><strong>Tel: </strong>{{$agent->phone}}<span style="color: #000">{{\Carbon\Carbon::parse($agent->created_at)->format('F d, Y')}}</span></p>
                        <img  class="center-img img-thumbnail" src="{{asset('/uploads/agent/'.$agent->image)}}"/>
                        <h2><a href="#">{{$agent->f_name." ". $agent->l_name}}</a></h2>
{{--                        <h6 class="text-center"><strong>Tel: </strong>{{$agent->phone}}</h6>--}}
                       <div class="blog-text" >
                           {!! Str::limit($agent->description,'150','...') !!}
                       </div>
                        <p class="ib "><a class="read-more-btn " href="{{route('agent.details',$agent->id)}}">More about this agent</a></p>

                    </div>
                </div>
                @endforeach

            </div>
            <nav>
                @if(method_exists($agents,'links'))
                    <ul class="pagination">
                        {{ $agents->appends(Illuminate\Support\Facades\Input::except('page'))->links() }}
                    </ul>
                @endif
            </nav>
        </div>
    </section>


@endsection

@push('js')

@endpush
