@extends('layouts.frontend.app')

@section('title','Rent')

@push('css')

@endpush

@section('content')
    <h2>Rent Property</h2>
    <div class="row">
        <div class="postcode-search-box">
            <div class="container mr-112">

                <!--======= FORM SECTION =========-->
                <div class="find-sec">
                    <form action="{{route('search-property')}}">
                    <ul class="row">
                        <li class="col-md-8 col-sm-6" >
                            <label>Post code/Area</label>
                            <input class="form-control sr-pb" id="searchTextField" type="text" size="50" placeholder="Enter Post code" autocomplete="on" runat="server" />
                            <input type="hidden" id="address" name="address" />
                            <input type="hidden" id="latitude" name="latitude" />
                            <input type="hidden" id="longitude" name="longitude" />
                        </li>
                        <input type="hidden" name="type" value="0">

                        <li class="col-md-4 col-sm-6">
                            <label></label>
                            <button  type="submit" class="btn agent_btnnn">Search</button>
                        </li>
                    </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </section>

    <!--======= BANNER =========-->





    <!--======= WHAT WE DO =========-->




    <!--======= CALL US =========-->

    <!-- <section class="call-us">
      <div class="overlay">
        <div class="container">
          <ul class="row">
            <li class="col-sm-6">
              <h4></h4>
              <h6></h6>
            </li>
            <li class="col-sm-4">
              <h1></h1>
            </li>
            <li class="col-sm-2 no-padding"> <a href="11-Register.html" class=""></a> </li>
          </ul>
        </div>
      </div>
    </section> -->

    <br><br><br>


    <section id="team">
        <div class="container">
            <!--======= TITTLE =========-->
            <div class="tittle">
                <h3>Find a Buyer or Renter who can Perform on click 2 Bid</h3>
                <br>

            </div>
            <div class="row pl-2">
                <div class="col-md-6">

                    <!--======= TEAM ROW =========-->
                    <ul class="row">

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-1.jpg')}}" alt="">
                                <div class="">
                                    <!--======= SOCIAL ICON =========-->
                                    <!-- <ul class="social_icons animated-6s fadeInUp">
                                      <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                      <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                      <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                      <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul> -->
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <!--  <div class="team-detail">
                                   <h6>David Martin</h6>
                                   <p>Founder</p>
                                 </div> -->
                            </div>
                        </li>

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-2.jpg')}}" alt="">
                                <div class="">
                                    <!--======= SOCIAL ICON =========-->
                                    <!-- <ul class="social_icons animated-6s fadeInUp">
                                      <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                      <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                      <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                      <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul> -->
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <!--  <div class="team-detail">
                                   <h6>Hendrick jack </h6>
                                   <p>co-Founder</p>
                                 </div> -->
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">

                    <!--======= TEAM ROW =========-->
                    <ul class="row">

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-3.jpg')}}" alt="">
                                <div class="">
                                    <!--======= SOCIAL ICON =========-->
                                    <!-- <ul class="social_icons animated-6s fadeInUp">
                                      <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                      <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                      <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                      <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul> -->
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <!-- <div class="team-detail">
                                  <h6>charles edward </h6>
                                  <p>team leader </p>
                                </div> -->
                            </div>
                        </li>

                        <!--======= TEAM =========-->
                        <li class="col-sm-6">
                            <div class="team"> <img class="img-responsive" src="{{asset('assets/frontend/images/agent-4.jpg')}}" alt="">
                                <div class="">
                                    <!--======= SOCIAL ICON =========-->
                                    <!-- <ul class="social_icons animated-6s fadeInUp">
                                      <li class="facebook"><a href="#."><i class="fa fa-facebook"></i></a></li>
                                      <li class="twitter"><a href="#."><i class="fa fa-twitter"></i></a></li>
                                      <li class="googleplus"><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                      <li class="linkedin"><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                    </ul> -->
                                </div>

                                <!--======= TEAM DETAILS =========-->
                                <!--  <div class="team-detail">
                                   <h6>jessica wevins </h6>
                                   <p>team leader</p>
                                 </div> -->
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('js')
    <script type="text/javascript">

        var options = {
            componentRestrictions: {country: "uk"}
        };
        function initialize() {
            var input = document.getElementById('searchTextField');
            var autocomplete = new google.maps.places.Autocomplete(input,options);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                document.getElementById('address').value = place.name;
                document.getElementById('latitude').value = place.geometry.location.lat();
                document.getElementById('longitude').value = place.geometry.location.lng();
                //alert("This function is working!");
                //alert(place.name);
                // alert(place.address_components[0].long_name);
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize)
    </script>
@endpush
