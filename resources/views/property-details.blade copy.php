@extends('layouts.frontend.app')

@section('title','Property-Details')

@push('css')
    <style>
        .center {
            display: block!important;
            margin-left: auto;
            margin-right: auto;
            width: 50%!important;
        }
        [v-cloak] {
            display: none;
        }
        .bid_amount{
            box-shadow: 0px 5px 6px -5px #00000052; border: 1px solid #1d3c6e;"
        }
        #basic-addon1{
            padding: 5px 14px;
            background-color: #f7f7f7;
            border: 1px solid #f7f7f7;
            border-right: 1px solid #e6e6e6;
            font-weight: bold;
        }

        .bid_amount .form-control{width:100%!important}
        .checkbox input[type=checkbox]{margin-top:-10px!important}
        .blog-text,.blog-text>* {
            color:#000;
            font-family: play;
        }
        span{
            color: #000 !important;
            font-family: play!important;
            font-size: 16px !important;
        }

        .center {

            margin-left: 2% !important;

        }
        b, strong {

            margin-left: 2% !important;
        }
    </style>
@endpush

@section('content')

    <div class="sub-banner">
        <div class="overlay">
            <div class="container">
                <h1>property details</h1>
                <ol class="breadcrumb">
                    <li class="pull-left">property details</li>
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li class="active">property details</li>
                </ol>
            </div>
        </div>
    </div>

    <!--======= PROPERTIES DETAIL PAGE =========-->
    <section class="properti-detsil"  >
        <div class="container">
            <div class="row" style="margin-top: 20px;">

                <!--======= LEFT BAR =========-->
                <div class="col-sm-8">

                    <!--======= THUMB SLIDER =========-->
                    <!--======= THUMB SLIDER =========-->

                    <div class="thumb-slider">

                        <div id="slider" class="flexslider">
                            <ul class="slides">
                                @foreach($property['image'] as $img)
                                    <li> <img style="width:100%;height: 400px"  src="{{asset('uploads/property/'.$img)}}" alt="{{$property->name}}" > </li>
                                @endforeach
                            </ul>
                        </div>

                        <!--======= THUMBS =========-->
                        <div id="carousel" class="flexslider">
                            <ul class="slides">
                                @foreach($property['image'] as $img)
                                    <li> <img class="img-responsive" src="{{asset('uploads/property/'.$img)}}" width="138px"  alt="{{$property->name}}"> </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>


                    <!--======= HOME INNER DETAILS =========-->
                    <ul class="home-in">
                        <li><span><i class="fa fa-home"></i> {{$property['living']}}Living</span></li>
                        <li><span><i class="fa fa-bed"></i> {{$property['bed']}} Bedrooms</span></li>
                        <li><span><i class="fa fa-tty"></i> {{$property['bath']}} Bathrooms</span></li>
                        <li><span> Furnishing : {{$property['furnishing']? 'Yes': 'No'}}</span></li>
                    </ul>

                    <!--======= TITTLE =========-->
                    <h5>{{$property->name}} - {{env('APP_CURRENCY')}}{{$property->price}}</h5>

                    <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
                        <div class="col-sm-12">
                            <ul class="nav nav-pills">
                                <li class="nav active"><a data-toggle="pill" href="#home">Description</a></li>
                                <li class="nav"><a data-toggle="pill" href="#menu1" >Map and nearby</a></li>
                                <li class="nav"><a data-toggle="pill" href="#menu2" >Street view</a></li>
                                <li class="nav"><a data-toggle="pill" href="#menu3" >Documents</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    <p style="color: #000 !important;"><b>Accomodation features:</b></p>
                                    <p style="color: #000 !important;">Date available : {{\Carbon\Carbon::parse($property['end_date'])->format('d/m/Y')}}</p>
                                    <p style="color: #000 !important;">Number of Bedrooms : {{$property['bed']}}</p>
                                    <p style="color: #000 !important;">Bathrooms : {{$property['bath']}} </p>
                                    <p style="color: #000 !important;">Living rooms : {{$property['living']}}</p>
                                    <p style="color: #000 !important;">Outdoor space : {{$property['outdoor']}}</p>
                                    <p style="color: #000 !important;">Furnishing : {{$property['furnishing']? 'Yes': 'No'}}</p>
                                    <p style="color: #000 !important;">Bid Type : {{$property['bid_type']? 'Multiple': 'Single'}}</p>

                                    <p style="color: #000 !important;"><b>Key features:</b></p>
                                    <p style="color: rgb(51, 51, 51); font-family: &quot;play&quot;, Raleway; font-size: 13px; text-align: justify;"></p>
                                    <div class="blog-text">
                                        {!! $property['features'] !!}
                                    </div>
                                    <p style="color: #000 !important;"><b>Full description:</b></p>
                                    <div class="blog-text" id="myDiv">
                                        {!! $property->description !!}
                                    </div>

                                </div>
                                <div id="menu1" class="tab-pane fade ">
                                    <div id="map_canvas" style="width: 100%;height:500px">

                                    </div>
                                </div>

                                <div id="menu2" class="tab-pane fade ">
                                    <iframe
                                        width="100%"
                                        height="500"
                                        frameborder="5"
                                        scrolling="no"
                                        marginheight="0"
                                        marginwidth="0"
                                        src="https://www.google.com/maps/embed/v1/streetview?key=AIzaSyAgtdpYUlacT_ST0_DkfCnGUzucMpKnXfI&location={{$property['latitude']}},{{$property['longitude']}}&heading=1&pitch=1
  &fov=35"
                                    >
                                    </iframe>

                                </div>
                                <div id="menu3" class="tab-pane fade ">
                                    @if($property['document'])
                                        <div class="text-center">
                                            <a class="text-info" href="{{asset('uploads/property/'.$property['document'])}}" >Download Document</a>
                                        </div>
                                    @else
                                        <div class="text-center">
                                            <strong >No documents found </strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>




                </div>
                <!--======= RIGT SIDEBAR =========-->
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-heading" >This property is marketed by:</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-8">
                                    <img class="center" src="{{asset('uploads/agent/'.$property->agent['image'])}}" alt="{{$property->agent['f_name']}}">
                                </div>
                                <div class="col-sm-4">
                                    <p class="text-center" style="font-size: 11px; color:#000000 !important; ">Time remaining</p>
                                    <div class="time-remaining text-center">
                                        @php
                                            //  $days = \Carbon\Carbon::parse($property->start_date)->diffInMinutes(\Carbon\Carbon::parse($property->end_date),$absolute = false);
                                              $minutes = \Carbon\Carbon::parse(\Carbon\Carbon::now())->diffInMinutes(\Carbon\Carbon::parse($property->end_date),$absolute = false);
                                              $day = \Carbon\Carbon::parse(\Carbon\Carbon::now())->diffInDays(\Carbon\Carbon::parse($property->end_date),$absolute = false);
                                             //  echo $minutes;
                                        @endphp
                                        @if($minutes<=0)
                                            Experied
                                        @elseif($minutes==1440)
                                            1 day left
                                        @elseif($minutes>1440)
                                            {{$day}} days left
                                        @elseif($minutes<1440)
                                            0 day left
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <p style="font-size:14px; color: #000 !important;"><b>{{$property->agent['c_name']}}</b> </p>
                            <p style="font-size: 13px; color: #000 !important;">{{$property->agent['address_1']}}, {{$property->agent['city']}}, {{$property->agent['postcode']}}, {{$property->agent->agent_county['county']}}</p>
                            <p style="font-size: 15px; color: #000 !important;">Scheduled to close</p>
                            <p style="font-size: 18px; color: #000 !important;"><i class="fa fa-clock-o"></i><strong>{{\Carbon\Carbon::parse($property->end_date)->format('l dS M, h:i a')}}</strong></p>
                            <div class="row">
                                <div class="col-md 12">
                                    <section>
                                        @if($minutes>0)
                                            <div>
                                                <a v-if="user!='0'" href="" class="sale-tag font-montserrat"  data-toggle="modal" data-target="#place_bid_modal"> Place a Bid!</a>
                                                <a v-else href="" class="sale-tag font-montserrat"  data-toggle="modal" data-target="#exampleModalCenter"> Place a Bid!</a>
                                            </div>
                                        @endif
                                    <!-- Modal -->
                                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-center " id="exampleModalLongTitle">Lets Bid Property</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <p>Already a member? Login here</p>
                                                                <form action="{{route('login')}}" method="POST">
                                                                    @csrf
                                                                    <div class="form-group">
                                                                        <input type="text" name="email" class="form-control" placeholder="User Email" required>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="Password" class="form-control" name="password" placeholder="Password" required>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="submit" class="model-btn" value="Login" />
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>New to Lets Bid? Register Now</p>
                                                                <a href="{{url('user-sign-up')}}" class="btn-lg model-btn"> Registration </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if(\App\Bid::wherePropertyId($property->id)->count()>0)
                                            <div>
                                                <a v-if="user!='0'" class="sale-tag price font-montserrat" @click.prevent="viewCurrentBid({{$property->id}})" data-toggle="modal" href="#modal-id"> View Current Bid!</a>
                                                <a v-else class="sale-tag price font-montserrat" data-toggle="modal" href="#exampleModalCenter"> View Current Bid!</a>
                                            </div>
                                    @endif

                                    <!-- Place A Bid Modal Start -->
                                        <div class="modal fade" id="place_bid_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-center " id="exampleModalLongTitle">Clicks2bid Property</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <form action="" @submit.prevent="submitBidForm({{$property->id}})">
                                                                    <div class="form-group">
                                                                        <div class="input-group bid_amount" >
                                                                            <span class="input-group-addon" id="basic-addon1">£</span>
                                                                            <input type="text" name="amount" v-model="form.amount" class="form-control input-lg" placeholder="Bid Amount (£)" >

                                                                        </div>
                                                                    </div>
                                                                    {{--                                                                    <div class="checkbox">--}}
                                                                    {{--                                                                        <label>--}}
                                                                    {{--                                                                            <input type="checkbox" required="">  I agree to the Lets Bid--}}
                                                                    {{--                                                                            <a href="#">Terms & Conditions</a> and I give my permission to the Estate Agent to run credit checks and references on my behalf in order to check my suitability/affordability to rent--}}
                                                                    {{--                                                                        </label>--}}
                                                                    {{--                                                                    </div>--}}
                                                                    <button type="submit" class="properti-detsil sale-tag">Submit</button>
                                                                </form>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  Place A bid Modal End -->
                                        <!-- View Current Bid Modal Start -->
                                        <div class="modal fade" id="modal-id" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-center " id="exampleModalLongTitle">Clicks2bid Property</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row" v-for="(bid, index) in current_bids">
                                                            <div class="col-md-6">
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">
                                                                        @{{ index+1 }}. {{env('APP_CURRENCY')}} @{{ bid.bid_amount }}
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">
                                                                        @{{ bid.created_at | timeformat }}
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  View Current Bid Modal End -->
                                    </section>
                                </div>
                            </div>
                            <p class="text-center" style="font-size: 22px; color:#000!important;">Call Agent: {{$property->agent['phone']}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--======= PROPERTY =========-->

@endsection

@push('js')

    <script>
        $(window).load(function() {
            // The slider being synced must be initialized first
            $('#carousel').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: 138,
                itemMargin: 0,
                asNavFor: '#slider'
            });

            $('#slider').flexslider({
                animation: "slide",
                controlNav: false,
                animationLoop: false,
                slideshow: false,
                sync: "#carousel"
            });
        });

    </script>
    <script>
        // Initialize and add the map
        function initialize() {
            // The location of Uluru
            var uluru = {lat: {{$property->latitude}}, lng: {{$property->longitude}}};
            // The map, centered at Uluru
            var map = new google.maps.Map(
                document.getElementById('map_canvas'), {zoom: 18, center: uluru});
            // The marker, positioned at Uluru
            var marker = new google.maps.Marker({position: uluru, map: map});
        }
    </script>

    {{--<script type="text/javascript">--}}

        {{--var options = {--}}
            {{--componentRestrictions: {country: "uk"}--}}
        {{--};--}}
        {{--function initialize() {--}}
            {{--var input = document.getElementById('searchTextField');--}}
            {{--var autocomplete = new google.maps.places.Autocomplete(input,options);--}}
            {{--google.maps.event.addListener(autocomplete, 'place_changed', function () {--}}
                {{--var place = autocomplete.getPlace();--}}
                {{--document.getElementById('address').value = place.name;--}}
                {{--document.getElementById('latitude').value = place.geometry.location.lat();--}}
                {{--document.getElementById('longitude').value = place.geometry.location.lng();--}}
                {{--//alert("This function is working!");--}}
                {{--//alert(place.name);--}}
                {{--// alert(place.address_components[0].long_name);--}}
            {{--});--}}
        {{--}--}}
        {{--google.maps.event.addDomListener(window, 'load', initialize)--}}
    {{--</script>--}}

@endpush
