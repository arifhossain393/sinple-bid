@extends('layout.backends.app')

@section('title','View Properties')

@push('css')
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    {{--<link href="{{asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" />--}}

    <style>

        .col-lg-10 {

            margin-left: 96px !important;
        }
        .form-group .form-line .form-label {

            color: #131212 !important;

        }
        .fallback {
            margin-top: 15px !important;
        }
        .list-group-item {
            font-size: 16px !important;
            color: #000 !important;
        }
        p {
            margin: 0 0 10px;
            color: #000 !important;
            font-size: 16px !important;
        }
        h4 {
            color: #000 !important;
        }
        .panel-post .panel-heading .media .media-body h4 a {
            color: #000 !important;
        }
        .badge {

            min-width: 129px !important;

        }
        element {

            font-size: 14px !important;

        }
        span {
            font-size: 14px !important;
        }
        .carousel-control.right {
            background-image: linear-gradient(to right, rgba(0, 0, 0, .0001) 0%, rgba(255, 255, 255, 0.5) 100%) !important;
        }
        .carousel-control.left {
            background-image: linear-gradient(to right, rgba(255, 255, 255, 0.5) 0%, rgba(255, 255, 255, 0) 100%) !important;
        }
    </style>
@endpush

@section('content')

    <div class="row clearfix">

        <div class="col-xs-12 col-sm-12">
            <div class="card">
                <div class="body">
                    <div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="material-icons">description</i>Details</a></li>
                            <li role="presentation"><a href="#profile_settings" aria-controls="settings" role="tab" data-toggle="tab"><i class="material-icons">my_location</i>Map And Nearby</a></li>
                            <li role="presentation"><a href="#change_password_settings" aria-controls="settings" role="tab" data-toggle="tab"><i class="material-icons">streetview</i>Street View</a></li>
                            <li role="presentation"><a href="#change_password_settings_tab" aria-controls="settings" role="tab" data-toggle="tab"><i class="material-icons">image</i>Property gallery</a></li>
                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="home">
                                <div class="panel panel-default panel-post">
                                    <div class="panel-heading">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <img src="{{asset('uploads/property/'.$property->image[0])}}" />
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading text-left">
                                                    <a class="pull-left" href="#">{{$property->name}}</a>
                                                    <strong class="pull-right" href="#"> {{env('APP_CURRENCY')}}{{$property->price}}</strong>
                                                </h4>

                                            </div>
                                            <h4>{{$property->address}}</h4>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="post">
                                            {{--<div class="post-heading">--}}
                                                {{--<p>I am a very simple wall post. I am good at containing <a href="#">#small</a> bits of <a href="#">#information</a>. I require little more information to use effectively.</p>--}}
                                            {{--</div>--}}
                                            {{--<div class="post-content">--}}
                                                {{--<img src="{{asset('uploads/property/'.$property->image[0])}}" class="img-responsive" />--}}
                                            {{--</div>--}}
                                            <div class="post-content"  style="margin-top:20px">

                                                <!-- Badges -->
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <div class="card">
                                                        <div class="header">
                                                            <h2>
                                                                Accomodation features:

                                                            </h2>

                                                        </div>
                                                        <div class="body">
                                                            <ul class="list-group">
                                                                <li class="list-group-item"> Date available :
                                                                    <span class="badge bg-pink">
                                                                        {{Carbon\Carbon::parse($property->start_date)->format('d/m/Y')}}
                                                                    </span>
                                                                </li>
                                                                <li class="list-group-item">Number of Bedrooms :<span class="badge bg-cyan">{{$property->bed}}</span></li>
                                                                <li class="list-group-item"> Bathrooms : <span class="badge bg-teal">{{$property->bath}}</span></li>
                                                                <li class="list-group-item"> Living rooms :<span class="badge bg-orange">{{$property->living}}</span></li>
                                                                <li class="list-group-item"> Outdoor space :<span class="badge bg-purple">{{$property->outdoor}}</span></li>
                                                                <li class="list-group-item">  Furnishing :<span class="badge bg-purple">{{$property->furnishing ? 'YES' :'NO'}}</span></li>
                                                                <li class="list-group-item">  Property Type :<span class="badge bg-purple">{{$property->type ? 'Sale' :'Rent'}}</span></li>
                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- #END# Badges -->
                                            </div>
                                            <div class="post-content">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <h4>Key features:</h4>
                                                    {!! $property->features !!}
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <h4>Description:</h4>
                                                    {!! $property->description !!}
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>


                            </div>
                            <div role="tabpanel" class="tab-pane fade in" id="profile_settings">
                                <form class="form-horizontal">

                                    <div id="map_canvas" style="width: 900px; height: 500px;margin-left: 30px; border: 3px solid #555555;">

                                    </div>


                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in" id="change_password_settings">
                                <form class="form-horizontal">
                                    <div style="width: 900px; height: 500px;margin-left: 30px;">
                                        <iframe
                                            width="100%"
                                            height="500"
                                            frameborder="5"
                                            scrolling="no"
                                            marginheight="0"
                                            marginwidth="0"
                                            src="https://www.google.com/maps/embed/v1/streetview?key={{env('GOOGLE_API_KEY')}}&location={{$latitude}},{{$longitude}}&heading=1&pitch=1
  &fov=35"
                                        >
                                        </iframe>

                                    </div>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in" id="change_password_settings_tab">
                                <form class="form-horizontal">

                                    <div class="row clearfix">
                                        <!-- Basic Example -->
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="card">

                                                <div class="body">
                                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                                        <!-- Indicators -->

                                                        <ol class="carousel-indicators">
                                                            @foreach($property->image as $img)
                                                            <li data-target="id_{{$loop->index}}"  data-slide-to="{{$loop->index}}" class="active"></li>
                                                            @endforeach
                                                        </ol>

                                                        <!-- Wrapper for slides -->
                                                        <div class="carousel-inner" role="listbox">
                                                            @foreach($property->image as $img)
                                                            <div class="item @if($loop->index==0) active @endif">
                                                                <img src="{{asset('uploads/property/'.$img)}}" />
                                                            </div>
                                                            @endforeach
                                                            {{--<div class="item">--}}
                                                                {{--<img src="{{asset('assets/backend/images/image-gallery/12.jpg')}}" />--}}
                                                            {{--</div>--}}
                                                            {{--<div class="item">--}}
                                                                {{--<img src="{{asset('assets/backend/images/image-gallery/19.jpg')}}" />--}}
                                                            {{--</div>--}}
                                                        </div>


                                                        <!-- Controls -->
                                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                            <span class="sr-only">Previous</span>
                                                        </a>
                                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                            <span class="sr-only">Next</span>
                                                        </a>

                                                        @foreach($property->image as $img)
                                                            <img  src="{{asset('uploads/property/'.$img)}}" alt="" width="50">
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- #END# Basic Example -->
                                        <!-- With Captions -->

                                        <!-- #END# With Captions -->
                                    </div>


                                </form>
                            </div>
                        </div>



                    <a class="btn btn-danger waves-effect" href="{{route('property.index')}}">
                        <span>Back</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <!-- Bootstrap Material Datetime Picker Plugin Js -->

    <script src="{{asset('assets/backend/js/pages/forms/form-validation.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/forms/basic-form-elements.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/forms/advanced-form-elements.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/dropzone/dropzone.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>

    <script>
        tinymce.init({
            selector: 'textarea'
        });
    </script>

    <script>
        $('#start_date').bootstrapMaterialDatePicker();
        $('#end_date').bootstrapMaterialDatePicker();

    </script>
        <script>
            // Initialize and add the map
            function initMap() {
                // The location of Uluru
                var uluru = {lat: {{$property->latitude}}, lng: {{$property->longitude}}};
                // The map, centered at Uluru
                var map = new google.maps.Map(
                    document.getElementById('map_canvas'), {zoom: 18, center: uluru});
                // The marker, positioned at Uluru
                var marker = new google.maps.Marker({position: uluru, map: map});
            }
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&callback=initMap">
        </script>

@endpush
