@extends('layout.backends.app')

@section('title','Update Properties')

@push('css')
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    {{--<link href="{{asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" />--}}

    <style>

        .col-lg-10 {

            margin-left: 96px !important;
        }
        .form-group .form-line .form-label {

            color: #131212 !important;

        }
        .fallback {
            margin-top: 15px !important;
        }
        .form-control {
            color: #000 !important;
        }
        .card .body {

            color: #000 !important;
        }
    </style>
@endpush

@section('content')
    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10" >
            <div class="card">

                <div class="header">
                    <h2 class="card-inside-title">Update Properties</h2>
                    {{--                            <ul class="header-dropdown m-r--5">--}}
                    {{--                                <li class="dropdown">--}}
                    {{--                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">--}}
                    {{--                                        <i class="material-icons">more_vert</i>--}}
                    {{--                                    </a>--}}
                    {{--                                    <ul class="dropdown-menu pull-right">--}}
                    {{--                                        <li><a href="javascript:void(0);">Action</a></li>--}}
                    {{--                                        <li><a href="javascript:void(0);">Another action</a></li>--}}
                    {{--                                        <li><a href="javascript:void(0);">Something else here</a></li>--}}
                    {{--                                    </ul>--}}
                    {{--                                </li>--}}
                    {{--                            </ul>--}}
                </div>
                <div class="body">
                    <form id="form_validation" method="post" action="{{route('property.update',$property->id)}}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" value="{{$property->name}}" class="form-control" name="name" >
                                @error('name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror

                                <label class="form-label">Property Name:</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input class="form-control input-search" id="searchTextField" type="text" size="50"  autocomplete="on" runat="server" value="{{$property->address}}" />
                                <input type="hidden" id="address" name="address" value="{{$property->address}}"/>
                                <input type="hidden" id="latitude" name="latitude" />
                                <input type="hidden" id="longitude" name="longitude" />

                                {{--<input v-model="address"  type="text" id="searchTextField" class="form-control input-search" autocomplete="on" runat="server"  >--}}
                                <label class="form-label">Address:</label>
                                @error('address')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        {{--<div class="form-group form-float">--}}
                            {{--<div class="form-line">--}}
                                {{--<input  value="{{$property->city}}" type="text" class="form-control" name="city" >--}}
                                {{--<label class="form-label">City:</label>--}}
                                {{--@error('city')--}}
                                {{--<span class="text-danger">{{ $message }}</span>--}}
                                {{--@enderror--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group form-float">
                            <div class="form-line">
                                <input  value="{{$property->postcode}}" type="text" class="form-control" name="postcode" >
                                <label class="form-label">Post Code:</label>
                                @error('postcode')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group form-float">
                            <div class="form-line">
                                <input value="{{$property->price}}" type="text" class="form-control" name="price"  >
                                <label class="form-label">Price:</label>
                                @error('price')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input value="{{$property->start_date}}" id="start_date" type="text" class="form-control datepicker form-control" name="start_date">
                                <label class="form-label">Opening Date:</label>
                                @error('start_date')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group form-float">
                            <div class="form-line">
                                <input value="{{$property->end_date}}" id="end_date" type="text" class="form-control datepicker form-control"  name="end_date"  >
                                <label class="form-label form-label-text">Closing Date:</label>
                                @error('end_date')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="bed" value="{{$property->bed}}" >
                                <label class="form-label ">Bed Room:</label>
                                @error('bed')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="bath"  value="{{$property->bath}}">
                                <label class="form-label">Bathroom:</label>
                                @error('bath')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="living"  value="{{$property->living}}">
                                <label class="form-label">Living Room:</label>
                                @error('living')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="outdoor" value="{{$property->outdoor}}" >
                                <label class="form-label">Outdoor Space:</label>
                            </div>
                        </div>

                        <h2 class="card-inside-title">Furnishing:</h2>
                        <div class="demo-radio-button">
                            <input name="furnishing" type="radio" class="with-gap" id="radio_3" value="1" {{$property->furnishing ? 'checked':''}}/>
                            <label for="radio_3">Yes</label>
                            <input name="furnishing" value="0" type="radio" id="radio_4" class="with-gap" {{$property->furnishing ? '':'checked'}}/>
                            <label for="radio_4">No</label>


                        </div>
                        <h2 class="card-inside-title">Property Type:</h2>
                        <div class="demo-radio-button">
                            <input name="type" type="radio" class="with-gap" id="radio_1" value="1" {{$property->type ? 'checked':''}} />
                            <label for="radio_1">Sale</label>
                            <input name="type" type="radio" id="radio_2" value="0" class="with-gap" {{$property->type ? '':'checked'}}/>
                            <label for="radio_2">Rent</label>
                        </div>

                        <h2 class="card-inside-title">Bid Type:</h2>
                        <div class="demo-radio-button">
                            <input  name="bid_type" type="radio" class="with-gap" id="radio_5" value="1" {{$property->bid_type ? 'checked':''}}/>
                            <label for="radio_5">Multiple</label>
                            <input   name="bid_type" value="0" type="radio" id="radio_6" class="with-gap" {{$property->bid_type ? '':'checked'}}/>
                            <label for="radio_6">Single</label>
                        </div>

                        <div class="fallback">
                            <label class="form-label form-label-text "><b>Image:</b></label>
                            <input name="image[]" type="file" multiple />
                            @foreach($property->image as $img)
                                <img src="{{asset('uploads/property/'.$img)}}" alt="" width="50">
                            @endforeach

                            @error('image')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="fallback">
                            <label class="form-label form-label-text "><b>Document:</b></label>
                            <input name="document" type="file" />
                            @error('document')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>


                        {{--<div class="dropzone"  id="frmFileUpload" >--}}
                        {{--<div class="dz-message">--}}
                        {{--<div class="drag-icon-cph">--}}
                        {{--<i class="material-icons">touch_app</i>--}}
                        {{--</div>--}}
                        {{--<h3>Drop files here or click to upload.</h3>--}}
                        {{--<em>(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</em>--}}
                        {{--</div>--}}
                        {{--<div class="fallback">--}}
                        {{--<input name="image" type="file" multiple />--}}
                        {{--</div>--}}
                        {{--</div>--}}

                        <h2 class="card-inside-title">Key Features:</h2>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea name="features" rows="4" class="form-control no-resize" placeholder="Please type what you want...">
                                            {{$property->features}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h2 class="card-inside-title">Description:</h2>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea  rows="4" name="description" class="form-control no-resize" placeholder="Please type what you want...">
                                            {{$property->description}}
                                        </textarea>
                                        @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        <a class="btn btn-danger waves-effect" href="{{route('property.index')}}">
                            <span>Back</span>
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Validation -->
    <!-- Advanced Validation -->

    <!-- #END# Advanced Validation -->
    <!-- Validation Stats -->

    <!-- #END# Validation Stats -->
@endsection

@push('js')
    <!-- Bootstrap Material Datetime Picker Plugin Js -->

    <script src="{{asset('assets/backend/js/pages/forms/form-validation.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/forms/basic-form-elements.js')}}"></script>
    <script src="{{asset('assets/backend/js/pages/forms/advanced-form-elements.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/dropzone/dropzone.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>
{{--    <script src="{{asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>--}}

    <script src="{{asset('assets/backend/plugins/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        });
    </script>

    <script>
        $('#start_date').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm' });
        $('#end_date').bootstrapMaterialDatePicker({format : 'YYYY-MM-DD HH:mm' });

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API_KEY')}}&libraries=places&callback=initialize"
            async defer></script>
    <script type="text/javascript">

        var options = {
            types_1: ['address'],
            componentRestrictions: {country: "uk"}

        };

        function initialize() {
            var input = document.getElementById('searchTextField');
            var autocomplete = new google.maps.places.Autocomplete(input,options);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                document.getElementById('address').value = place.name;
                document.getElementById('latitude').value = place.geometry.location.lat();
                document.getElementById('longitude').value = place.geometry.location.lng();
                //alert("This function is working!");
                //alert(place.name);
                // alert(place.address_components[0].long_name);
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize)
    </script>

@endpush
