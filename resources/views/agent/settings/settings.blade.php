@extends('layout.backends.app')

@section('title','Acounts Settings')

@push('css')
    <link href="{{asset('assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" />

    {{--<link href="{{asset('assets/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" />--}}

    <style>

        .col-lg-10 {

            margin-left: 96px !important;
        }
        .form-group .form-line .form-label {

            color: #131212 !important;

        }
        .fallback {
            margin-top: 15px !important;
        }
        .list-group-item {
            font-size: 16px !important;
            color: #000 !important;
        }
        p {
            margin: 0 0 10px;
            color: #000 !important;
            font-size: 16px !important;
        }
        h4 {
            color: #000 !important;
        }
        .panel-post .panel-heading .media .media-body h4 a {
            color: #000 !important;
        }
        .badge {

            min-width: 129px !important;

        }
        element {

            font-size: 14px !important;

        }
        span {
            font-size: 14px !important;
        }
        .form-group {
            width: 55% !important;

        }
        input {
            margin-top: 5px !important;
        }
        .card .body {

            color: #0b0a0a !important;
        }
        .btn.btn-danger {
            margin-left: -74px !important;
        }
        b, strong {
            margin-left: 19px !important;
        }
        .input-style{margin-top: -1px !important;}
        .form-control {

            color: #000 !important;

        }
        .btn-group.bootstrap-select.form-line {
            margin-left: 11px !important;
        }
        .col-sm-2 {
            width: 19.667% !important;
        }


    </style>
@endpush

@section('content')

    <div class="row clearfix">

        <div class="col-xs-12 col-sm-12">
            <div class="card">
                <div class="body">
                    <div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="@if(!session()->get('active')) active @endif"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="material-icons">face</i> UPDATE PROFILE</a></li>
                            <li class="@if(session()->get('active')) active @endif" role="presentation"><a href="#profile_settings" aria-controls="settings" role="tab" data-toggle="tab"><i class="material-icons">change_history</i> CHANGE PASSWORD</a></li>

                        </ul>

                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in @if(!session()->get('active')) active @endif" id="home">
                                <form class="form-horizontal" method="POST" action="{{ route('agent.profile.update',$agent->id) }}"  enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label for="name" class="col-sm-2 control-label" style="margin-left: 2px !important;">First Name:</label>
                                        <div class="col-sm-9">
                                            <div class="form-line">
                                                <input type="text" class="form-control input-style"  name="f_name" placeholder="Name" value="{{Auth::guard('agent')->user()->f_name}}">
                                            </div>
                                            @error('f_name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="name" class="col-sm-2 control-label" style="margin-left: 2px !important;">Last Name:</label>
                                        <div class="col-sm-9">
                                            <div class="form-line">
                                                <input type="text" class="form-control input-style"  name="l_name" placeholder="Name" value="{{Auth::guard('agent')->user()->l_name}}">
                                            </div>
                                            @error('l_name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">Email:</label>
                                        <div class="col-sm-9">
                                            <div class="form-line">
                                                <input type="email" class="form-control input-style" id="NewPassword" name="email"  value="{{Auth::guard('agent')->user()->email}}">
                                            </div>
                                            @error('email')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone" class="col-sm-2 control-label" style="margin-left: 5px !important;">Phone:</label>
                                        <div class="col-sm-9">
                                            <div class="form-line">
                                                <input type="text" class="form-control input-style"  name="phone"  value="{{Auth::guard('agent')->user()->phone}}">
                                            </div>
                                            @error('phone')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address_1" class="col-sm-2 control-label" style="margin-left: 5px !important;">Address-1:</label>
                                        <div class="col-sm-9">
                                            <div class="form-line">
                                                <input type="text" class="form-control input-style"  name="address_1"  value="{{Auth::guard('agent')->user()->address_1}}">
                                            </div>
                                            @error('address_1')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address_2" class="col-sm-2 control-label" style="margin-left: 5px !important;">Address-2:</label>
                                        <div class="col-sm-9">
                                            <div class="form-line">
                                                <input type="text" class="form-control input-style"  name="address_2"  value="{{Auth::guard('agent')->user()->address_2}}">
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="postcode" class="col-sm-2 control-label" style="margin-left: 5px !important;">Postcode:</label>
                                        <div class="col-sm-9">
                                            <div class="form-line">
                                                <input type="text" class="form-control input-style"  name="postcode"  value="{{Auth::guard('agent')->user()->postcode}}">
                                            </div>
                                            @error('postcode')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="country" class="col-sm-2 control-label" style="margin-left: 5px !important;">Country:</label>
                                        <div class="col-sm-9">
                                        <select class="form-line" name="country">
                                            <option value="">*Country</option>
                                            @foreach($countries as $country)
                                                @if($country->id==$agent->country)
                                                    <option value="{{$country->id}}"selected>{{$country->name}}</option>
                                                @else
                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                @endif
                                            @endforeach
                                            @error('country')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </select>
                                        </div>
                                    </div>




                                    <div class="form-group">
                                        <label for="city" class="col-sm-2 control-label" >City:</label>
                                        <div class="col-sm-9">
                                            <div class="form-line">
                                                <input type="text" class="form-control input-style"  name="city" placeholder="Enter Your Phone number" value="{{Auth::guard('agent')->user()->city}}">
                                            </div>
                                            @error('city')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="county" class="col-sm-2 control-label" style="margin-left: 5px !important;">County:</label>
                                        <div class="col-sm-9">
                                        <select class="form-line" name="county" >

                                            <option value="">*County</option>
                                            @foreach($county as $coun)
                                                @if($coun->id==$agent->county)
                                                    <option value="{{$coun->id}}"selected>{{$coun->county}}</option>
                                                @else
                                                    <option value="{{$coun->id}}">{{$coun->county}}</option>
                                                @endif
                                            @endforeach
                                            @error('county')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </select>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label for="branch" class="col-sm-2 control-label" style="margin-left: 5px !important;">Branches:</label>
                                        <div class="col-sm-9">
                                            <div class="form-line">
                                                <input type="text" class="form-control input-style"  name="branch" placeholder="Enter Your Phone number" value="{{Auth::guard('agent')->user()->branch}}">
                                            </div>
                                            @error('branch')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label "><b>Image:</b></label>
                                        <input name="image" type="file" multiple />
                                        <img src="{{asset('uploads/agent/'.$agent->image)}}" alt="{{$agent->name}}" width="50">
                                        @error('image')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-9">
                                            <button type="submit" class="btn btn-danger">SUBMIT</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade in @if(session()->get('active')) active @endif " id="profile_settings">
                                <form class="form-horizontal"method="POST" action="{{ route('agent.password.update',$agent->id) }}"  enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group">
                                        <label for="OldPassword" class="col-sm-4 control-label">Old Password:</label>
                                        <div class="col-sm-8">
                                            <div class="form-line">
                                                <input type="password" class="form-control input-style"  name="old_password" placeholder="Old Password" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="NewPassword" class="col-sm-4 control-label">New Password:</label>
                                        <div class="col-sm-8">
                                            <div class="form-line">
                                                <input type="password" class="form-control input-style"  name="password" placeholder="New Password" >

                                            </div>
                                            @error('password')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="NewPasswordConfirm" class="col-sm-4 control-label">Confirm Password:</label>
                                        <div class="col-sm-8">
                                            <div class="form-line">
                                                <input type="password" class="form-control input-style"  name="confirm_password" placeholder="New Password (Confirm)" >

                                            </div>
                                            @error('confirm_password')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-danger">SUBMIT</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>



                        {{--<a class="btn btn-danger waves-effect" href="">--}}
                            {{--<span>Back</span>--}}
                        {{--</a>--}}
                    </div>
                </div>
            </div>
        </div>

    @endsection

    @push('js')
        <!-- Bootstrap Material Datetime Picker Plugin Js -->

            <script src="{{asset('assets/backend/js/pages/forms/form-validation.js')}}"></script>
            <script src="{{asset('assets/backend/js/pages/forms/basic-form-elements.js')}}"></script>
            <script src="{{asset('assets/backend/js/pages/forms/advanced-form-elements.js')}}"></script>
            <script src="{{asset('assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
            <script src="{{asset('assets/backend/plugins/dropzone/dropzone.js')}}"></script>
            <script src="{{asset('assets/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

            <script src="{{asset('assets/backend/plugins/ckeditor/ckeditor.js')}}"></script>
            <script src="{{asset('assets/backend/plugins/tinymce/tinymce.js')}}"></script>

            <script>
                tinymce.init({
                    selector: 'textarea'
                });
            </script>

            <script>
                $('#start_date').bootstrapMaterialDatePicker();
                $('#end_date').bootstrapMaterialDatePicker();

            </script>

    @endpush
