@extends('layout.backendss.app')
@section('title','Plan')
@push('css')
    <style>
        .gateway--info {
            margin-left: 22% !important;
        }
        .btn.btn-pay {
            color: #003085 !important;
        }
        .col {
            margin-left: 15px !important;
        }
    </style>
@endpush
{{--@section('content')--}}
    {{--@php--}}
    {{--$email = '';--}}
    {{--$url = '';--}}
        {{--if(env('PAYPAL_MODE')=='sandbox'){--}}
            {{--$email = env('PAYAPL_SANDBOX_EMAIL');--}}
            {{--$url = "https://www.sandbox.paypal.com/cgi-bin/webscr";--}}
        {{--}elseif(env('PAYPAL_MODE')=='live'){--}}
         {{--$email = env('PAYAPL_LIVE_EMAIL');--}}
            {{--$url = "https://www.paypal.com/cgi-bin/webscr";--}}
        {{--}--}}
    {{--@endphp--}}
    {{--<form action="{{$url}}" method="post" name="frmTransaction" id="frmTransaction">--}}
        {{--<input type="hidden" name="business" value="{{$email}}">--}}
        {{--<input type="hidden" name="cmd" value="_xclick">--}}
        {{--<input type="hidden" name="item_name" value="{{$plan->name}}">--}}
        {{--<input type="hidden" name="item_number" value="{{$plan->id}}">--}}
        {{--<input type="hidden" name="amount" value="{{$plan->price}}">--}}
        {{--<input type="hidden" name="currency_code" value="GBP">--}}
        {{--<input type="hidden" name="cancel_return" value="{{route('payment.cancel')}}">--}}
        {{--<input type="hidden" name="return" value="{{route('payment.status')}}">--}}
       {{--<button type="Submit" onclick="paymentForm()">Pay</button>--}}
    {{--</form>--}}
{{--@endsection--}}
@section('content')
    <div class="container">
        <div class="gateway--info">
            <div class="gateway--desc">
                @if(session()->has('message'))
                    <p class="message">
                        {{ session('message') }}
                    </p>
                @endif
                <div class="row">
                    <div class="col">
                        <img src="{{ asset('assets/backend/images/paypal.png') }}" class="img-responsive gateway__img">
                    </div>
                    <div class="col">
                        <img src="" class="img-responsive gateway__img">
                    </div>
                </div>
                <p><strong>Order Overview !</strong></p>
                <hr>
                {{--<p>Item : Yearly Subscription cost !</p>--}}
                <p>Amount : {{env('APP_CURRENCY')}}{{ $service->price }}</p>
                <hr>
            </div>
            <div class="gateway--paypal">
                <form method="POST" action="{{ route('checkout.payment.paypal', ['transaction_id' => encrypt($transaction_id)]) }}">
                    {{ csrf_field() }}
                    <button class="btn btn-pay">
                        <i class="fa fa-paypal" aria-hidden="true"></i> Pay with PayPal
                    </button>
                </form>
            </div>
        </div>
    </div>
@stop
@push('js')
    <script>
        function paymentForm(){
            document.frmTransaction.submit();
        }
    </script>

@endpush
