@extends('layouts.frontend.app')

@section('title','Property-Details')

@push('css')
    <style>
        .panel {
                border-color: #ddd;
        }
        .panel-default > .panel-heading {
            color: #333;
            background-color: #f5f5f5;
            border-color: #ddd;
            padding: 10px 15px;
            border-bottom: 1px solid transparent;
            border-bottom-color: transparent;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
        }
        .panel-body {
            padding: 15px;
        }
        .center {
            margin-left: 2% !important;
            display: block !important;
            margin-left: auto;
            margin-right: auto;
            width: 50% !important;
        }
        .time-remaining {
            padding-top: 5px;
            padding-bottom: 5px;
            border-radius: 4px;
            background: #0a0a0a;
            color: #ffffff;
        }
        .sale-tag.price {
            background: #376bff;
        }
        .sale-tag {
            background: #ff5722;
            font-size: 16px;
            text-transform: uppercase;
            color: #fff;
            display: inline-block;
            float: left;
            padding: 10px 30px;
            margin-bottom: 5px;
            width: 100%;
            text-align: center;
        }
        .properties-details-page .sidebar-right {
            margin: 0 0 30px 0;
            background: #fff;
            border: 1px solid #ddd;
            box-shadow: none;
        }
        .tab-content {
            background: #F4F4F4;
        }
        .properties-description, .property-details  {
            padding: 15px;
        }
        .tabbing-box .tab-pane {
            padding-top: 10px !important;
        }
        .sale-tag.price:hover {
            background: #0746f7;
            color: #fff;
        }
    </style>
@endpush

@section('content')
    <!-- Sub banner start -->
<div class="sub-banner">
    <div class="container breadcrumb-area">
        <div class="breadcrumb-areas">
            <h1>Property Details</h1>
            <ul class="breadcrumbs">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Property Details</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub Banner end -->

<!-- Properties details page start -->
<div class="properties-details-page content-area-6">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 col-xs-12">
                <div class="properties-details-section">
                    <div id="propertiesDetailsSlider" class="carousel properties-details-sliders slide mb-40">
                        <!-- main slider carousel items -->
                        <div class="carousel-inner">

                            @foreach($property['image'] as $key=>$img)
                                <div class="{{ $key == 0 ? ' active' : '' }} item carousel-item" data-slide-number="{{ $key++ }}">
                                    <img src="{{asset('uploads/property/'.$img)}}" class="img-fluid" alt="{{$property->name}}" style="width: 100%;">
                                </div>
                            @endforeach

                        </div>
                        <!-- main slider carousel nav controls -->
                        <ul class="carousel-indicators smail-properties list-inline nav nav-justified">
                            @foreach($property['image'] as $key=>$img)
                                <li class="list-inline-item {{ $key == 0 ? ' active' : '' }}">
                                    <a id="carousel-selector-0" class="selected" data-slide-to="{{ $key++ }}" data-target="#propertiesDetailsSlider">
                                        <img src="{{asset('uploads/property/'.$img)}}" class="img-fluid" alt="{{$property->name}}">
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="heading-properties-2">
                            <h3>{{$property->name}}</h3>
                            <div class="price-location"><span class="property-price">{{env('APP_CURRENCY')}}{{$property->price}}</span> <span class="rent">FOR @if($property->type=='1')SALE @elseif($property->type=='0') RENT @endif</span> <span class="location"><i class="flaticon-pin"></i>{{$property->agent['address_1']}}, {{$property->agent['city']}}, {{$property->agent['postcode']}}, {{$property->agent->agent_county['county']}}</span></div>
                        </div>
                    </div>
                    <!-- Advanced search start -->
                    {{-- <div class="widget-2 advanced-search bg-grea-2 d-lg-none d-xl-none">
                        <h3 class="sidebar-title">Search Properties</h3>
                        <form method="GET">
                            <div class="form-group">
                                <select class="selectpicker search-fields" name="all-status">
                                    <option>All Status</option>
                                    <option>For Sale</option>
                                    <option>For Rent</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="selectpicker search-fields" name="all-type">
                                    <option>All Type</option>
                                    <option>Apartments</option>
                                    <option>Shop</option>
                                    <option>Restaurant</option>
                                    <option>Villa</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="selectpicker search-fields" name="commercial">
                                    <option>Commercial</option>
                                    <option>Residential</option>
                                    <option>Land</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="selectpicker search-fields" name="location">
                                    <option>Location</option>
                                    <option>American</option>
                                    <option>Florida</option>
                                    <option>Belgium</option>
                                    <option>Canada</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select class="selectpicker search-fields" name="bedrooms">
                                            <option>Bedrooms</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select class="selectpicker search-fields" name="bathroom">
                                            <option>Bathroom</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="range-slider">
                                <label>Price</label>
                                <div data-min="0" data-max="150000"  data-min-name="min_price" data-max-name="max_price" data-unit="USD" class="range-slider-ui ui-slider" aria-disabled="false"></div>
                                <div class="clearfix"></div>
                            </div>
                            <a class="show-more-options" data-toggle="collapse" data-target="#options-content2">
                                <i class="fa fa-plus-circle"></i> Other Features
                            </a>
                            <div id="options-content2" class="collapse">
                                <h3 class="sidebar-title">Amenities</h3>
                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox9" type="checkbox">
                                    <label for="checkbox9">
                                        Air Condition
                                    </label>
                                </div>
                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox10" type="checkbox">
                                    <label for="checkbox10">
                                        Places to seat
                                    </label>
                                </div>
                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox11" type="checkbox">
                                    <label for="checkbox11">
                                        Swimming Pool
                                    </label>
                                </div>
                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox12" type="checkbox">
                                    <label for="checkbox12">
                                        Free Parking
                                    </label>
                                </div>
                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox13" type="checkbox">
                                    <label for="checkbox13">
                                        Central Heating
                                    </label>
                                </div>
                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox14" type="checkbox">
                                    <label for="checkbox14">
                                        Laundry Room
                                    </label>
                                </div>
                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox15" type="checkbox">
                                    <label for="checkbox15">
                                        Window Covering
                                    </label>
                                </div>
                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox16" type="checkbox">
                                    <label for="checkbox16">
                                        Alarm
                                    </label>
                                </div>
                                <br>
                            </div>
                            <div class="form-group mb-0">
                                <button class="search-button">Search</button>
                            </div>
                        </form>
                    </div> --}}
                    <!-- Tabbing box start -->
                    <div class="tabbing tabbing-box mb-40">
                        <ul class="nav nav-tabs" id="carTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="one" aria-selected="false">Description</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="three" aria-selected="true">Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="5-tab" data-toggle="tab" href="#5" role="tab" aria-controls="5" aria-selected="true">Location</a>
                            </li>

                        </ul>
                        <div class="tab-content" id="carTabContent">
                            <div class="tab-pane fade active show" id="one" role="tabpanel" aria-labelledby="one-tab">
                                <div class="properties-description mb-50">
                                    <h3 class="heading-2">
                                       Key features:
                                    </h3>
                                    {!! $property['features'] !!}
                                    <br>
                                    <h3 class="heading-2">
                                        Description
                                    </h3>
                                    {!! $property->description !!}

                                </div>
                            </div>
                            <div class="tab-pane fade " id="three" role="tabpanel" aria-labelledby="three-tab">
                                <div class="property-details mb-40">
                                    <h3 class="heading-2">Property Details</h3>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6">
                                            <ul>
                                                <li>
                                                    <strong>Number of Bedrooms : {{$property['bed']}} </strong>
                                                </li>
                                                <li>
                                                    <strong>Bathrooms : {{$property['bath']}} </strong>
                                                </li>
                                                <li>
                                                    <strong>Living rooms : {{$property['living']}} </strong>
                                                </li>

                                            </ul>
                                        </div>
                                        <div class="col-md-4 col-sm-6">
                                            <ul>
                                                <li>
                                                    <strong>Outdoor space : {{$property['outdoor']}} </strong>
                                                </li>
                                                <li>
                                                    <strong>Furnishing : {{$property['furnishing']? 'Yes': 'No'}} </strong>
                                                </li>
                                                <li>
                                                    <strong>Bid Type : {{$property['bid_type']? 'Multiple': 'Single'}} </strong>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade " id="5" role="tabpanel" aria-labelledby="5-tab">
                                <div class="location mb-50">
                                    <iframe
                                        width="100%"
                                        height="550"
                                        frameborder="5"
                                        scrolling="no"
                                        marginheight="0"
                                        marginwidth="0"
                                        src="https://www.google.com/maps/embed/v1/streetview?key=AIzaSyAgtdpYUlacT_ST0_DkfCnGUzucMpKnXfI&location={{$property['latitude']}},{{$property['longitude']}}&heading=1&pitch=1
  &fov=35"
                                    >
                                    </iframe>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="sidebar-right">
                   <div class="panel panel-default">
                        <div class="panel-heading" >This property is marketed by:</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-8">
                                    <img class="center" src="{{asset('uploads/agent/'.$property->agent['image'])}}" alt="{{$property->agent['f_name']}}">
                                </div>
                                <div class="col-sm-4">
                                    <p class="text-center" style="font-size: 11px; color:#000000 !important; ">Time remaining</p>
                                    <div class="time-remaining text-center">
                                        @php
                                            //  $days = \Carbon\Carbon::parse($property->start_date)->diffInMinutes(\Carbon\Carbon::parse($property->end_date),$absolute = false);
                                            $minutes = \Carbon\Carbon::parse(\Carbon\Carbon::now())->diffInMinutes(\Carbon\Carbon::parse($property->end_date),$absolute = false);
                                            $day = \Carbon\Carbon::parse(\Carbon\Carbon::now())->diffInDays(\Carbon\Carbon::parse($property->end_date),$absolute = false);
                                            //  echo $minutes;
                                        @endphp
                                        @if($minutes<=0)
                                            Experied
                                        @elseif($minutes==1440)
                                            1 day left
                                        @elseif($minutes>1440)
                                            {{$day}} days left
                                        @elseif($minutes<1440)
                                            0 day left
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <p style="font-size:14px; color: #000 !important;"><b>{{$property->agent['c_name']}}</b> </p>
                            <p style="font-size: 13px; color: #000 !important;">{{$property->agent['address_1']}}, {{$property->agent['city']}}, {{$property->agent['postcode']}}, {{$property->agent->agent_county['county']}}</p>
                            <p style="font-size: 15px; color: #000 !important;">Scheduled to close</p>
                            <p style="font-size: 18px; color: #000 !important;"><i class="fa fa-clock-o"></i><strong>{{\Carbon\Carbon::parse($property->end_date)->format('l dS M, h:i a')}}</strong></p>
                            <div class="row">
                                <div class="col-md 12">
                                    <section>
                                        @if($minutes>0)
                                            <div>
                                                <a v-if="user!='0'" href="" class="sale-tag font-montserrat"  data-toggle="modal" data-target="#place_bid_modal"> Place a Bid!</a>
                                                <a v-else href="" class="sale-tag font-montserrat"  data-toggle="modal" data-target="#exampleModalCenter"> Place a Bid!</a>
                                            </div>
                                        @endif
                                    <!-- Modal -->
                                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-center " id="exampleModalLongTitle">Lets Bid Property</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <p>Already a member? Login here</p>
                                                                <form action="{{route('login')}}" method="POST">
                                                                    @csrf
                                                                    <div class="form-group">
                                                                        <input type="text" name="email" class="form-control" placeholder="User Email" required>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="Password" class="form-control" name="password" placeholder="Password" required>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="submit" class="model-btn" value="Login" />
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>New to Lets Bid? Register Now</p>
                                                                <a href="{{url('user-sign-up')}}" class="btn-lg model-btn"> Registration </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if(\App\Bid::wherePropertyId($property->id)->count()>0)
                                            <div>
                                                {{-- <a v-if="user!='0'" class="sale-tag price font-montserrat" @click.prevent="viewCurrentBid({{$property->id}})" data-toggle="modal" href="#modal-id"> View Current Bid!</a> --}}
                                                <a v-else class="sale-tag price font-montserrat" data-toggle="modal" href="#exampleModalCenter"> View Current Bid!</a>
                                            </div>
                                    @endif

                                    <!-- Place A Bid Modal Start -->
                                        <div class="modal fade" id="place_bid_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-center " id="exampleModalLongTitle">Clicks2bid Property</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <form action="" @submit.prevent="submitBidForm({{$property->id}})">
                                                                    <div class="form-group">
                                                                        <div class="input-group bid_amount" >
                                                                            <span class="input-group-addon" id="basic-addon1">£</span>
                                                                            <input type="text" name="amount" v-model="form.amount" class="form-control input-lg" placeholder="Bid Amount (£)" >

                                                                        </div>
                                                                    </div>
                                                                    {{--                                                                    <div class="checkbox">--}}
                                                                    {{--                                                                        <label>--}}
                                                                    {{--                                                                            <input type="checkbox" required="">  I agree to the Lets Bid--}}
                                                                    {{--                                                                            <a href="#">Terms & Conditions</a> and I give my permission to the Estate Agent to run credit checks and references on my behalf in order to check my suitability/affordability to rent--}}
                                                                    {{--                                                                        </label>--}}
                                                                    {{--                                                                    </div>--}}
                                                                    <button type="submit" class="properti-detsil sale-tag">Submit</button>
                                                                </form>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  Place A bid Modal End -->
                                        <!-- View Current Bid Modal Start -->
                                        <div class="modal fade" id="modal-id" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-center " id="exampleModalLongTitle">Clicks2bid Property</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row" v-for="(bid, index) in current_bids">
                                                            <div class="col-md-6">
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">
                                                                        @{{ index+1 }}. {{env('APP_CURRENCY')}} @{{ bid.bid_amount }}
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <ul class="list-group">
                                                                    <li class="list-group-item">
                                                                        @{{ bid.created_at | timeformat }}
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--  View Current Bid Modal End -->
                                    </section>
                                </div>
                            </div>
                            <p class="text-center" style="font-size: 22px; color:#000!important;">Call Agent: {{$property->agent['phone']}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Properties details page end -->

@endsection

@push('js')

    <script>
        // Initialize and add the map
        function initialize() {
            // The location of Uluru
            var uluru = {lat: {{$property->latitude}}, lng: {{$property->longitude}}};
            // The map, centered at Uluru
            var map = new google.maps.Map(
                document.getElementById('map_canvas'), {zoom: 18, center: uluru});
            // The marker, positioned at Uluru
            var marker = new google.maps.Marker({position: uluru, map: map});
        }
    </script>

    {{--<script type="text/javascript">--}}

        {{--var options = {--}}
            {{--componentRestrictions: {country: "uk"}--}}
        {{--};--}}
        {{--function initialize() {--}}
            {{--var input = document.getElementById('searchTextField');--}}
            {{--var autocomplete = new google.maps.places.Autocomplete(input,options);--}}
            {{--google.maps.event.addListener(autocomplete, 'place_changed', function () {--}}
                {{--var place = autocomplete.getPlace();--}}
                {{--document.getElementById('address').value = place.name;--}}
                {{--document.getElementById('latitude').value = place.geometry.location.lat();--}}
                {{--document.getElementById('longitude').value = place.geometry.location.lng();--}}
                {{--//alert("This function is working!");--}}
                {{--//alert(place.name);--}}
                {{--// alert(place.address_components[0].long_name);--}}
            {{--});--}}
        {{--}--}}
        {{--google.maps.event.addDomListener(window, 'load', initialize)--}}
    {{--</script>--}}

@endpush
