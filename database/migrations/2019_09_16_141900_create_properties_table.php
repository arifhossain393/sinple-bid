<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('agent_id')->unsigned();
            $table->foreign('agent_id')->on('agents')->references('id')->onDelete('cascade');
            $table->string('name');
            $table->string('address');
            $table->string('postcode');
            $table->timestamp('start_date')->nullable()->default(null);
            $table->timestamp('end_date')->nullable()->default(null);
            $table->integer('price')->nullable();
            $table->integer('bed')->nullable();
            $table->integer('bath')->nullable();
            $table->integer('living')->nullable();
            $table->string('outdoor')->nullable();
            $table->boolean('furnishing')->comment('1=>yes,0=>no');
            $table->boolean('type')->comment('1=>sale,0=>rent');
            $table->boolean('bid_type')->comment('1=>multiple,0=>single');
            $table->text('features')->nullable();
            $table->string('image')->default('default.png');
            $table->text('description')->nullable();
            $table->string('document')->nullable();
            $table->boolean('status')->default(1)->comment('active=>1,inactive=>0');
            $table->string('latitude',25)->nullable();
            $table->string('longitude',25)->nullable();
            $table->timestamps();
        });

        DB::statement("ALTER TABLE properties AUTO_INCREMENT = 99775501;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
