<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('c_name');
            $table->string('f_name');
            $table->string('l_name');
            $table->text('address_1');
            $table->text('address_2');
            $table->string('city');
            $table->string('postcode');
            $table->string('county');
            $table->string('country');
            $table->string('branch');
            $table->string('image');
            $table->string('phone');
            $table->string('latitude',25)->nullable();
            $table->string('longitude',25)->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('token',60);

            $table->boolean('status')->default(false);
            $table->boolean('confirm_status')->default(false);
            $table->text('description');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent');
    }
}
