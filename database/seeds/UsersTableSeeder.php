<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => '1',
            'name' => 'Mr.Buyer',
            'username' => 'buyer',
            'email' => 'buyer@gmail.com',
            'password' => bcrypt('123456')
        ]);
        DB::table('users')->insert([
            'role_id' => '2',
            'name' => 'Mr.Tenant',
            'username' => 'tenant',
            'email' => 'tenant@gmail.com',
            'password' => bcrypt('123456')
        ]);
    }
}
