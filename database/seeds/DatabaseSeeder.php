<?php

use App\Admin;
use App\Agent;
use App\Country;
use App\County;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       // $this->call(UsersTableSeeder::class);
        $this->call(PermissionTableSeeder::class);

        factory(Admin::class,1)->create();
        factory(Agent::class,1)->create();
        factory(Country::class,20)->create();
        factory(County::class,20)->create();

    }
}
