<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Admin;
use App\Agent;
use App\Country;
use App\County;
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

//$factory->define(User::class, function (Faker $faker) {
//    return [
//        'name' => $faker->name,
//        'email' => $faker->unique()->safeEmail,
//        'email_verified_at' => now(),
//        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
//        'remember_token' => Str::random(10),
//    ];
//});
$factory->define(Admin::class, function (Faker $faker) {
    return [
        'name' => 'admin',
        'email' => 'admin@admin.com',
        'password' => '$2y$12$nqa2FrrndfWGc.8TpxbE3.x0GWJ.ji3hk.cVqFJ54eaP/fO070t9i', // password
        'is_super'=>1,
        'remember_token' => Str::random(10),
        'token'=>Str::random(60),
    ];
});
$factory->define(Agent::class, function (Faker $faker) {
    return [
        'c_name' => 'REMAX Ambassador',
        'f_name'=>'REMAX',
        'l_name'=>'Ambassador',
        'address_1'=>'Luton, UK',
        'address_2'=>'Luton, UK',
        'city'=>'Luton, UK',
        'postcode'=>'LU11BB',
        'county'=>'LU11BB',
        'country'=>'United Kingdom',
        'branch'=>'1',
        'phone'=>'03331231111',
        'email' => 'agent@agent.com',
        'password' => '$2y$12$nqa2FrrndfWGc.8TpxbE3.x0GWJ.ji3hk.cVqFJ54eaP/fO070t9i', // password
        'status'=>1,
        'description'=>'RE/MAX Ambassador is located in Luton, Bedfordshire. Approximately 30 miles northwest of London’s financial hub, Luton is both a borough and unitary authority of Bedfordshire. While the earliest settlement in the area can be traced back thousands of years, the establishment of the town dates t',
        'image'=>'default.png',
        'remember_token' => Str::random(10),
        'token'=>Str::random(60),
    ];
});
$factory->define(Country::class, function (Faker $faker) {
    return [
        'sortname' => 'sort',
        'name' => $faker->country,
    ];
});
$factory->define(County::class, function (Faker $faker) {
    return [
        'county' => $faker->state,
    ];
});
