<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    protected $guarded = [];
    public function agents(){
        return $this->hasMany(Agent::class,'county');
    }
}
