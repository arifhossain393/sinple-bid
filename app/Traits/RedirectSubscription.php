<?php


namespace App\Traits;


use App\Payment;

Trait RedirectSubscription
{
    public function redirect_subscription(){
            $payment = Payment::whereAgentId(1)
                ->wherePaymentStatus(1)
                ->latest('id')
//                  ->whereBetween('reservation_from', [$from, $to])
                ->first();

            $end_days = \Carbon\Carbon::now()->toDateTimeString();
            $day = \Carbon\Carbon::parse($payment->created_at)->diffInDays($end_days,$absolute = false);

            if($day && $day<30){
                return redirect()->route('agent.dashboard');
            }else{
                return redirect()->route('agent-subscription');
            }
        }

}
