<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];
    const PAYMENT_COMPLETED = 1;
    const PAYMENT_PENDING = 0;
    protected $dates = ['deleted_at'];


}
