<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $guarded = [];

    public function admins()
    {
        return $this->hasOne(Admin::class);
    }
    public function permission(){
        return $this->hasOne(Permission::class);
    }

}
