<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $guarded = [];
    protected $casts = [
        'image'=>'json'
    ];

    public function agent(){
        return $this->belongsTo(Agent::class, 'agent_id', 'id');
    }
    public function bids(){
        return $this->hasMany(Bid::class);
    }
    public function favorites(){
        return $this->hasMany(Favorite::class);
    }
}
