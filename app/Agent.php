<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Agent extends Authenticatable
{
    use Notifiable;

    protected $guard = 'agent';

    protected $guarded = [];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function properties(){
        return $this->hasMany(Property::class, 'agent_id', 'id');
    }
   public function specific_property(){
        return $this->properties()->latest()->take(3);
   }
    public function agent_county(){
        return $this->belongsTo(County::class,'county');
    }


}
