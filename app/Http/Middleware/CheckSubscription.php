<?php

namespace App\Http\Middleware;

use App\Subscription;
use Closure;
use App\Payment;
use Illuminate\Support\Facades\Auth;
class CheckSubscription
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = null)
    {
                $payment = Payment::whereAgentId(auth()->guard('agent')->user()->id)
                    ->wherePaymentStatus(1)
                    ->latest('id')
//                  ->whereBetween('reservation_from', [$from, $to])
                    ->first();
                  $start_date =  $payment!=null ?  $payment->created_at : 0;
                $end_days = \Carbon\Carbon::now()->toDateTimeString();
                $day = \Carbon\Carbon::parse($start_date)->diffInDays($end_days, $absolute = false);
                if ($day && $day > 30) {
                    return response()->view('agent-subscripton');
                }

        return $next($request);

    }
}
