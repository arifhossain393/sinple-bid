<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Payment;
class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
//    public function handle($request, Closure $next, $guard = null)
//    {
//        if ($guard == "admin" && Auth::guard($guard)->check()) {
//            return redirect('/admin');
//        }
//        elseif ($guard == "agent" && Auth::guard($guard)->check()) {
//            return redirect('/agent');
//        }
//        elseif (Auth::guard($guard)->check() && Auth::user()->role->id==1) {
//            return redirect()->route('buyer.dashboard');
//        }elseif (Auth::guard($guard)->check() && Auth::user()->role->id==2){
//            return redirect()->route('tenant.dashboard');
//        }else{
//            return $next($request);
//        }
//
//
//    }
    public function handle($request, Closure $next, $guard = null)
    {
        switch ($guard) {
            case 'admin':
                if (Auth::guard($guard)->check()) {
                    return redirect()->route('admin.dashboard');
                }
                break;
            case 'agent':
                if (Auth::guard($guard)->check()) {
                    return redirect()->route('agent.dashboard');
                }
                break;
            default:
//                return redirect()->route('user.dashboard');
                break;
        }
        return $next($request);
    }
}

