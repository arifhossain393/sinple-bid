<?php

namespace App\Http\Controllers;

use App\Order;
use App\Payment;
use App\Subscription;
use Illuminate\Http\Request;

use App\PayPal;

class PaymentController extends Controller
{


    public function form(Request $request, $service_id)
    {
        if(!auth()->guard('agent')->check()) {
            return abort(401);
        }

           $service = Subscription::findOrFail($service_id);

           $order = new Order();
           $transaction_id = rand(10000000, 99999999);
           $order->agent_id = auth()->guard('agent')->user()->id;  //user id
           $order->transaction_id = $transaction_id;
           $order->service_id = $service->id;
           $order->amount = $service->price;
          // $order->amount = $service->property;
           $order->save();

           // the above order is just for example.
           return view('agent.plan.show', compact('service', 'transaction_id'));


    }

    /**
     * @param $order_id
     * @param Request $request
     */
    public function checkout($transaction_id, Request $request)
    {
        if(!auth()->guard('agent')->check()) {
            return abort(401);
        }
            $order = Order::where('transaction_id', decrypt($transaction_id))->first();

            $paypal = new PayPal;

            $response = $paypal->purchase([
                'amount' => $paypal->formatAmount($order->amount),
                'transactionId' => $order->transaction_id,
                'currency' => 'GBP',
                'cancelUrl' => $paypal->getCancelUrl($order),
                'returnUrl' => $paypal->getReturnUrl($order),
            ]);

            if ($response->isRedirect()) {
                $response->redirect();
            }

            return redirect()->back()->with([
                'message' => $response->getMessage(),

            ]);

    }

    /**
     * @param $order_id
     * @param Request $request
     * @return mixed
     */
    public function completed($order_id, Request $request)
    {
        if(!auth()->guard('agent')->check()) {
            return abort(401);
        }
        $order = Order::findOrFail($order_id);

        $paypal = new PayPal;

        $response = $paypal->complete([
            'amount' => $paypal->formatAmount($order->amount),
            'transactionId' => $order->transaction_id,
            'currency' => 'GBP',
            'cancelUrl' => $paypal->getCancelUrl($order),
            'returnUrl' => $paypal->getReturnUrl($order),
            'notifyUrl' => $paypal->getNotifyUrl($order),
        ]);



        if ($response->isSuccessful()) {
            $order->update([
                'transaction_id' => $response->getTransactionReference(),
                'payment_status' => Order::PAYMENT_COMPLETED,
            ]);

            return redirect()->route('paymentCompleted', encrypt($order_id))->with([
                'message' => 'You recent payment is sucessful with reference code ' . $response->getTransactionReference(),
            ]);
        }

        return redirect()->back()->with([
            'message' => $response->getMessage(),
        ]);

    }

    public function paymentCompleted($order)
    {
        if(!auth()->guard('agent')->check()) {
            return abort(401);
        }
        $order_id = decrypt($order);

        $order = Order::findOrFail($order_id);
        if ($payment = Payment::where('transaction_id', $order->transaction_id)->first()) {
            $payment_id = $payment->id;
        } else {
            $payment = new Payment;
            $payment->agent_id = $order->agent_id;
            $payment->transaction_id = $order->transaction_id;
            $payment->payment_status = $order->payment_status;
            $payment->payment_amount = $order->amount;
            $payment->subscription_id = $order->service_id;
            $payment->save();

        }
        toast()->success('You have successfully subscribed');
        return redirect()->route('agent.dashboard');

    }

    /**
     * @param $order_id
     */
    public function cancelled($order_id)
    {
        if(!auth()->guard('agent')->check()) {
            return abort(401);
        }
        $order = Order::findOrFail($order_id);

        return redirect()->route('order.paypal', encrypt($order_id))->with([
            'message' => 'You have cancelled your recent PayPal payment !',
        ]);
    }

    /**
     * @param $order_id
     * @param $env
     * @param Request $request
     */
    public function webhook($order_id, $env, Request $request)
    {
        if(!auth()->guard('agent')->check()) {
            return abort(401);
        }
        // to do with new release of sudiptpa/paypal-ipn v3.0 (under development)
    }


    public function paymentInfo(Request $request)
    {
        if(!auth()->guard('agent')->check()) {
            return abort(401);
        }
        if ($request->tx) {
            if ($payment = Payment::where('transaction_id', $request->tx)->first()) {
                $payment_id = $payment->id;
            } else {
                $payment = new Payment;
                $payment->item_number = $request->item_number;
                $payment->transaction_id = $request->tx;
                $payment->currency_code = $request->cc;
                $payment->payment_status = $request->st;
                $payment->save();
                $payment_id = $payment->id;
            }
            return 'Pyament has been done and your payment id is : ' . $payment_id;
        } else {
            return 'Payment has failed';
        }


    }
}
