<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Country;
use App\County;
use App\Notifications\AgentConfirmation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Traits\LatLng;
use Notification;

class AgentSignUpCorntroller extends Controller
{
    use LatLng;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $countries = Country::all();
        $counties = County::all();
        return view('agent-sign-up',compact('countries','counties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $this->agent_validation($request);

        $lat_lang =  $this->get_latitude_longitude($request->address_1);
        $latitude =  $lat_lang['lat'];
        $longitude =  $lat_lang['lng'];
        $location = public_path('uploads/agent');
        if($request->hasFile('image')){
            $img = $request->file('image');
            $name = $img->getClientOriginalName();
            $img_name = time()."_".$name;
            $img->move($location,$img_name);
        }


        unset($request['confirm_password']);
        $request['token'] = str_random(60);
        $request['password'] = Hash::make($request->password);
        $request['latitude'] = $latitude;
        $request['longitude'] = $longitude;
        $request['longitude'] = $longitude;
        $agent = Agent::create(array_merge($request->all(), ['image' => $img_name]));

        //$agent = Auth::guard('agent')->user()->id;
        $agent = Agent::findorfail($agent->id);
        $agent->notify(new AgentConfirmation($agent));
        toast()->success('Congratulation Please Check Your Email ');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agent = Agent::findOrFail($id);

        if($agent->confirm_status==0){
            $confirm_status = '1';
        }else{
            return redirect()->route('agent.auth.login');
        }
        $agent->confirm_status = $confirm_status;
        $agent->save();
        toast()->success('Status Successfully Updated');
        return redirect()->route('agent.auth.login');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function agent_validation($request,$id=null){

        if($request->method()=='POST'){
            $email = 'required|string|email|max:255|unique:agents';
            $img = 'required|max:2000';
            $password = 'required|max:19|min:8';
            $confirm_password = 'required|min:8|max:19|same:password';
        }elseif ($request->method()=='PUT' || $request->method()=='PATCH'){
            $email = 'required|string|email|max:255|unique:agents,email,'.$id;
            $img = 'max:2000';
            $password = 'nullable';
            $confirm_password = 'nullable';
        }
        $request->validate([
            'c_name'=>'required|min:5|max:255',
            'f_name'=>'required|min:5|max:255',
            'l_name'=>'required|min:5|max:255',
            'email' => $email,
            'address_1'=>'required|min:5|max:255',
            // 'address_2'=>'required|min:5|max:255',
            'postcode'=>'required|min:4|max:40',
            'country'=>'required',
            'county'=>'required',
            'image'=>$img,
            'phone'=>'required|min:9',
            'description'=>'required|min:10|max:10000',
            'password'=>$password,
            'confirm_password'=>$confirm_password

        ],
        [
            'c_name.required'=>'Company name field required',
            'f_name.required'=>'First name field required',
            'l_name.required'=>'Last name field required',
            'address_1.required'=>'Address field required',
        ]);
    }

    public function unlink_image($agent){
        $document = public_path("uploads/agent/{$agent->image}");
        if (file_exists($document)) {
            @unlink($document);
        }

    }

    public function agent_confirmation($id=null){
        $agent = Agent::findOrFail($id);
        if($agent->confirm_status==0){
            $confirm_status = '1';
        }else{
            $confirm_status = '0';
        }
        $agent->confirm_status = $confirm_status;
        $agent->save();
        toast()->success('Status Successfully Updated');
        return redirect()->route('agent.auth.login');
    }
}
