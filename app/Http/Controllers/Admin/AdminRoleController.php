<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdminRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $admins = Admin::latest()->get();
        return view('admin.admin.index',compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        return view('admin.admin.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();

        $this->admin_validation($request);

        $request['token'] = str_random(60);
        unset($request['confirm_password']);
        $request['password'] = Hash::make($request->password);
        Admin::create(array_merge($request->all()));
        toast()->success('User Successfully Added');
        return redirect()->route('adminRole.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::get();
        $admin = Admin::findorfail($id);
        return view('admin.admin.edit',compact('admin','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin  = Admin::findOrFail($id);
        $this->admin_validation($request);
        unset($request['confirm_password']);
        $request['password'] = $request->password ?  Hash::make($request->password): $admin->password;
        $admin->update(array_merge($request->all()));
        toast()->success('User Successfully Added');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $admin = Admin::findorfail($id);
        $admin->delete();
        toast()->success('User Successfully Deleted');
        return redirect()->back();
    }

    public function admin_validation($request,$id=null){
        if($request->method()=='POST'){
            $email = 'required|string|email|max:255|unique:admins';

            $password = 'required|max:19|min:8';
            $confirm_password = 'required|min:8|max:19|same:password';
        }elseif ($request->method()=='PUT' || $request->method()=='PATCH'){
            $email = 'required|string|email|max:255|unique:users,email,'.$id;

            $password = 'nullable';
            $confirm_password = 'nullable|same:password';
        }
        $request->validate([
            'name'=>'required|min:5|max:255',

            'email' => $email,
            'password' => $password,
            'confirm_password' => $confirm_password,
            'role_id'=>'required',
        ]);
    }
}
