<?php

namespace App\Http\Controllers\Admin;

use App\Agent;
use App\Bid;
use App\Property;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $properties = Property::latest()->paginate(10);
        return view('admin.bid.list',compact('properties'));

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bid_status_change(Request $request){
        $bid = Bid::findOrFail($request->id);
        $bid->status = $request->status;
        $bid->save();
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search_bid_property(Request $request){
        $searchTerm = $request->keyword;

        $properties = Property::where('name', 'LIKE', "%{$searchTerm}%")
            ->orWhere('price', 'LIKE', "%{$searchTerm}%")
            ->orWhere('id', 'LIKE', "%{$searchTerm}%")
            ->paginate(10);
        return view('admin.bid.list',compact('properties'));
    }

}
