<?php

namespace App\Http\Controllers\Admin;

use App\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::latest()->get();
        return view('admin.permission.index',compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.permission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = Permission::where('role_id',$request->role_id)->first();
        if($check){
            toast('Role Already aded please edit role or create a new role!!','warning');
            return redirect()->back();
        }
        $request->validate([
            'role_id'=>'required',
        ]);
        $permission = new Permission();
        $permission->role_id = $request->role_id;
        $permission->permission = $request->permission;
        $permission->save();
        toast('Permission Added Successfully!!','success');
        return redirect()->route('adminPermission.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission,$id)
    {
        $permission = Permission::findOrFail($id);
        return view('admin.permission.edit',compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission,$id)
    {
        $request->validate([
            'role_id'=>'required',
        ]);
        $permission = Permission::whereId($id)->first();
        $permission->update($request->all());
        toast('Permission Updated Successfully!!','success');
        return redirect()->route('adminPermission.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission,$id)
    {
        $permission = Permission::findOrFail($id);
        $permission->delete();
        toast('Permission Deleted Successfully!!','success');
        return redirect()->route('adminPermission.index');

    }
}
