<?php
namespace App\Http\Controllers\Admin;
use App\Agent;
use App\Bid;
use App\Property;
use App\User;
use Illuminate\Http\Request;
use App\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
class AdminController extends Controller
{
    use RegistersUsers;
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $bidCount = Bid::count();
        $userCount = User::count();
        $propertyCount = Property::count();
        $agentCount = Agent::count();
        return view('admin',compact('agentCount','propertyCount','userCount','bidCount'));
    }

    public function create()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        $this->validator($request->all())->validate();
        $admin = Admin::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        return redirect()->intended('admin.auth.login');
    }



}
