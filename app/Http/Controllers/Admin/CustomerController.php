<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use App\Notifications\AdminCustomerApproval;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{

    public function index()
    {
        $users = User::where('confirm_status',1)->latest()->get();
        return view('admin.manage-customer.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $countries = Country::get();
        return view('admin.manage-customer.create',compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//    return $request->all();
        $img_name = '';
        $this->user_validation($request);
        $location = public_path('uploads/user');
        if($request->hasFile('image')){
            $img = $request->file('image');
            $name = $img->getClientOriginalName();
            $img_name = time()."_".$name;
            $img->move($location,$img_name);
        }
        $request['token'] = str_random(60);
        unset($request['confirm_password']);
        $request['password'] = Hash::make($request->password);
        User::create(array_merge($request->all(), ['image' => $img_name]));
        toast()->success('Customer Successfully Added');
        return redirect()->route('customer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $countries = Country::get();
        $user = User::findorfail($id);
        return view('admin.manage-customer.edit',compact('countries','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return $request->all();
        $user = User::findOrFail($id);
        $this->user_validation($request,$id);
        $location = public_path('uploads/user');
        if($request->hasFile('image')){
            $this->unlink_image($user);
            $img = $request->file('image');
            $name = $img->getClientOriginalName();
            $img_name = time()."_".$name;
            $img->move($location,$img_name);
        }else{
            $img_name = $user->image;
        }

        $user->update(array_merge($request->all(), ['image' => $img_name]));

        toast()->success('Customer Successfully updated');
        return redirect()->route('customer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $this->unlink_image($user);
        $user->delete();
        toast()->success('Customer Successfully Deleted');
        return redirect()->back();
    }

    public function user_validation($request,$id=null){
        if($request->method()=='POST'){
            $email = 'required|string|email|max:255|unique:users';
            $img = 'required|max:2000';
            $password = 'required|max:19|min:8';
            $confirm_password = 'required|min:8|max:19|same:password';
        }elseif ($request->method()=='PUT' || $request->method()=='PATCH'){
            $email = 'required|string|email|max:255|unique:users,email,'.$id;
            $img = 'max:2000';
            $password = 'nullable';
            $confirm_password = 'nullable';
        }
        $request->validate([
            'f_name'=>'required|min:5|max:255',
            'l_name'=>'required|min:5|max:255',
            'email' => $email,
            'password' => $password,
            'confirm_password' => $confirm_password,
            'address_1'=>'required|min:5|max:255',
            //'address_2'=>'required|min:5|max:255',
            'postcode'=>'required|min:4|max:40',
            'country'=>'required',
            'role_id'=>'required',
            'city'=>'required',
            //'image'=>$img,
            'phone'=>'required|min:9',
            'about'=>'required|min:10|max:10000',
        ],
            [

                'role_id.required'=>'The user type field required',
                'f_name.required'=>'First name field required',
                'l_name.required'=>'Last name field required',
                'address_1.required'=>'Address field required',

            ]);
    }


    public function unlink_image($user){
        $document = public_path("uploads/user/{$user->image}");
        if (file_exists($document)) {
            @unlink($document);
        }

    }

    public function user_active_deactive($id=null){
        $user = User::findOrFail($id);
        if($user->status==0){
            $status = '1';
        }else{
            $status = '0';
        }
        $user->status = $status;
        $user->save();
        $user = User::findorfail($user->id);
        $user->notify(new AdminCustomerApproval($user));
        toast()->success('Status Successfully Updated');
        return redirect()->back();
    }
}

