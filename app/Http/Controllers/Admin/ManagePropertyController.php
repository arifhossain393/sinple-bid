<?php

namespace App\Http\Controllers\Admin;

use App\Property;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mapper;
use App\Traits\LatLng;
class ManagePropertyController extends Controller
{
use LatLng;
    public function index()
    {
        $properties = Property::latest()->get();
        return view('admin.manage-property.index',compact('properties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $property = Property::findorfail($id);
        $lat_lang =  $this->get_latitude_longitude($property->address);
        $latitude =  $lat_lang['lat'];
        $longitude =  $lat_lang['lng'];
        return view('admin.manage-property.show',compact('property','latitude','longitude'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $property = Property::findOrFail($id);
        $this->unlink_image($property);
        $this->unlink_document($property);
        $property->delete();
        toast()->success('Property Successfully Deleted');
        return redirect()->back();
    }
    public function unlink_document($property){
        $document = public_path("uploads/property/{$property->document}");
        if (file_exists($document)) {
            @unlink($document);
        }
    }
    public function unlink_image($property){
        foreach ($property->image as $image){
            $img = public_path("uploads/property/{$image}");
            if (file_exists($img)) {
                @unlink($img);
            }
        }
    }
}
