<?php

namespace App\Http\Controllers\admin;

use App\Admin;
use App\Country;
use App\County;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    public function index($id)
    {
        $admin = Admin::findorfail($id);
        return view('admin.settings.settings',compact('admin'));
    }

    public function updateProfile(Request $request, $id)
    {
        $this->admin_validation($request,$id);
        $admin = Admin::findOrFail($id);

        $admin->update(array_merge($request->all()));

        toast()->success('Your profile Successfully updated');
        return redirect()->back();
    }

    public function updatePassword(Request $request, $id)
    {
        $request->validate([
            'old_password'=>'min:8|max:19|required',
            'password'=>'min:8|max:19|required',
            'confirm_password'=>'min:8|max:19|required|same:password',
        ]);
        $admin = Admin::findOrFail($id);
        $check = Hash::check($request->old_password,$admin->password);
        $new_hash_password = Hash::check($request->password,$admin->password);
        $new_hash_make =  Hash::make($request->password);
        if ($new_hash_password){
            toast()->warning('Old Password and New Password Are Same...!');
            return redirect()->back()->with('active','active');
        }else if($check){
            $admin->password = $new_hash_make;
            $admin->save();
            toast()->success('Your Password Successfully Changed');
            return redirect()->back()->with('active','active');
        }
        else{
            toast()->warning('Old Password Not Match...!');
            return redirect()->back()->with('active','active');
        }
    }


    public function admin_validation($request,$id=null){
        if($request->method()=='POST'){

            $email = 'required|string|email|max:255|unique:agents';
        }elseif ($request->method()=='PUT' || $request->method()=='PATCH'){
            $email = 'required|string|email|max:255|unique:agents,email,'.$id;

        }
        $request->validate([
            'name'=>'required|min:5|max:255',
            'email' => $email,
        ]);
    }
}
