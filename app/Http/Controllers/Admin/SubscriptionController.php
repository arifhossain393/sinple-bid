<?php

namespace App\Http\Controllers\Admin;

use App\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscriptions = Subscription::latest()->get();
        return view('admin.subscription.subscription_list',compact('subscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.subscription.create_subscription');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->package_validation($request);

       $location = public_path('uploads/subscription');

        if($request->hasFile('image')){
            $doc = $request->file('image');
            $name = $doc->getClientOriginalName();
            $document = time()."_".$name;
            $doc->move($location,$document);
        }
        Subscription::create(array_merge($request->all(),['image'=>$document]));
        toast()->success('Subscription Successfully Added');
        return redirect()->route('subscription.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subscription = Subscription::findOrFail($id);
        return view('admin.subscription.edit_subscription',compact('subscription'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subscription = Subscription::findOrFail($id);
        $location = public_path('uploads/subscription');
        $image = '';
        $this->package_validation($request);
        if($request->hasFile('image')){
            $this->unlink_image($subscription);
            $doc = $request->file('image');
            $name = $doc->getClientOriginalName();
            $image = time()."_".$name;
            $doc->move($location,$image);
        }else{
            $image = $subscription->image;
        }
        $subscription->update(array_merge($request->all(),['image'=>$image]));
        toast()->success('Subscription Successfully Added');
        return redirect()->route('subscription.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subscription = Subscription::findOrFail($id);
        $this->unlink_image($subscription);
        $subscription->delete();
        toast()->success('Subscription Successfully Deleted');
        return redirect()->route('subscription.index');
    }

    public  function package_validation($request){
        $img = '';
        if($request->method()=='PUT'){
            $img = 'max:2000';
        }elseif($request->method()=='POST'){
            $img = 'required|max:2000';
        }
        $request->validate([
            'name'=>'required|min:5|max:255',
            'status'=>'required',
            'description'=>'required|min:10',
            'price'=>'required',
            'property'=>'required',
            //'days'=>'required|integer',
            'image'=>$img
        ]);
    }


    public function unlink_image($subscription){
        $img = public_path("uploads/subscription/{$subscription->image}");
        if (file_exists($img)) {
            @unlink($img);
        }
    }

}
