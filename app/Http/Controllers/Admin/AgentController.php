<?php

namespace App\Http\Controllers\Admin;

use App\Agent;
use App\Country;
use App\County;
use App\Notifications\AdminApproval;
use App\Notifications\AgentConfirmation;
use App\Property;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Traits\LatLng;

class AgentController extends Controller
{
   use LatLng;

    public function index()
    {
        $agents = Agent::where('confirm_status',1)->latest()->get();
        return view('admin.manage-agent.index',compact('agents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::get();
        $county = County::get();
        return view('admin.manage-agent.create',compact('countries','county'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->agent_validation($request);
        $lat_lang =  $this->get_latitude_longitude($request->address_1);
        $latitude =  $lat_lang['lat'];
        $longitude =  $lat_lang['lng'];


        $location = public_path('uploads/agent');
        if($request->hasFile('image')){
            $img = $request->file('image');
            $name = $img->getClientOriginalName();
            $img_name = time()."_".$name;
            $img->move($location,$img_name);
        }

        unset($request['confirm_password']);
        $request['token'] = str_random(60);
        $request['password'] = Hash::make($request->password);
        $request['latitude'] = $latitude;
        $request['longitude'] = $longitude;
         Agent::create(array_merge($request->all(), ['image' => $img_name]));

        toast()->success('Agent Successfully Added');
        return redirect()->route('agent.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $property = Property::get();
        $agent = Agent::get($id);
        return view('admin.manage-agent.show',compact('property','agent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $countries = Country::get();
        $county = county::get();
        $agent = Agent::findOrFail($id);
        return view('admin.manage-agent.edit',compact('agent','countries','county'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->agent_validation($request,$id);
        $lat_lang =  $this->get_latitude_longitude($request->address_1);
        $latitude =  $lat_lang['lat'];
        $longitude =  $lat_lang['lng'];
        $agent = Agent::findOrFail($id);
        $location = public_path('uploads/agent');
        $img_name ='';
        if($request->hasFile('image')){
            $this->unlink_image($agent);
            $img = $request->file('image');
            $name = $img->getClientOriginalName();
            $img_name = time()."_".$name;
            $img->move($location,$img_name);
        }else{
            $img_name = $agent->image;
        }
        $request['latitude'] = $latitude;
        $request['longitude'] = $longitude;
        $agent->update(array_merge($request->all(), ['image' => $img_name]));

        toast()->success('Agent Successfully updated');
        return redirect()->route('agent.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $agent = Agent::findOrFail($id);
        $this->unlink_image($agent);
        $agent->delete();
        toast()->success('Agent Successfully Deleted');
        return redirect()->back();
    }



    public function agent_validation($request,$id=null){

        if($request->method()=='POST'){
            $email = 'required|string|email|max:255|unique:agents';
            $img = 'required|max:2000';
            $password = 'required|max:19|min:8';
            $confirm_password = 'required|min:8|max:19|same:password';
        }elseif ($request->method()=='PUT' || $request->method()=='PATCH'){
            $email = 'required|string|email|max:255|unique:agents,email,'.$id;
            $img = 'max:2000';
            $password = 'nullable';
            $confirm_password = 'nullable';
        }
        $request->validate([
            'c_name'=>'required|min:5|max:255',
            'f_name'=>'required|min:5|max:255',
            'l_name'=>'required|min:5|max:255',
            'email' => $email,
            'address_1'=>'required|min:5|max:255',
            // 'address_2'=>'required|min:5|max:255',
            'postcode'=>'required|min:4|max:40',
            'country'=>'required',
            'county'=>'required',
            'image'=>$img,
            'phone'=>'required|min:9',
            'description'=>'required|min:10|max:10000',
            'password'=>$password,
            'confirm_password'=>$confirm_password

        ],
            [
                'c_name.required'=>'Company name field required',
                'f_name.required'=>'First name field required',
                'l_name.required'=>'Last name field required',
                'address_1.required'=>'Address field required',
            ]);
    }

    public function unlink_image($agent){
        $document = public_path("uploads/agent/{$agent->image}");
        if (file_exists($document)) {
            @unlink($document);
        }

    }
    public function agent_active_deactive($id=null){
        $agent = Agent::findOrFail($id);
        if($agent->status==0){
            $status = '1';
        }else{
            $status = '0';
        }
        $agent->status = $status;
        $agent->save();
        $agent = Agent::findorfail($agent->id);
        $agent->notify(new AdminApproval($agent));
        toast()->success('Status Successfully Updated');
        return redirect()->back();
    }
}
