<?php

namespace App\Http\Controllers;

use App\Property;
use App\Review;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        $review = Review::where('status',1)->take(3)->get();
        $properties = Property::latest()->take(6)->get();
        $sale_properties = Property::latest()->whereType(1)->take(4)->get();
        $rent_properties = Property::latest()->whereType(0)->take(4)->get();
        return view('welcome',compact('properties','sale_properties','rent_properties','review'));
    }

    public function work()
    {
        return view('how-it-works');
    }
    public function faq()
    {
        return view('faq');
    }
    public function resourse()
    {
        return view('resource');
    }
    public function terms()
    {
        return view('terms');

    }
    public function privacy()
    {
        return view('privacy');
    }

}
