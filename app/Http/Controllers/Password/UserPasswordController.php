<?php

namespace App\Http\Controllers\Password;

use App\Notifications\UserPasswordResetNotification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserPasswordController extends Controller
{
    public function reset_password_form(){
        return view('auth.passwords.user_email');
    }
    public function submit_password_form(Request $request){
       $user = User::whereEmail($request->email)->first();
        if ($user){
            $user->notify(new UserPasswordResetNotification($user));
            return back()->with('success','Please check your mail account...');
        }else{
            return back()->with('error','Email doesn\'t match in our record');
        }

    }
    public function mail_reset_password_form($token=null){

        return view('auth.passwords.user_reset',compact('token'));
    }
    public function mail_submit_password_form(Request $request){
        $request->validate([
            'email'=>'required',
            'password'=>'required|min:4|max:19',
            'confirm_password'=>'required|same:password',
        ]);
        $user = User::whereEmail($request->email)->whereToken($request->token)->first();
        if($user) {
            $user->password = Hash::make($request->password);
            $user->token = str_random(60);
            $user->save();
            return redirect()->route('login')->with('success','Password Successfully Reset Please login Your Account');
        }
        return back()->with('error','Email or token not match ...!');
    }
}
