<?php

namespace App\Http\Controllers\Password;


use App\Admin;
use App\Notifications\AdminPasswordResetNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdminPasswordController extends Controller
{
    public function reset_password_form(){
        return view('auth.passwords.admin_email');
    }
    public function submit_password_form(Request $request){
        $admin = Admin::whereEmail($request->email)->first();
        if ($admin){
            $admin->notify(new AdminPasswordResetNotification($admin));
            return back()->with('success','Please check your mail account...');
        }else{
            return back()->with('error','Email doesn\'t match in our record');
        }

    }
    public function mail_reset_password_form($token=null){

        return view('auth.passwords.admin_reset',compact('token'));
    }
    public function mail_submit_password_form(Request $request){
        $request->validate([
            'email'=>'required',
            'password'=>'required|min:4|max:19',
            'confirm_password'=>'required|same:password',
        ]);
        $admin = Admin::whereEmail($request->email)->whereToken($request->token)->first();
        if($admin) {
            $admin->password = Hash::make($request->password);
            $admin->token = str_random(60);
            $admin->save();
            return redirect()->route('admin.auth.login')->with('success','Password Successfully Reset Please login Your Account');
        }
        return back()->with('error','Email or token not match ...!');
    }
}
