<?php

namespace App\Http\Controllers\Password;

use App\Agent;
use App\Notifications\AgentPasswordResetNotification;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AgentPasswordController extends Controller
{
    public function reset_password_form(){
        return view('auth.passwords.agent_email');
    }
    public function submit_password_form(Request $request){
        $agent = Agent::whereEmail($request->email)->first();
        if ($agent){
            $agent->notify(new AgentPasswordResetNotification($agent));
            return back()->with('success','Please check your mail account...');
        }else{
            return back()->with('error','Email doesn\'t match in our record');
        }

    }
    public function mail_reset_password_form($token=null){

        return view('auth.passwords.agent_reset',compact('token'));
    }
    public function mail_submit_password_form(Request $request){
        $request->validate([
            'email'=>'required',
            'password'=>'required|min:4|max:19',
            'confirm_password'=>'required|same:password',
        ]);
        $agent = Agent::whereEmail($request->email)->whereToken($request->token)->first();
        if($agent) {
            $agent->password = Hash::make($request->password);
            $agent->token = str_random(60);
            $agent->save();
            return redirect()->route('agent.auth.login')->with('success','Password Successfully Reset Please login Your Account');
        }
        return back()->with('error','Email or token not match ...!');
    }
}
