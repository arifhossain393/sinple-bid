<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Property;
use Illuminate\Http\Request;
use Mapper;

class SearchController extends Controller
{
    public function search_property(Request $request)
    {
        if ($request->latitude) {

            $address = $request->address;
            //$postcode = $request->postcode;
            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $type = $request->type ? $request->type : '';
            $distance = 40;

            $properties = Property::query()
                ->orWhere('type', $type)
                // ->orWhere('postcode', $postcode)
                ->whereRaw(\DB::raw("(3959 * acos( cos( radians($latitude) ) * cos( radians( latitude ) )  *
                          cos( radians( longitude ) - radians($longitude) ) + sin( radians($latitude) ) * sin(radians( latitude ) ) ) ) < $distance "))
                ->orderBy(\DB::raw('ABS(latitude - ' . (float) $latitude . ') + ABS(longitude - ' . (float) $longitude . ')'), 'DESC')
                ->paginate(9);
            return view('property_list')
                ->with(['properties' => $properties, 'latitude' => $latitude, 'longitude' => $longitude, 'address' => $address,]);
        } else {
            toast()->warning('Please choose one of the pre populated locations to continue with your search');
            return redirect()->back();
        }
    }
    public function find_agent_list(Request $request)
    {
        if ($request->latitude) {
            $address = $request->address;
            $postcode = $request->postcode;
            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $distance = $request->distance ? $request->distance : '40';
            $name = $request->agent_name ? $request->agent_name : '';
            $agents = Agent::query()
                ->where('status', 1)
                ->orWhere('postcode', $postcode)
                //                ->orWhere('f_name', 'LIKE', "%{$name}%")
                //                ->orWhere('l_name', 'LIKE', "%{$name}%")
                ->whereRaw(\DB::raw("(3959 * acos( cos( radians($latitude) ) * cos( radians( latitude ) )  *
                          cos( radians( longitude ) - radians($longitude) ) + sin( radians($latitude) ) * sin(radians( latitude ) ) ) ) < $distance "))
                ->orderBy(\DB::raw('ABS(latitude - ' . (float) $latitude . ') + ABS(longitude - ' . (float) $longitude . ')'), 'DESC')
                ->paginate(9);

            return view('agent-list', compact('agents'));
        } else {
            toast()->warning('Please choose one of the pre populated locations to continue with your search');
            return redirect()->back();
        }
    }
    public function search_all_property(Request $request)
    {
        if ($request->latitude) {
            $address = $request->address;
            //$postcode = $request->postcode;
            $latitude = $request->latitude;
            $longitude = $request->longitude;
            $distance = $request->distance ? $request->distance : '40';
            $min_price = $request->min_price ? $request->min_price : Property::min('price');
            $max_price = $request->max_price ? $request->max_price : Property::max('price');
            $min_bed = $request->min_bed ? $request->min_bed : Property::min('bed');
            $max_bed = $request->max_bed ? $request->max_bed : Property::max('bed');
            $properties = Property::query()
                ->whereBetween('price', [$min_price, $max_price])
                ->whereBetween('bed', [$min_bed, $max_bed])
                //->orWhere('postcode', $postcode)
                ->whereRaw(\DB::raw("(3959 * acos( cos( radians($latitude) ) * cos( radians( latitude ) )  *
                          cos( radians( longitude ) - radians($longitude) ) + sin( radians($latitude) ) * sin(radians( latitude ) ) ) ) < $distance "))
                ->orderBy(\DB::raw('ABS(latitude - ' . (float) $latitude . ') + ABS(longitude - ' . (float) $longitude . ')'), 'DESC')
                ->paginate(9);
            return view('property_list')
                ->with(['properties' => $properties, 'latitude' => $latitude, 'longitude' => $longitude, 'address' => $address]);
        } else {
            toast()->warning('Please choose one of the pre populated locations to continue with your search');
            return redirect()->back();
        }
    }
}
