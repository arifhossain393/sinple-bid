<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */


    protected $redirectTo='/user' ;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
//    public function __construct()
//    {
//
//        $this->middleware('guest')->except('logout');
//    }

    public function logout()
    {
//        Auth::guard('admin')->logout();
        $this->guard()->logout();
        return redirect('/login');

    }


//    public function logout(Request $request)
//    {
//        $this->guard()->logout();
//
//        $request->session()->invalidate();
//
//        return $this->loggedOut($request) ?: redirect('/');
//    }

}
