<?php

namespace App\Http\Controllers\Agent;

use App\Agent;
use App\Country;
use App\County;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    public function index($id)
    {
        $countries = Country::get();
        $county = County::get();
        $agent = Agent::findorfail($id);
        return view('agent.settings.settings',compact('agent','countries','county'));
    }

    public function updateProfile(Request $request, $id)
    {

        //return $request->all();
        $this->agent_validation($request,$id);
        $agent = Agent::findOrFail($id);
        $location = public_path('uploads/agent');
        $img_name ='';
        if($request->hasFile('image')){
            $this->unlink_image($agent);
            $img = $request->file('image');
            $name = $img->getClientOriginalName();
            $img_name = time()."_".$name;
            $img->move($location,$img_name);
        }else{
            $img_name = $agent->image;
        }

        $agent->update(array_merge($request->all(), ['image' => $img_name]));

        toast()->success('Your profile Successfully updated');
        return redirect()->back();
    }

    public function updatePassword(Request $request, $id)
{
    $request->validate([
        'old_password'=>'min:8|max:19|required',
        'password'=>'min:8|max:19|required',
        'confirm_password'=>'min:8|max:19|required|same:password',
    ]);
    $agent = Agent::findOrFail($id);
    $check = Hash::check($request->old_password,$agent->password);
    $new_hash_password = Hash::check($request->password,$agent->password);
    $new_hash_make =  Hash::make($request->password);
    if ($new_hash_password){
        toast()->warning('Old Password and New Password Are Same...!');
        return redirect()->back()->with('active','active');
    }else if($check){
        $agent->password = $new_hash_make;
        $agent->save();
        toast()->success('Your Password Successfully Changed');
        return redirect()->back()->with('active','active');
    }
    else{
        toast()->warning('Old Password Not Match...!');
        return redirect()->back()->with('active','active');
    }
}
    public function agent_validation($request,$id=null){
        if($request->method()=='POST'){
            $email = 'required|string|email|max:255|unique:agents';
            $img = 'required|max:2000';
            $password = 'required|max:19|min:8';
            $confirm_password = 'required|min:8|max:19|same:password';
        }elseif ($request->method()=='PUT' || $request->method()=='PATCH'){
            $email = 'required|string|email|max:255|unique:agents,email,'.$id;
            $img = 'max:2000';
            $password = 'nullable';
            $confirm_password = 'nullable';
        }
        $request->validate([
            'f_name'=>'required|min:5|max:255',
            'email' => $email,
            'image'=>$img,
            'phone'=>'required|min:9',
            'address_1'=>'required|min:5|max:255',

            'postcode'=>'required|min:4|max:40',
            'city'=>'required',
            'country'=>'required',
            'county'=>'required',
            'password'=>$password,
            'confirm_password'=>$confirm_password

        ]);
    }







    public function unlink_image($agent){
        $document = public_path("uploads/agent/{$agent->image}");
        if (file_exists($document)) {
            @unlink($document);
        }

    }
}
