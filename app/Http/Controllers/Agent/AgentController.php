<?php

namespace App\Http\Controllers\Agent;

use App\Agent;
use App\Payment;
use App\Subscription;
use App\Traits\RedirectSubscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;


class AgentController extends Controller
{
    use RegistersUsers,RedirectSubscription;

    public function index()
    {
        return view('agent');
    }


    public function create()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        $this->validator($request->all())->validate();
        $agent = Agent::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        return redirect()->intended('agent.auth.login');
    }

    private function validator($all)
    {
    }
    public function plan_show($id){
        $plan = Subscription::findOrFail($id);
        return view('agent.plan.show',compact('plan'));
    }

}
