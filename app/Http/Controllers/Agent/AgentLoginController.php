<?php

namespace App\Http\Controllers\Agent;

use App\Agent;
use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class AgentLoginController extends Controller
{
    /*
     |--------------------------------------------------------------------------
     | Login Controller
     |--------------------------------------------------------------------------
     |
     | This controller handles authenticating users for the application and
     | redirecting them to your home screen. The controller uses a trait
     | to conveniently provide its functionality to your applications.
     |
     */
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';
    public function __construct()
    {
        $this->middleware('guest:agent')->except('logout');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('auth.agent_login');
    }
    public function loginAgent(Request $request)
    {

        // Validate the form data
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:8'
        ]);
        // Attempt to log the user in
        $agent = Agent::whereEmail($request->email)->first();

        if($agent && $agent->status=='1'){
            if (Auth::guard('agent')->attempt(['email' => $request->email, 'password' => $request->password],false)) {
                // if successful, then redirect to their intended location
              
             return redirect()->route('agent.dashboard');

            }
        }else if($agent && $agent->status=='0'){
            toast('For login must need admin approval','warning');
            return redirect()->route('agent.auth.login');
        }

        // if unsuccessful, then redirect back to the login with the form data
        toast('Invalid email or password','warning');
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }
    public function logout()
    {

        Auth::guard('agent')->logout();
        return redirect()->route('agent.auth.login');

    }
}
