<?php

namespace App\Http\Controllers\Agent;

use App\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    public function index(){
        $subscriptions = Subscription::where('status',1)->latest()->get();
        return view('agent-subscripton',compact('subscriptions'));
    }
}
