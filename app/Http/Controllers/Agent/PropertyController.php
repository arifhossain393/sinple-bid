<?php

namespace App\Http\Controllers\Agent;

use App\Payment;
use App\Property;
use App\Subscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use Mapper;
use App\Traits\LatLng;
class PropertyController extends Controller
{
use LatLng;
    public function index()
    {
        $properties = Property::where('agent_id',auth()->guard('agent')->user()->id)->latest()->get();

        return view('agent.manage-property.index',compact('properties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agent.manage-property.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $subscription = Payment::where('agent_id',auth()->guard('agent')->user()->id)->latest()->first();
//        return $subscription;
//        $subscription = Subscription::findOrFail($subscription->id);
//        if($subscription->property)
        //return $request->address;

        $this->property_validation($request);
//        $lat_lang =  $this->get_latitude_longitude($request->address);
////        $latitude =  $lat_lang['lat'];
////        $longitude =  $lat_lang['lng'];

    $document = '';
        $files = $request->file('image');
        $location = public_path('uploads/property');
        foreach($files as $image)
        {
            $name = $image->getClientOriginalName();
            $file_name = time()."_".$name;
            $image->move($location,$file_name);
            $data[] = $file_name;
        }
//        document upload
        if($request->hasFile('document')){
            $doc = $request->file('document');
            $name = $doc->getClientOriginalName();
            $document = time()."_".$name;
            $doc->move($location,$document);
        }
//        $request['latitude'] = $latitude;
//        $request['longitude'] = $longitude;
        Property::create(array_merge($request->all(), ['image' => $data,'document'=>$document]));
        toast()->success('Property Successfully Added');
        return redirect()->route('property.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=null)
    {
        $property = Property::findorfail($id);
        $lat_lang =  $this->get_latitude_longitude($property->address);
        $latitude =  $lat_lang['lat'];
        $longitude =  $lat_lang['lng'];
        return view('agent.manage-property.show',compact('property','latitude','longitude'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $property = Property::findOrFail($id);
        return view('agent.manage-property.edit',compact('property'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $latitude =  '';
        $longitude =  '';
        $property = Property::findOrFail($id);
        if($request->address==$property->address){
            $latitude = $property->latitude;
            $longitude =  $property->longitude;;
        }else{
            $latitude = $request->latitude;
            $longitude =  $request->longitude;;
        }

        $this->property_validation($request);
//        $lat_lang =  $this->get_latitude_longitude($request->address);
//        $latitude =  $lat_lang['lat'];
//        $longitude =  $lat_lang['lng'];

        $location = public_path('uploads/property');
        $document = '';
        if($request->hasFile('image')){
            $files = $request->file('image');
            $this->unlink_image($property);
            foreach($files as $image)
            {
                $name = $image->getClientOriginalName();
                $file_name = time()."_".$name;
                $image->move($location,$file_name);
                $data[] = $file_name;
            }
        }else{
            $data = $property->image;
        }
//        document upload
        if($request->hasFile('document')){
            $this->unlink_document($property);
            $doc = $request->file('document');
            $name = $doc->getClientOriginalName();
            $document = time()."_".$name;
            $doc->move($location,$document);
        }else{
            $document =  $property->document;
        }
        $request['latitude'] = $latitude;
        $request['longitude'] = $longitude;
        $property->update(array_merge($request->all(), ['image' => $data,'document'=>$document]));
        toast()->success('Property Successfully Updated');
        return redirect()->route('property.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $property = Property::findOrFail($id);
        $this->unlink_document($property);
        $this->unlink_image($property);

        $property->delete();
        toast()->success('Property Successfully Deleted');
        return redirect()->back();
    }

    public  function property_validation($request){
        $img = '';
      if($request->method()=='PUT'){
          $img = 'max:2000';
      }elseif($request->method()=='POST'){
          $img = 'required|max:2000';
      }
        $request->validate([
            'name'=>'required|min:5|max:255',
            'address'=>'required|min:5|max:255',
            //'cities'=>'required|min:4|max:20',
            'postcode'=>'required|min:4|max:20',
            'start_date'=>'required|date',
            'end_date'=>'required|date|after_or_equal:start_date',
            'price'=>'required|int',
            'image'=>$img,
            'document'=>'max:2000|nullable',
            'description'=>'required|min:10|max:10000',
            'bed'=>'integer | nullable',
            'bath'=>'integer | nullable',
            'living'=>'integer | nullable',

        ]);
    }

    public function unlink_document($property){
        $document = public_path("uploads/property/{$property->document}");
        if (file_exists($document)) {
            @unlink($document);
        }
    }
    public function unlink_image($property){
        foreach ($property->image as $image){
            $img = public_path("uploads/property/{$image}");
            if (file_exists($img)) {
                @unlink($img);
            }
        }
    }

}
