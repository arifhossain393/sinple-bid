<?php

namespace App\Http\Controllers\Agent;

use App\Bid;
use App\Property;
use App\Traits\RedirectSubscription;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BidController extends Controller
{
    public function index(){
        $agent =auth()->guard('agent')->user();
        $properties = Property::where('agent_id',$agent->id)->latest()->paginate(10);
        return view('agent.bid.list',compact('properties'));
    }
    public function bid_status_change(Request $request){
       $bid = Bid::findOrFail($request->id);
       $bid->status = $request->status;
       $bid->save();
       return redirect()->back();
    }

    public function agent_search_bid_property(Request $request){

        $this->search_validation($request);
        $searchTerm = $request->keyword;

        $properties = Property::where('name', 'LIKE', "%{$searchTerm}%")
            ->orWhere('price', 'LIKE', "%{$searchTerm}%")
            ->orWhere('id', 'LIKE', "%{$searchTerm}%")
            ->paginate(10);
        return view('agent.bid.list',compact('properties'));
    }


    public function search_validation($request){
        $request->validate([
        'keyword' => 'required',]);
    }
}
