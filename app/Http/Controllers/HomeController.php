<?php

namespace App\Http\Controllers;

use App\Bid;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        return view('user.dashboard');
    }
    public function view_current_bid($id=null){
        $current_bids = Bid::wherePropertyId($id)->latest()->get();
        return response()->json($current_bids);
    }
}
