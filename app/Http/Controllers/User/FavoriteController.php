<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Favorite;
use Illuminate\Http\Request;


class FavoriteController extends Controller
{
    public function index(){
        $favorites = Favorite::where('user_id',auth()->user()->id)->latest()->get();
        return view('user.favorite.list',compact('favorites'));
    }
    public function get_all_favorite(){
        $favorites = Favorite::where('user_id',auth()->user()->id)->latest()->get();
       return response()->json($favorites);
    }
    public function destroy($id){
        Favorite::findOrFail($id)->delete();
        toast('Favorite Removed  Successfully','success');
        return redirect()->back();
    }
    public function store(Request $request){
        $favorite = Favorite::create($request->all());

    }
}
