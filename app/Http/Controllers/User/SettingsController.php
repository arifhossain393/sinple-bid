<?php

namespace App\Http\Controllers\User;

use App\Country;
use App\County;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{
    public function index($id)
    {
        $countries = Country::get();
        $county = County::get();
        $user = User::findorfail($id);
        return view('user.settings.settings',compact('user','countries','county'));
    }

    public function updateProfile(Request $request, $id)
    {

        $this->user_validation($request,$id);
        $user = User::findOrFail($id);
        $location = public_path('uploads/user');
        $img_name ='';
        if($request->hasFile('image')){
            $this->unlink_image($user);
            $img = $request->file('image');
            $name = $img->getClientOriginalName();
            $img_name = time()."_".$name;
            $img->move($location,$img_name);
        }else{
            $img_name = $user->image;
        }

        $user->update(array_merge($request->all(), ['image' => $img_name]));

        toast()->success('Your profile Successfully updated');
        return redirect()->back();
    }
    public function updatePassword(Request $request, $id)
    {
        $request->validate([
            //'old_password'=>'min:6|max:19|required',
            'password'=>'min:8|max:19|required',
            'confirm_password'=>'min:8|max:19|required|same:password',
        ]);
        $user = User::findOrFail($id);
        $check = Hash::check($request->old_password,$user->password);
        $new_hash_password = Hash::check($request->password,$user->password);
        $new_hash_make =  Hash::make($request->password);
        if ($new_hash_password){
            toast()->warning('Old Password and New Password Are Same...!');
            return redirect()->back()->with('active','active');
        }else if($check){
            $user->password = $new_hash_make;
            $user->save();
            toast()->success('Your Password Successfully Changed');
            return redirect()->back()->with('active','active');
        }
        else{
            toast()->warning('Old Password Not Match...!');
            return redirect()->back()->with('active','active');
        }
    }

    public function user_validation($request,$id=null){
        if($request->method()=='POST'){
            $email = 'required|string|email|max:255|unique:users';
            $img = 'required|max:2000';
            $password = 'required|max:19|min:8';
            $confirm_password = 'required|min:8|max:19|same:password';
        }elseif ($request->method()=='PUT' || $request->method()=='PATCH'){
            $email = 'required|string|email|max:255|unique:users,email,'.$id;
            $img = 'max:2000';
            $password = 'nullable';
            $confirm_password = 'nullable';
        }
        $request->validate([
            'f_name'=>'required|min:5|max:255',
            'email' => $email,
            'password' => $password,
            'confirm_password' => $confirm_password,
            'address_1'=>'required|min:5|max:255',

            'postcode'=>'required|min:4|max:40',
            'country'=>'required',

            'city'=>'required',
            'image'=>$img,
            'phone'=>'required|min:9',
        ]);
    }




    public function unlink_image($user){
        $document = public_path("uploads/user/{$user->image}");
        if (file_exists($document)) {
            @unlink($document);
        }

    }
}
