<?php

namespace App\Http\Controllers\User;

use App\Agent;
use App\Bid;
use App\Notifications\BidNotification;
use App\Property;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class BidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bids = Bid::latest()->where('user_id',auth()->user()->id)->get();
        return view('user.bid.list',compact('bids'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $bid =  Bid::wherePropertyId($request->property_id)->whereUserId($request->user_id)->first();
        $property = Property::findOrFail($request->property_id);
        if($property->bid_type==1){
            if($request->bid_amount>$property->price){
                $bid  = Bid::create($request->all());
                $agent = Agent::findOrFail($bid->property->agent->id);
                $agent->notify(new BidNotification($bid));
                return response('Created',201);
            }else{
                return response()->json('error_amount');
            }
        }elseif($property->bid_type==0){
            if($bid){
                return response()->json('error');
            }else{
                if($request->bid_amount>$property->price){
                    $bid  = Bid::create($request->all());
                    $agent = Agent::findOrFail($bid->property->agent->id);
                    $agent->notify(new BidNotification($bid));
                    return response('Created',201);
                }else{
                    return response()->json('error_amount');
                }
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bid = Bid::findOrFail($id);
        return view('user.bid.view',compact('bid'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
