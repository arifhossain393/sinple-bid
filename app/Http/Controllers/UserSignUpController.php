<?php

namespace App\Http\Controllers;

use App\Country;
use App\Notifications\UserConfirmation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserSignUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::get();
        return view('user-sign-up',compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //return $request->all();
        $img_name = '';
        $this->user_validation($request);
        $location = public_path('uploads/user');
        if($request->hasFile('image')){
            $img = $request->file('image');
            $name = $img->getClientOriginalName();
            $img_name = time()."_".$name;
            $img->move($location,$img_name);
        }
        unset($request['confirm_password']);
        $request['password'] = Hash::make($request->password);
        $request['token'] = str_random(60);
       $user = User::create(array_merge($request->all(), ['image' => $img_name]));



        $user = User::findorfail($user->id);
        $user->notify(new UserConfirmation($user));
        toast()->success('Congratulation Please Check Your Email ');

        return redirect()->back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        if($user->confirm_status==0){
            $confirm_status = '1';
        }else{
            return redirect()->route('agent.auth.login');
        }
        $user->confirm_status = $confirm_status;
        $user->save();
        toast()->success('Status Successfully Updated');
        return redirect()->route('login');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function user_validation($request,$id=null){
        if($request->method()=='POST'){
            $email = 'required|string|email|max:255|unique:users';
            $img = 'required|max:2000';
            $password = 'required|max:19|min:8';
            $confirm_password = 'required|min:8|max:19|same:password';
        }elseif ($request->method()=='PUT' || $request->method()=='PATCH'){
            $email = 'required|string|email|max:255|unique:users,email,'.$id;
            $img = 'max:2000';
            $password = 'nullable';
            $confirm_password = 'nullable';
        }
        $request->validate([
            'f_name'=>'required|min:5|max:255',
            'l_name'=>'required|min:5|max:255',
            'email' => $email,
            'password' => $password,
            'confirm_password' => $confirm_password,
            'address_1'=>'required|min:5|max:255',
            //'address_2'=>'required|min:5|max:255',
            'postcode'=>'required|min:4|max:40',
            'country'=>'required',
            'role_id'=>'required',
            'city'=>'required',
            //'image'=>$img,
            'phone'=>'required|min:9',
            'about'=>'required|min:10|max:10000',
        ],
        [

                'role_id.required'=>'The user type field required',
                'f_name.required'=>'First name field required',
                'l_name.required'=>'Last name field required',
                'address_1.required'=>'Address field required',

        ]);
    }

    public function user_confirmation($id=null){
        $user = User::findOrFail($id);
        if($user->confirm_status==0){
            $confirm_status = '1';
        }else{
            $confirm_status = '0';
        }
        $user->confirm_status = $confirm_status;
        $user->save();
        toast()->success('Status Successfully Updated');
        return redirect()->route('');
    }
}
