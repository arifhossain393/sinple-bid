<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Property;
use Illuminate\Http\Request;

class PropertyDetailsController extends Controller
{
    public function index($id=null)
    {
        $property = Property::findOrFail($id);
        return view('property-details',compact('property'));
    }
    public function agent_details($id){
        $agent = Agent::findOrFail($id);
        return view('agent-details',compact('agent'));
    }
}
